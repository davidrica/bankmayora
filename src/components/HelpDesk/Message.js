import React from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity, TouchableNativeFeedback, Modal, TouchableHighlight } from 'react-native';
import SvgUri from 'react-native-svg-uri';
import HeaderPage from '../HeaderPage';
import { css } from '../Styles';
import { Icon, List, Grid, Col, Radio } from 'native-base';
import { trash01, replyEmail01 } from 'helpers/images';

export default class Message extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      modalVisible: false,
    };
  }

  openModal() {
    this.setState({modalVisible:true});
  }

  closeModal() {
    this.setState({modalVisible:false});
  }

  static navigationOptions = ({ navigation }) => ({
    header: (
      <HeaderPage goBack={() => navigation.goBack()} title={'Help Desk'} navigations={() => navigation.navigate("DrawerOpen")} />
    )
  });

  render() {
    const { params } = this.props.navigation.state;
    return (
      <View style={{flex: 1, backgroundColor: '#eef0f0'}}>
        <View style={[css.container,]}>
          <View style={{flex: 1, marginBottom: 25}}>
            <View style={{flex: 0.1, flexDirection: 'row', paddingRight: 5, marginBottom: 45}}>
              <View style={{flex: 1, alignItems: 'flex-start'}}>
              </View>
              <View style={{flex: 0.2, alignItems: 'flex-end'}}>
                <TouchableOpacity onPress={() => this.props.navigation.navigate("NewMessage", {subject: params.subject})}>
                  <SvgUri width="22" height="22" source={replyEmail01} />
                </TouchableOpacity>
              </View>
              <View style={{flex: 0.2, alignItems: 'flex-end'}}>
                <TouchableOpacity onPress={() => this.openModal()}>
                  <SvgUri width="22" height="22" source={trash01} />
                </TouchableOpacity>
              </View>
            </View>

            <View style={{flexDirection: 'row', marginBottom: 30}}>
              <Text style={[css.b, {marginRight: 55}]}>Subject</Text>
              <Text style={[css.b, {marginRight: 20}]}>:</Text>
              <Text style={[css.p, css.b, css.darkRed]}>{params.subject}</Text>
            </View>

            <View>
              <Text style={[css.b, {marginBottom: 30}]}>Message</Text>
              <View style={{paddingHorizontal: 12}}>
                <Text style={[css.p, css.red, {marginBottom: 20}]}>Dear Mr. David Ricardo,</Text>
                <Text style={[css.p, css.red, {marginBottom: 20}]}>If you forgot your password, please login to Internet Banking and go to Menu Forgot Password. In this menu you have to input your User ID and Input OTP as authentication.</Text>
                <Text style={[css.p, css.red]}>Thank you.</Text>
              </View>
            </View>

          </View>
        </View>

        <Modal transparent={true} visible={this.state.modalVisible} onRequestClose={() => this.closeModal()} animationType={'none'}>
          <TouchableHighlight onPress={() => this.closeModal()} style={[styles.modalContainer, {flex: 1, backgroundColor: 'rgba(0,0,0,.6)'}]}>
            <View style={styles.MainContainer}>
              <SvgUri width="70" height="70" source={trash01} />
              <Text style={[css.b, {textAlign: 'center', fontSize: 16, marginTop: 15}]}>Do you want to delete this message?</Text>
              <View style={{flexDirection: 'row'}}>
                <TouchableOpacity onPress={() => this.closeModal()} style={[css.buttonGrey, {flex: 1, marginRight: 30, marginTop: 25, elevation: 1, paddingVertical: 5}]}>
                  <Text style={css.buttonTextGrey}>No</Text>
                </TouchableOpacity>
                <TouchableOpacity style={[css.button, {flex: 1, marginTop: 25, elevation: 1, paddingVertical: 5}]}>
                  <Text style={css.buttonText}>Yes</Text>
                </TouchableOpacity>            
              </View>
            </View>
          </TouchableHighlight>         
        </Modal>

      </View>
    );
  }
}

const styles = StyleSheet.create({
  modalContainer: {
    backgroundColor: 'transparent',
    paddingHorizontal: 20,
    justifyContent: 'center'
  },

  MainContainer :{
    justifyContent: 'center',
    alignItems: 'center',
    padding: 20,
    paddingBottom: 10,
    backgroundColor: '#fff',
    borderRadius: 3,
    elevation: 2,
  },
});