import React from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity, TouchableNativeFeedback, Modal, TouchableHighlight } from 'react-native';
import SvgUri from 'react-native-svg-uri';
import HeaderPage from '../HeaderPage';
import { css } from '../Styles';
import { Icon, List, Grid, Col, Radio, StyleProvider, CheckBox } from 'native-base';
import { trash01 } from 'helpers/images';

//Custom nativeBase Theme
import getTheme from 'themes/components';
import customTheme from 'themes/variables/customTheme';


export default class Inbox extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      itemSelected: 'itemOne',
      itemSelected2: 'itemOne',
      indexTemp: {},
      modalVisible: false,
      panelBackground: '#fff',
    };
  }

  radioPress(index) {
    this.setState({indexTemp: index});
    console.log(this.state.indexTemp);
  }

  openModal() {
    this.setState({modalVisible:true});
  }

  closeModal() {
    this.setState({modalVisible:false});
  }

  radioCheck(index) {
    let selectFalse = {};
    selectFalse['selected'+index] = false;

    let selectTrue = {};
    selectTrue['selected'+index] = true;

    this.state['selected'+index] ? this.setState(selectFalse) : this.setState(selectTrue);
    // this.state.selected1 == true ? this.setState({selected1: false}) : this.setState({selected1: true});
  }

  static navigationOptions = ({ navigation }) => ({
    header: (
      <HeaderPage goBack={() => navigation.goBack()} title={'Help Desk'} navigations={() => navigation.navigate("DrawerOpen")} />
    )
  });

  render() {
    let datas = [
    {
      subject: 'RE: Forgot User ID',
      date: '28/02/2018',
      time: '11:15',
      index: 0,
      selected: this.state.selected0,
    },
    {
      subject: 'RE: Forgot Password',
      date: '28/02/2018',
      time: '11:15',
      index: 1,
      selected: this.state.selected1,
    },
    {
      subject: 'RE: Error',
      date: '28/02/2018',
      time: '11:15',
      index: 2,
      selected: this.state.selected2,
    },
    {
      subject: 'RE: Forgot Password',
      date: '28/02/2018',
      time: '11:15',
      index: 3,
      selected: this.state.selected3,
    },
  ];
    return (
      <StyleProvider style={getTheme(customTheme)}>
        <View style={{flex: 1, backgroundColor: '#eef0f0'}}>
          <View style={[css.container,]}>
            <View style={{flex: 1, marginBottom: 25}}>
              <View style={{flex: 0.1, flexDirection: 'row', paddingRight: 5}}>
                <View style={{flex: 1, alignItems: 'flex-start'}}>
                  <Text style={[css.h2, css.h2a, css.red, {marginBottom: 15}]}>Inbox</Text>
                </View>
                <View style={{flex: 1, alignItems: 'flex-end'}}>
                  <TouchableOpacity onPress={() => this.openModal()}>
                    <SvgUri width="22" height="22" source={trash01} />
                  </TouchableOpacity>
                </View>
              </View>

              {
                datas.map(function(data, index){
                  return (
                    <TouchableOpacity key={index} 
                    style={[css.panel, {height: 43, flexDirection: 'row', paddingLeft: 0, backgroundColor: data.selected ? '#a71e23' : '#fff'}]}
                    onPress={() => this.props.navigation.navigate("Message", {subject: data.subject})}>
                      <TouchableOpacity onPress={() => this.radioCheck(index)}
                      style={{flex: 0.5, paddingLeft: 20, height: 43, justifyContent: 'center'}}>
                        <Radio onPress={() => this.radioCheck(index)}
                        selected={data.selected} />
                      </TouchableOpacity>
                      <View style={{flex: 2}}>
                        <Text style={[css.p, css.b,{color: data.selected ? '#fff' : '#080909'} ]}>{data.subject}</Text>     
                      </View>
                      <View style={{flex: 1, alignItems: 'flex-end'}}>
                        <Text style={{fontSize: 12, textAlign: 'right', color: data.selected ? '#fff' : '#080909'}}>{data.date}</Text>   
                        <Text style={{fontSize: 12, textAlign: 'right', color: data.selected ? '#fff' : '#080909'}}>{data.time}</Text>  
                      </View>
                    </TouchableOpacity>
                  );
                }.bind(this))
              }

            </View>
          </View>

          <Modal transparent={true} visible={this.state.modalVisible} onRequestClose={() => this.closeModal()} animationType={'none'}>
            <TouchableHighlight onPress={() => this.closeModal()} style={[styles.modalContainer, {flex: 1, backgroundColor: 'rgba(0,0,0,.6)'}]}>
              <View style={styles.MainContainer}>
                <SvgUri width="70" height="70" source={trash01} />
                <Text style={[css.b, {textAlign: 'center', fontSize: 16, marginTop: 15}]}>Do you want to delete this message?</Text>
                <View style={{flexDirection: 'row'}}>
                  <TouchableOpacity onPress={() => this.closeModal()} style={[css.buttonGrey, {flex: 1, marginRight: 30, marginTop: 25, elevation: 1, paddingVertical: 5}]}>
                    <Text style={css.buttonTextGrey}>No</Text>
                  </TouchableOpacity>
                  <TouchableOpacity style={[css.button, {flex: 1, marginTop: 25, elevation: 1, paddingVertical: 5}]}>
                    <Text style={css.buttonText}>Yes</Text>
                  </TouchableOpacity>            
                </View>
              </View>
            </TouchableHighlight>         
          </Modal>
        </View>
      </StyleProvider>
    );
  }
}

const styles = StyleSheet.create({
  modalContainer: {
    backgroundColor: 'transparent',
    paddingHorizontal: 20,
    justifyContent: 'center'
  },

  MainContainer :{
    justifyContent: 'center',
    alignItems: 'center',
    padding: 20,
    paddingBottom: 10,
    backgroundColor: '#fff',
    borderRadius: 3,
    elevation: 2,
  },
});