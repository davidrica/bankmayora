import React from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity } from 'react-native';
import SvgUri from 'react-native-svg-uri';
import { Container, Header, Title, Left, Icon, Right, Button, Body, Content, Card, CardItem } from 'native-base';
import { menuWhite01 } from 'helpers/images';

export default class HeaderPage extends React.Component {

  render() {
    return (
      <Header androidStatusBarColor={'#8a181b'} style={{backgroundColor: '#a71e23', paddingLeft: 20, paddingRight: 20}}>
        <Left style={{flex: 1}}>
          <Button
            transparent
            onPress={this.props.goBack}>
            <Icon name="arrow-back" />
          </Button>
        </Left>
        <Body style={{flex: 2, alignItems: 'center'}}>          
          <Title style={{fontWeight: 'bold', fontSize: 20}}>{this.props.title}</Title>
        </Body>
        <Right style={{flex: 1}}>
          <Button style={{marginRight: -15}} transparent onPress={this.props.navigations}>
            <SvgUri width="25" height="25" source={menuWhite01} />
          </Button>
        </Right>
      </Header>
    );
  }
}

const styles = StyleSheet.create({

  topHead: {
    backgroundColor: '#8a181b',
    // height: getStatusBarHeight(),
  },

  bottomHead: {
    backgroundColor: '#a71e23',
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 25,
  },

  bh01: {
    flex: 1,
    justifyContent: 'flex-start',
  },

  bh02: {
    flex: 6,
    justifyContent: 'center',
  },

  bh03: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
  },

  titleHeader: {
    color:'#fff', fontSize: 21, textAlign:'center', fontWeight: 'bold',
  }

});