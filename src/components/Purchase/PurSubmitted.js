import React from 'react';
import { StyleSheet, Text, View, TouchableOpacity, TextInput, Picker, DatePickerAndroid } from 'react-native';
import SvgUri from 'react-native-svg-uri';
import HeaderPage from '../HeaderPage';
import { css } from '../Styles';
import { Grid, Col, Content } from 'native-base';
import { NavigationActions } from "react-navigation";
import { success01 } from 'helpers/images';
import { connect } from 'react-redux';
import { goPurchaseConfirm1 } from 'helpers/redux/actions';

class PurSubmitted extends React.Component {
  constructor(props) {
    super(props);
    this.state = { 
      text: '',
    };
  }

  done() {
    this.props.goPurchaseConfirm1('','','','','DD/MM/YYYY','');
    this.props.navigation.navigate("HomeMenu");
  }

  render() {
    let { name, id, name2, id2, date, amount } = this.props.purchase;

    let amountConvert = amount;
    let totalDebit = parseInt(amountConvert);
    totalDebit = totalDebit + 5000;
    totalDebit = totalDebit.toString();
    totalDebit = totalDebit.replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
    amountConvert = amountConvert.replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");

    //Month
    const fields = date.split('/');
    let month = fields[1];

    if (month == '1') {month = 'Januari'}
    else if (month == '2') {month = 'Februari'}
    else if (month == '3') {month = 'Maret'}
    else if (month == '4') {month = 'April'}
    else if (month == '5') {month = 'Mei'}
    else if (month == '6') {month = 'Juni'}
    else if (month == '7') {month = 'Juli'}
    else if (month == '8') {month = 'Agustus'}
    else if (month == '9') {month = 'September'}
    else if (month == '10') {month = 'Oktober'}
    else if (month == '11') {month = 'November'}
    else if (month == '12') {month = 'Desember'}

    date = fields[0] + ' ' + month + ' ' + fields[2];

    return (
      <View style={{flex: 1, backgroundColor: '#eef0f0'}}>
        <Content showsVerticalScrollIndicator={false}>
          <View style={css.container}>
            <View style={{alignItems: 'center', marginTop: 10, marginBottom: 45,}}>
              <SvgUri width="50" height="50" source={success01} />
              <Text style={[css.b, {textAlign: 'center', fontSize: 16, maxWidth: 250, marginTop: 10}]}>This transaction has been submitted and successfully executed</Text>
            </View>
    
            <Grid style={{flex: 0, marginBottom: 20}}>
              <Col><Text style={[css.b, {marginBottom: 15} ]}>Transfer From</Text></Col>
              <Col style={{alignItems: 'flex-end'}}>
                <Text style={[css.b, css.darkRed, {fontSize: 18}]}>{name}</Text>
                <Text style={[css.darkRed, {fontSize: 16}]}>{id}</Text>
              </Col>
            </Grid>
            <Grid style={{flex: 0, marginBottom: 20}}>
              <Col style={{flex: 0.5}}><Text style={[css.b, {marginBottom: 15} ]}>Nick Name</Text></Col>
              <Col style={{alignItems: 'flex-end'}}>
                <Text style={[css.b, css.darkRed, {fontSize: 18}]}>{name}</Text>
              </Col>
            </Grid>
            <Grid style={{flex: 0, marginBottom: 20}}>
              <Col><Text style={[css.b, {marginBottom: 15} ]}>Payee Detail</Text></Col>
              <Col style={{alignItems: 'flex-end'}}>
                <Text style={[css.b, css.darkRed, {fontSize: 18}]}>{name2}</Text>
                <Text style={[css.darkRed, {fontSize: 16}]}>{id2}</Text>
              </Col>
            </Grid>
            <Grid style={{flex: 0, marginBottom: 20}}>
              <Col><Text style={[css.b, {marginBottom: 15} ]}>Amount</Text></Col>
              <Col style={{alignItems: 'flex-end'}}><Text style={[css.p, css.darkRed]}>IDR <Text style={[css.b, css.darkRed, {fontSize: 20}]}>{amountConvert}</Text>.00</Text></Col>
            </Grid>
            <Grid style={{flex: 0, marginBottom: 20}}>
              <Col style={{flex: 1.5}}><Text style={[css.b, {marginBottom: 15} ]}>Transaction Reference Number</Text></Col>
              <Col style={{alignItems: 'flex-end'}}><Text style={[css.b, css.p, css.darkRed, {fontSize: 18}]}>1873649134734</Text></Col>
            </Grid>
            <Grid style={{flex: 0, marginBottom: 20}}>
              <Col style={{flex: 1.5}}><Text style={[css.b, {marginBottom: 15} ]}>Reference Number</Text></Col>
              <Col style={{alignItems: 'flex-end'}}><Text style={[css.b, css.p, css.darkRed, {fontSize: 18}]}>1873649134734</Text></Col>
            </Grid>
            <Grid style={{flex: 0, marginBottom: 20}}>
              <Col><Text style={[css.b, {marginBottom: 15} ]}>Submission Date</Text></Col>
              <Col style={{alignItems: 'flex-end'}}><Text style={[css.p, css.darkRed, {fontSize: 18}]}>{date}</Text></Col>
            </Grid>

            <Text style={[css.darkRed, {marginBottom: 20, marginTop: 50, marginHorizontal: -10, fontSize: 11.5, textAlign: 'center'}]}>You can check this reference later on the transaction status home menu</Text>

            <TouchableOpacity onPress={() => this.done()} style={[css.button, {paddingVertical: 12, marginTop: 0}]}>
              <Text style={css.buttonText}>Done</Text>
            </TouchableOpacity>
          </View>
        </Content>
      </View>
    );
  }
}

PurSubmitted.navigationOptions = ({ navigation }) => ({
  header: (
    <HeaderPage goBack={() => navigation.dispatch(NavigationActions.back())} title={'Submitted !'} navigations={() => navigation.navigate("DrawerOpen")} />
  )
});

function mapStateToProps(state) {
  return {
    purchase: state.purchase
  }
}

export default connect(mapStateToProps, { goPurchaseConfirm1 })(PurSubmitted);