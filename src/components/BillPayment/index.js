import React, { Component } from "react";
import BillPayment from './BillPayment';
import BillConfirm1 from './BillConfirm1';
import BillConfirm2 from './BillConfirm2';
import BillNewBen1 from './BillNewBen1';
import BillSubmitted from './BillSubmitted';
import HomeMenu from '../HomeMenu';
import { StackNavigator } from "react-navigation";
import CardStackStyleInterpolator from 'react-navigation/src/views/CardStack/CardStackStyleInterpolator';

export default (DrawNav = StackNavigator(
	{
	  BillPayment: { screen: BillPayment },
	  BillConfirm1: { screen: BillConfirm1 },
	  BillConfirm2: { screen: BillConfirm2 },
	  BillNewBen1: { screen: BillNewBen1 },
	  BillSubmitted: { screen: BillSubmitted },
	  HomeMenu: { screen: HomeMenu },
	},
	{
		transitionConfig: ()=> {
			return {screenInterpolator: CardStackStyleInterpolator.forHorizontal}
		}
	}
));