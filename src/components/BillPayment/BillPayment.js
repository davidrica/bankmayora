import React from 'react';
import { StyleSheet, Text, View, TouchableOpacity, TextInput, Picker, DatePickerAndroid, ActivityIndicator, Modal, TouchableHighlight, 
  ListView } from 'react-native';
import SvgUri from 'react-native-svg-uri';
import HeaderPage from '../HeaderPage';
import { css } from '../Styles';
import { Icon, Grid, Col, ListItem } from 'native-base';
import { NavigationActions } from "react-navigation";
import { addList01 } from 'helpers/images';
import { connect } from 'react-redux';
import { goBillConfirm1 } from 'helpers/redux/actions';

class BillPayment extends React.Component {
  constructor(props) {
    super(props);
    this.state = { 
      text: '',
      id: '',
      text2: '',
      id2: '',
      language: 'IDR',
      simpleText: 'DD//MM/YYYY',
      modalVisible: false,
      modalVisible2: false,
      isLoading: true,
    };
    this.arrayholder = [] ;
    this.arrayholder2 = [] ;
  }

  componentDidMount() {

    const datas = [
      {id: 10150063825, name: 'David Mayora', balance: '4,000,000', route: 'CurrentBalance'},
      {id: 11241279512, name: 'David USD Mayora', balance: '22,134,286', route: 'CurrentBalance'},
      {id: 11241279111, name: 'David New Mayora', balance: '19,997,821', route: 'CurrentBalance'},
    ];

    let datas2 = [
      {id: 10150063825, name: 'David Telkomsel', balance: '4,000,000', route: 'CurrentBalance'},
      {id: 11241279111861, name: 'PLN', balance: '22,134,286', route: 'CurrentBalance'},
      {id: 11241279111861, name: 'PAM', balance: '19,997,821', route: 'CurrentBalance'},
    ];

    this.setState({bugState: '?'});
    datas2.push(
      {id: this.props.bill.phoneNumber, name: this.props.bill.nickName, balance: '22,134,286', route: 'CurrentBalance', bank: this.props.bill.bank},
    );

    if (datas2[3].id == undefined) {
      datas2 = [
        {id: 10150063825, name: 'David Telkomsel', balance: '4,000,000', route: 'CurrentBalance'},
        {id: 11241279111861, name: 'PLN', balance: '22,134,286', route: 'CurrentBalance'},
        {id: 11241279111861, name: 'PAM', balance: '19,997,821', route: 'CurrentBalance'},
      ];
    }

    let ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
    let ds2 = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
    this.setState({
      isLoading: false,
      dataSource: ds.cloneWithRows(datas),
      dataSource2: ds2.cloneWithRows(datas2),
    }, 
    function() {
      this.arrayholder = datas ;
      this.arrayholder2 = datas2 ;
    });
      
  }

  GetListViewItem (field, nameField, idField) {
    this.setState({modalVisible:false, modalVisible2: false});
    let { name, id, name2, id2, date, amount} = this.props.bill;
    if (field == 'fromAccount') {
      name = nameField;
      id = idField; 
      this.props.goBillConfirm1(name, id, name2, id2, date, amount);  
    } 
    else {
      name2 = nameField;
      id2 = idField;
      this.props.goBillConfirm1(name, id, name2, id2, date, amount);
    }  
    console.log(this.props.bill);
  }

  SearchFilterFunction(text){
    const newData = this.arrayholder.filter(function(item){
      const itemData = item.name.toUpperCase()
      const textData = text.toUpperCase()
      return itemData.indexOf(textData) > -1
    })

    this.setState({
      dataSource: this.state.dataSource.cloneWithRows(newData),
      text: text
    })
  }

  SearchFilterFunction2(text2){
    const newData2 = this.arrayholder2.filter(function(item){
      const itemData2 = item.name.toUpperCase()
      const textData2 = text2.toUpperCase()
      return itemData2.indexOf(textData2) > -1
    })

    this.setState({
      dataSource2: this.state.dataSource2.cloneWithRows(newData2),
      text2: text2
    })
  }

  handleNewBen(){
    this.setState({
      modalVisible2: false,
    })
    this.props.navigation.navigate('BillNewBen1');
  }

  openModal() {
    this.setState({modalVisible:true});
  }

  closeModal() {
    this.setState({modalVisible:false});
  }

  openModal2() {
    this.setState({modalVisible2:true});
  }

  closeModal2() {
    this.setState({modalVisible2:false});
  }

  buttonConfirm() {
    let { name, id, name2, id2, date, amount} = this.props.bill;
    this.props.goBillConfirm1(name, id, name2, id2, date, amount);
    this.props.navigation.navigate("DomConfirm1");
  }

  onChangeText(value, par) {
    let { name, id, name2, id2, date, amount} = this.props.bill;
    amount = value;
    this.props.goBillConfirm1(name, id, name2, id2, date, amount);  
    console.log(this.props.bill);
  }

  static title = 'DatePickerAndroid';
  static description = 'Standard Android date picker dialog';

  state = {
    presetDate: new Date(2020, 4, 5),
    simpleDate: new Date(2020, 4, 5),
    spinnerDate: new Date(2020, 4, 5),
    calendarDate: new Date(2020, 4, 5),
    defaultDate: new Date(2020, 4, 5),
    allDate: new Date(2020, 4, 5),
    simpleText: 'pick a date',
    spinnerText: 'pick a date',
    calendarText: 'pick a date',
    defaultText: 'pick a date',
    minText: 'pick a date, no earlier than today',
    maxText: 'pick a date, no later than today',
    presetText: 'pick a date, preset to 2020/5/5',
    allText: 'pick a date between 2020/5/1 and 2020/5/10',
  };

  showPicker = async (stateKey, options) => {
    try {
      var newState = {};
      const {action, year, month, day} = await DatePickerAndroid.open(options);
      if (action === DatePickerAndroid.dismissedAction) {
        newState[stateKey + 'Text'] = 'dismissed';
      } else {
        var date = new Date(year, month, day);
        newState[stateKey + 'Text'] = date.toLocaleDateString();
        newState[stateKey + 'Date'] = date;
        formatedDate = day + '/' + (month+1) + '/' + year;
      }
      let { name, id, name2, id2, date, amount} = this.props.bill;
      date = formatedDate;
      console.log(date);
      this.props.goBillConfirm1(name, id, name2, id2, date, amount);
      console.log(this.props.bill);

      this.setState(newState);
    } catch ({code, message}) {
      console.warn(`Error in example '${stateKey}': `, message);
    }
  };

  render() {
    if (this.state.isLoading) {
      return (
        <View style={{flex: 1, paddingTop: 20}}>
          <ActivityIndicator />
        </View>
      );
    }
    const { navigate } = this.props.navigation;
    const { name, id, name2, id2, date, amount } = this.props.bill;
    return (
      <View style={{flex: 1, backgroundColor: '#eef0f0'}}>
        <View style={css.container}>
          <View style={{flex: 1}}>
            <View style={css.singleFormGroup}>
              <Text style={[css.b, {marginBottom: 15}]}>From Account</Text>
              <TouchableOpacity style={[css.textInputView, {justifyContent: 'center'}]} onPress={() => this.openModal()}>
                <Grid style={{justifyContent: 'center'}}>
                  <Col style={{justifyContent: 'center'}}>
                    <Text style={[css.h2, css.darkRed]}>{name}</Text>
                  </Col>
                  <Col style={{justifyContent: 'center'}}>
                    <Text style={[css.p, css.darkRed, {fontSize: 16}]}>{id}</Text>
                  </Col>
                </Grid>
              </TouchableOpacity>
              <Icon name='md-search' style={{color:'#cecece', fontSize: 27, elevation: 99, position: 'absolute', bottom: 9, right: 22}}/>
            </View>

            <Modal transparent={true} visible={this.state.modalVisible} onRequestClose={() => this.closeModal()} animationType={'none'}>
              <TouchableHighlight onPress={() => this.closeModal()}style={[styles.modalContainer, {flex: 1, backgroundColor: 'rgba(47,48,48,.3)'}]}>
                <View style={styles.MainContainer}>
                  <View>
                    <TextInput 
                     style={[styles.TextInputStyleClass,{ fontSize: 20, color: '#9c9ea0'}]}
                     onChangeText={(text) => this.SearchFilterFunction(text)}
                     value={this.state.text}
                     underlineColorAndroid='transparent'
                    />
                    <Icon name='md-search' style={{color:'#cecece', fontSize: 27, elevation: 99, position: 'absolute', bottom: 15, right: 22}}/>
                  </View>
                  <ListView
                    dataSource={this.state.dataSource}
                    renderRow={(rowData) => 
                      
                      <ListItem style={[css.panel, {marginLeft: 0, paddingHorizontal: 10}]} button onPress={this.GetListViewItem.bind(this, 'fromAccount', rowData.name, rowData.id)}>
                        <View style={css.leftTextPanel}>
                          <Text style={[css.h2, css.darkRed]}>{rowData.name}</Text>
                          <Text style={[css.p, css.darkRed]}>{rowData.id}</Text>         
                        </View>
                        <View style={css.rightTextPanel}>
                          <Text style={[css.p, css.darkRed, {textAlign: 'right'}]}>IDR 
                            <Text style={[css.h2, css.darkRed]}> {rowData.balance}</Text>
                            .00
                          </Text>      
                        </View>
                      </ListItem>
                    }
                    enableEmptySections={true}
                    style={{marginTop: 5}}
                  />
                </View>
              </TouchableHighlight>         
            </Modal>

            <View style={css.singleFormGroup}>
              <Text style={[css.b, {marginBottom: 15}]}>Payee</Text>
              <TouchableOpacity style={[css.textInputView, {justifyContent: 'center'}]} onPress={() => this.openModal2()}>
                <Grid style={{justifyContent: 'center'}}>
                  <Col style={{justifyContent: 'center'}}>
                    <Text style={[css.h2, css.darkRed]}>{name2}</Text>
                  </Col>
                  <Col style={{justifyContent: 'center'}}>
                    <Text style={[css.p, css.darkRed, {fontSize: 16}]}>{id2}</Text>
                  </Col>
                </Grid>
              </TouchableOpacity>
              <Icon name='md-search' style={{color:'#cecece', fontSize: 27, elevation: 99, position: 'absolute', bottom: 9, right: 22}}/>
            </View>

            <Modal transparent={true} visible={this.state.modalVisible2} onRequestClose={() => this.closeModal2()} animationType={'none'}>
              <TouchableHighlight onPress={() => this.closeModal2()}style={[styles.modalContainer, {flex: 1, backgroundColor: 'rgba(47,48,48,.3)'}]}>
                <View style={styles.MainContainer}>
                  <View>
                    <TextInput 
                     style={[styles.TextInputStyleClass,{ fontSize: 20, color: '#9c9ea0'}]}
                     onChangeText={(text2) => this.SearchFilterFunction2(text2)}
                     value={this.state.text2}
                     underlineColorAndroid='transparent'
                    />
                    <Icon name='md-search' style={{color:'#cecece', fontSize: 27, elevation: 99, position: 'absolute', bottom: 15, right: 22}}/>
                  </View>
                  <ListView
                    dataSource={this.state.dataSource2}
                    renderRow={(rowData) => 
                      
                      <ListItem style={[css.panel, {marginLeft: 0, paddingHorizontal: 10}]} button onPress={this.GetListViewItem.bind(this, 'payee', rowData.name, rowData.id, rowData.bank)}>
                        <View style={css.leftTextPanel}>
                          <Text style={[css.h2, css.darkRed]}>{rowData.name}</Text>         
                        </View>
                        <View style={css.rightTextPanel}>
                          <Text style={[css.p, css.darkRed, {textAlign: 'right'}]}>{rowData.id}</Text>    
                        </View>
                      </ListItem>
                    }
                    enableEmptySections={true}
                    style={{marginTop: 5}}
                  />
                  <TouchableOpacity onPress={() => this.handleNewBen()} style={[css.panel, {marginLeft: 0, alignItems: 'center'}]}>
                    <View style={[{flexDirection: 'row', alignItems: 'center', paddingHorizontal: 10, paddingLeft: 7}]}>
                      <View style={{flex: 1}}>
                        <SvgUri width="25" height="25" source={addList01} />  
                      </View>
                      <View style={{flex: 6}}>
                        <Text style={[css.h2, css.darkRed, {textAlign: 'center'}]}>Add new beneficiary contact</Text>     
                        
                      </View>
                    </View>
                  </TouchableOpacity>
                </View>
              </TouchableHighlight>         
            </Modal>

            <View style={[css.labelInputInline, css.singleFormGroup]}>
              <Text style={[css.b, {flex: 1}]}>Date</Text>
              <TouchableOpacity onPress={this.showPicker.bind(this, 'simple', {date: this.state.simpleDate})} style={[css.textInputView, {flex: 0.7, justifyContent: 'center', height: 45,}]}> 
                <Text style={[css.red, {alignItems: 'center', fontSize: 16}]}>{date}</Text>
                <Icon name='md-arrow-dropdown' style={{color:'#a71e23', fontSize: 20, position: 'absolute', bottom: 11, right: 22}}/>
              </TouchableOpacity>
            </View>

            <View style={[css.labelInputInline, css.singleFormGroup]}>
              <Text style={[css.b, {flex: 1}]}>Amount</Text>
              <TextInput keyboardType='numeric' maxLength={12} underlineColorAndroid='transparent' style={[css.textInput, {flex: 2}]} 
              onChangeText={(amount) => this.onChangeText(amount, 'amount')} value={amount} />
            </View>
            <View style={{marginTop: 35}}>
              <TouchableOpacity onPress={() => this.props.navigation.navigate("BillConfirm1")} style={[css.button, {flex: 0, paddingVertical: 12}]}>
                <Text style={css.buttonText}>Confirm</Text>
              </TouchableOpacity>
            </View>
          </View>

        </View>
      </View>
    );
  }
}

BillPayment.navigationOptions = ({ navigation }) => ({
  header: (
    <HeaderPage goBack={() => navigation.dispatch(NavigationActions.back())} title={'Bill Payment'} navigations={() => navigation.navigate("DrawerOpen")} />
  )
});

const styles = StyleSheet.create({
  modalContainer: {
    backgroundColor: 'transparent',
    paddingHorizontal: 20,
    paddingTop: 103,
  },

  MainContainer :{
    justifyContent: 'center',
    padding: 10,
    backgroundColor: 'rgba(215,215,215,.9)',
    borderRadius: 3,
    elevation: 2,
  },

  TextInputStyleClass:{
    height: 61,
    borderRadius: 3 ,
    elevation: 1,
    backgroundColor : "#FFFFFF",
    marginBottom: 0,
    paddingHorizontal: 10,
  }
});

function mapStateToProps(state) {
  return {
    bill: state.bill
  }
}

export default connect(mapStateToProps, { goBillConfirm1 })(BillPayment);