import React from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity, Linking } from 'react-native';
import SvgUri from 'react-native-svg-uri';
import HomeMenu from './HomeMenu';
import DeviceRegistration from './DeviceRegistration';
import DeviceRegistrationOTP from './DeviceRegistrationOTP';
import { Container, Header, Title, Left, Icon, Right, Button, Body, Content, Card, CardItem } from "native-base";
import { Provider } from 'react-redux';
import { bm01, mobileBanking01, clickMayora01 } from 'helpers/images';
import { connect } from 'react-redux';

class FirstPageOption extends React.Component {
  render() {
    const {registered} = this.props.deviceRegistration;
    return (
      <View style={{flex: 1, backgroundColor: '#8a181b', padding: 4, paddingBottom: 6}}>
        <Header androidStatusBarColor={'#8a181b'} style={{backgroundColor: '#a71e23', height: 0}}/>
        <View style={styles.container}>
          <Image source={bm01} 
          style={{width:200, resizeMode: 'contain', alignSelf: 'flex-end', marginRight: -40, marginTop: 5, marginBottom: 45}}/>
          <View style={{flex: 1, justifyContent: 'center'}}>
            <TouchableOpacity onPress={registered ? this.props.onLogin : this.props.onMobileBanking} style={[styles.optionBox, {marginBottom: 50}]}>
              <View>
                <SvgUri width='130' height='130' fill={'#a71e23'} source={mobileBanking01}/>
              </View>
            </TouchableOpacity>
            <TouchableOpacity onPress={()=>{ Linking.openURL('http://bankmayora.com')}} style={[styles.optionBox, {marginBottom: 50}]}>
              <View>
                <SvgUri width='200' height='200' fill={'#a71e23'} source={clickMayora01}/>
              </View>
            </TouchableOpacity>
          </View>
        </View>
        <Text style={{color: '#a71e23', textAlign: 'center', fontSize: 12, paddingBottom: 10, backgroundColor: '#eef0f0'}}>
        Copyright @ 2017, Bank Mayora{"\n"}
        Designed by PT. Aprisma Indonesia, All right reserved
        </Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#eef0f0',
    // justifyContent: 'center',
    paddingHorizontal: 50,
    borderTopLeftRadius: 30,
  },

  content: {

  },

  optionBox: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#fff',
    maxHeight: 150,
    elevation: 2,
  },
});

function mapStateToProps(state) {
  return {
    deviceRegistration: state.deviceRegistration
  }
}

export default connect(mapStateToProps)(FirstPageOption);