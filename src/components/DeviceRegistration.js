import React from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity, Linking, TouchableHighlight, Modal } from 'react-native';
import SvgUri from 'react-native-svg-uri';
import HomeMenu from './HomeMenu';
import DeviceRegistrationOTP from './DeviceRegistrationOTP';
import {css} from './Styles';
import { Container, Header, Title, Left, Icon, Right, Button, Body, Content, Card, CardItem, Form, Input, Item, Label} from "native-base";
import { bm01, failed01 } from 'helpers/images';

export default class DeviceRegistration extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      userId: '',
      password: '',
      modalVisible: false,
    }
  }

  registration() {
    if (this.state.userId == 'david' && this.state.password == '1234') {
      this.props.onDeviceRegistrationOTP()
    }
    else {
      this.setState({modalVisible:true});
    }
  }

  openModal() {
    this.setState({modalVisible:true});
  }

  closeModal() {
    this.setState({modalVisible:false});
  }

  render() {
    return (
      <View style={{flex: 1, backgroundColor: '#8a181b', padding: 4, paddingBottom: 6}}>
        <Header androidStatusBarColor={'#8a181b'} style={{backgroundColor: '#a71e23', height: 0}}/>
        <View style={styles.container}>
          <Image source={bm01} 
          style={{width:200, resizeMode: 'contain', alignSelf: 'flex-end', marginRight: -10, marginTop: 5}}/>
          <View style={{flex: 2,justifyContent: 'center'}}>
	          <Form>
              <View style={{paddingRight: 15}}>
  	            <Item floatingLabel>
  	              <Label>User ID</Label>
  	              <Input onChangeText={(userId) => this.setState({userId})} autoCapitalize='none' style={[css.red, {paddingLeft: 25, fontSize: 20}]} />
  	            </Item>
  	            <Item floatingLabel>
  	              <Label>Password</Label>
  	              <Input onChangeText={(password) => this.setState({password})} autoCapitalize='none' secureTextEntry style={[css.red, {paddingLeft: 25, fontSize: 20}]} />
  	            </Item>       
              </View>
	            <TouchableOpacity style={css.button} onPress={() => this.registration()}>
		            <Text style={css.buttonText}>Device Registration</Text>
            	</TouchableOpacity>
	          </Form>
          </View>
        </View>
        <Text style={{color: '#a71e23', textAlign: 'center', fontSize: 12, paddingBottom: 10, backgroundColor: '#eef0f0'}}>
        Copyright @ 2017, Bank Mayora{"\n"}
        Designed by PT. Aprisma Indonesia, All right reserved
        </Text>
        <Modal transparent={true} visible={this.state.modalVisible} onRequestClose={() => this.closeModal()} animationType={'none'}>
          <TouchableHighlight onPress={() => this.closeModal()} style={[styles.modalContainer, {flex: 1, backgroundColor: 'rgba(0,0,0,.6)'}]}>
            <View style={styles.MainContainer}>
             <SvgUri width="50" height="50" source={failed01} />
             <Text style={[css.red, {textAlign: 'center', fontSize: 16, maxWidth: 300, marginTop: 20}]}><Text style={[css.b, css.red]}>User ID</Text> or <Text style={[css.b, css.red]}>Password</Text> you entered is incorrect</Text>
             <Text style={[css.red, {textAlign: 'center', fontSize: 16, maxWidth: 300}]}>Please try again</Text>
            </View>
          </TouchableHighlight>         
        </Modal>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#eef0f0',
    // justifyContent: 'center',
    paddingHorizontal: 20,
    borderTopLeftRadius: 30,
  },

  content: {

  },

  optionBox: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#fff',
    maxHeight: 150,
    elevation: 2,
  },

   modalContainer: {
    backgroundColor: 'transparent',
    paddingHorizontal: 20,
    justifyContent: 'center',
    paddingVertical: 25,
  },

  MainContainer :{
    justifyContent: 'center',
    alignItems: 'center',
    padding: 20,
    backgroundColor: '#fff',
    borderRadius: 3,
    elevation: 2,
  },
});