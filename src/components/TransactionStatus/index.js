import React, { Component } from "react";
import TransactionStatus from './TransactionStatus';
import TransHistory from './TransHistory';
import TransReference from './TransReference';
import HomeMenu from '../HomeMenu';
import { StackNavigator } from "react-navigation";
import CardStackStyleInterpolator from 'react-navigation/src/views/CardStack/CardStackStyleInterpolator';

export default (DrawNav = StackNavigator(
	{
	  TransactionStatus: { screen: TransactionStatus },
	  TransHistory: { screen: TransHistory },
	  TransReference: { screen: TransReference },
	  HomeMenu: { screen: HomeMenu },
	},
	{
		transitionConfig: ()=> {
			return {screenInterpolator: CardStackStyleInterpolator.forHorizontal}
		}
	}
));