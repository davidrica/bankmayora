import React from 'react';
import { StyleSheet, Text, View, TouchableOpacity, TextInput, Picker, DatePickerAndroid } from 'react-native';
import SvgUri from 'react-native-svg-uri';
import HeaderPage from '../HeaderPage';
import { css } from '../Styles';
import { Icon } from 'native-base';
import { NavigationActions } from "react-navigation";
// import { Icon } from 'react-native-elements';

export default class TransactionStatus extends React.Component {
  constructor(props) {
    super(props);
    this.state = { 
      text: '',
      language: 'IDR',
      simpleText: 'DD//MM/YYYY',
      spinnerText: 'DD//MM/YYYY',
      transaction: 'Select Transaction'
    };
  }

  static title = 'DatePickerAndroid';
  static description = 'Standard Android date picker dialog';

  state = {
    presetDate: new Date(2020, 4, 5),
    simpleDate: new Date(2020, 4, 5),
    spinnerDate: new Date(2020, 4, 5),
    calendarDate: new Date(2020, 4, 5),
    defaultDate: new Date(2020, 4, 5),
    allDate: new Date(2020, 4, 5),
    simpleText: 'pick a date',
    spinnerText: 'pick a date',
    calendarText: 'pick a date',
    defaultText: 'pick a date',
    minText: 'pick a date, no earlier than today',
    maxText: 'pick a date, no later than today',
    presetText: 'pick a date, preset to 2020/5/5',
    allText: 'pick a date between 2020/5/1 and 2020/5/10',
    transaction: 'Select Amount'
  };

  showPicker = async (stateKey, options) => {
    try {
      var newState = {};
      const {action, year, month, day} = await DatePickerAndroid.open(options);
      if (action === DatePickerAndroid.dismissedAction) {
        newState[stateKey + 'Text'] = 'dismissed';
      } else {
        var date = new Date(year, month, day);
        newState[stateKey + 'Text'] = date.toLocaleDateString();
        newState[stateKey + 'Date'] = date;
      }
      this.setState(newState);
    } catch ({code, message}) {
      console.warn(`Error in example '${stateKey}': `, message);
    }
  };

  render() {
    const { navigate } = this.props.navigation;
    return (
      <View style={{flex: 1, backgroundColor: '#eef0f0'}}>
        <View style={css.container}>
          <View style={{flex: 1}}>
            <View style={[css.labelInputInline, css.singleFormGroup]}>
              <Text style={[css.b, {flex: 1}]}>Transaction Date From</Text>
              <TouchableOpacity onPress={this.showPicker.bind(this, 'simple', {date: this.state.simpleDate})} style={[css.textInputView, {flex: 1, justifyContent: 'center', height: 45, alignItems: 'center', paddingRight: 26, paddingLeft: 4}]}> 
                <Text style={[css.red, {alignItems: 'center', fontSize: 16}]}>{this.state.simpleText}</Text>
              </TouchableOpacity>
              <Icon name='md-arrow-dropdown' style={{color:'#a71e23', fontSize: 20, position: 'absolute', bottom: 11, right: 22, elevation: 1}}/>
            </View>

            <View style={[css.labelInputInline, css.singleFormGroup]}>
              <Text style={[css.b, {flex: 1}]}>Transaction Date To</Text>
              <TouchableOpacity onPress={this.showPicker.bind(this, 'spinner', {date: this.state.spinnerDate})} style={[css.textInputView, {flex: 1, justifyContent: 'center', height: 45, alignItems: 'center', paddingRight: 26, paddingLeft: 4}]}> 
                <Text style={[css.red, {alignItems: 'center', fontSize: 16}]}>{this.state.spinnerText}</Text>
              </TouchableOpacity>
              <Icon name='md-arrow-dropdown' style={{color:'#a71e23', fontSize: 20, position: 'absolute', bottom: 11, right: 22, elevation: 1}}/>
            </View>

            <View style={[css.labelInputInline, css.singleFormGroup]}>
              <Text style={[css.b, {flex: 1}]}>Transaction Type</Text>
              <Picker
                mode='dropdown'
                selectedValue={this.state.transaction}
                onValueChange={(itemValue, itemIndex) => this.setState({transaction: itemValue})}
                style={[css.textInputView, {flex: 1, justifyContent: 'center', height: 45, color: '#a71e23'}]}>
                <Picker.Item label="Select Transaction" value="select-transaction" />
                <Picker.Item label="Select All" value="SelectAll" />
                <Picker.Item label="Inhouse Transfer" value="InhouseTransfer" />
                <Picker.Item label="Domestic Transfer" value="DomesticTransfer" />
                <Picker.Item label="SKN Transfer" value="SKNTransfer" />
                <Picker.Item label="RTGS Transfer" value="RTGSTransfer" />
                <Picker.Item label="Bill Payment" value="BillPayment" />
                <Picker.Item label="Purchaseer" value="Purchaseer" />
              </Picker>
              <Icon name='md-arrow-dropdown' style={{color:'#a71e23', fontSize: 20, position: 'absolute', bottom: 11, right: 22, elevation: 1}}/>
            </View>
          </View>

          <TouchableOpacity onPress={() => this.props.navigation.navigate('TransHistory', {transaction: this.state.transaction})} style={[css.button, {flex: 0, paddingVertical: 12, marginBottom: 20}]}>
            <Text style={css.buttonText}>Continue</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

TransactionStatus.navigationOptions = ({ navigation }) => ({
  header: (
    <HeaderPage goBack={() => navigation.dispatch(NavigationActions.back())} title={'Transaction Status'} navigations={() => navigation.navigate("DrawerOpen")} />
  )
});