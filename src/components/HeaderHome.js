import React from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity } from 'react-native';
import SvgUri from 'react-native-svg-uri';
import { Container, Header, Title, Left, Icon, Right, Button, Body, Content, Card, CardItem } from "native-base";
import { userWhite01, menuWhite01 } from 'helpers/images';

export default class HeaderHome extends React.Component {

  render() {
    return (
      <Header androidStatusBarColor={'#8a181b'} style={{backgroundColor: '#a71e23', paddingLeft: 20}}>
        <Left style={{marginLeft: 5}}><SvgUri width="25" height="25" source={userWhite01} /></Left>
        <Body style={{minWidth: 200, paddingLeft: 20,}}>          
          <Title style={{color:'#fff', fontSize: 18}}>Welcome, David!</Title>
          <Title style={{color:'#fff', fontSize: 15}}>Last Login: 3 Oct 18:03</Title>
        </Body>
        <Right>
          <Button style={{marginRight: -5}} transparent onPress={this.props.navigations}>
            <SvgUri width="25" height="25" source={menuWhite01} />
          </Button>
        </Right>
      </Header>
    );
  }
}

const styles = StyleSheet.create({

  topHead: {
    backgroundColor: '#8a181b',
    height: 25,
  },

  bottomHead: {
    backgroundColor: '#a71e23',
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 25,
  },

  bh01: {
    flex: 1,
    justifyContent: 'flex-start',
  },

  bh02: {
    flex: 6,
    justifyContent: 'center',
  },

  bh03: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
  },

});