import React from 'react';
import { StyleSheet, Text, View, TouchableOpacity, TextInput, TouchableHighlight, Modal, Picker } from 'react-native';
import HeaderPage from '../HeaderPage';
import { css } from '../Styles';
import { Grid, Col, Content, Icon, Radio, StyleProvider } from 'native-base';
import { NavigationActions } from "react-navigation";
import SvgUri from 'react-native-svg-uri';
import { success01 } from 'helpers/images';
import { connect } from 'react-redux';
import { newDomesticBeneficiary } from 'helpers/redux/actions';

//Custom nativeBase Theme
import getTheme from 'themes/components';
import customTheme from 'themes/variables/customTheme';

class DomNewBen1 extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      onOffBackground: '#fff',
      onOffText: 'OFF',
      onOffColor: '#080909',
      onOffWidth: 1,
      onOffBorderColor: '#080909',
      radioBackground1: '#a71e23', radioBackground2: '#fff', radioBackground3: '#a71e23', radioBackground4: '#fff',
      radioTextColor1: '#fff', radioTextColor2: '#a71e23', radioTextColor3: '#fff', radioTextColor4: '#a71e23',
      modalVisible: false,
      itemSelected: 'itemOne',
      itemSelected2: 'itemOne',
    };
  }

  handleSave() {
    this.state.onOffBackground == '#fff' ? this.setState({
      onOffBackground: '#a71e23', onOffText: 'ON', onOffColor: '#fff', onOffWidth: 0
    })  : this.setState({
      onOffBackground: '#fff', onOffText: 'OFF', onOffColor: '#080909', onOffWidth: 1
    });
  }

  handleBenAdded(){
    this.setState({
      modalVisible: false,
    })
    let { nickName, accountNumber, accountName, bank, beneficiaryType, transactionType, citizenshipType, saveBeneficiary } = this.props.domestic;
    this.props.newDomesticBeneficiary(nickName, accountNumber, accountName, bank, beneficiaryType, transactionType, citizenshipType, saveBeneficiary);
    this.props.navigation.navigate('DomesticTransfer');
  }

  handleRadio1() {
    this.setState({
      itemSelected: 'itemOne',
      radioBackground1: '#a71e23', radioBackground2: '#fff',
      radioTextColor1: '#fff', radioTextColor2: '#a71e23',
    });
  }

  handleRadio2() {
    this.setState({
      itemSelected: 'itemTwo',
      radioBackground2: '#a71e23', radioBackground1: '#fff',
      radioTextColor2: '#fff', radioTextColor1: '#a71e23',
    });
  }

  handleRadio3() {
    this.setState({
      itemSelected2: 'itemOne',
      radioBackground3: '#a71e23', radioBackground4: '#fff',
      radioTextColor3: '#fff', radioTextColor4: '#a71e23',
    });
  }

  handleRadio4() {
    this.setState({
      itemSelected2: 'itemTwo',
      radioBackground4: '#a71e23', radioBackground3: '#fff',
      radioTextColor4: '#fff', radioTextColor3: '#a71e23',
    });
  }

  openModal() {
    this.setState({modalVisible:true});
  }

  closeModal() {
    this.setState({modalVisible:false});
  }

  onChangeText(value, par) {
    let { nickName, accountNumber, accountName, bank, beneficiaryType, transactionType, citizenshipType, saveBeneficiary } = this.props.domestic;
    if (par == 'nickName' ) {nickName = value}
    else if (par == 'accountNumber' ) {accountNumber = value} 
    else if (par == 'accountName') {accountName = value}
    else if (par == 'bank') {bank = value}
    else if (par == 'beneficiaryType') {beneficiaryType = value}
    else if (par == 'transactionType') {transactionType = value}
    else if (par == 'citizenshipType') {citizenshipType = value}
    else if (par == 'saveBeneficiary') {saveBeneficiary = value}
    this.props.newDomesticBeneficiary(nickName, accountNumber, accountName, bank, beneficiaryType, transactionType, citizenshipType, saveBeneficiary);  
    console.log(this.props.domestic);
  }

  static navigationOptions = ({ navigation }) => ({
    header: (
      <HeaderPage goBack={() => navigation.goBack()} title={'Domestic Transfer'} navigations={() => navigation.navigate("DrawerOpen")} />
    )
  });

  render() {
    const { nickName, accountNumber, accountName, bank, beneficiaryType, transactionType, citizenshipType, saveBeneficiary  } = this.props.domestic;
    return (
      <StyleProvider style={getTheme(customTheme)}>
        <View style={{flex: 1, backgroundColor: '#eef0f0'}}>
          <Content showsVerticalScrollIndicator={false} style={css.container}>
            <View style={{flex: 1}}>
              <Text style={[css.b, {marginBottom: 15} ]}>Transfer To - <Text style={[css.b, css.red, {fontSize: 16}]}>New Beneficiary Contact</Text></Text>
              <View style={[css.labelInputInline, css.singleFormGroup]}>
                <Text style={[css.b, {flex: 1}]}>Nick Name</Text>
                <TextInput  maxLength={16} underlineColorAndroid='transparent' style={[css.textInput, {flex: 1.4}]} 
                onChangeText={(nickName) => this.onChangeText(nickName, 'nickName')} value={nickName} />
              </View>
              <View style={[css.labelInputInline, css.singleFormGroup]}>
                <Text style={[css.b, {flex: 1}]}>Account No.</Text>
                <TextInput keyboardType='numeric' maxLength={16} underlineColorAndroid='transparent' style={[css.textInput, {flex: 1.4}]} 
                onChangeText={(accountNumber) => this.onChangeText(accountNumber, 'accountNumber')} value={accountNumber} />
              </View>
              <View style={[css.labelInputInline, css.singleFormGroup]}>
                <Text style={[css.b, {flex: 1}]}>Account Name</Text>
                <TextInput underlineColorAndroid='transparent' style={[css.textInput, {flex: 1.4}]} 
                onChangeText={(accountName) => this.onChangeText(accountName, 'accountName')} value={accountName} />
              </View>
              <View style={[css.labelInputInline, css.singleFormGroup]}>
                <Text style={[css.b, {flex: 1}]}>Bank</Text>
                <Picker 
                  mode='dropdown'
                  selectedValue={bank}
                  onValueChange={(itemValue, itemIndex) => this.onChangeText(itemValue, 'bank')}
                  style={[css.textInputView, {flex: 1.4, justifyContent: 'center', height: 45, color: '#a71e23'}]}>
                  <Picker.Item label="    Select Bank" value="Select Bank" />
                  <Picker.Item label="    Bank ANZ" value="Bank ANZ" />
                  <Picker.Item label="    Bank Bukopin" value="Bank Bukopin" />
                  <Picker.Item label="    Bank Central Asia" value="Bank Central Asia" />
                  <Picker.Item label="    Bank Jabar Banten" value="Bank Jabar Banten" />
                  <Picker.Item label="    Bank Mandiri" value="Bank Mandiri" />
                  <Picker.Item label="    Bank Negara Indonesia" value="Bank Negara Indonesia" />
                  <Picker.Item label="    Bank Rakyat Indonesia" value="Bank Rakyat Indonesia" />
                </Picker>
                <Icon name='md-arrow-dropdown' style={{color:'#a71e23', fontSize: 20, position: 'absolute', bottom: 11, right: 22, elevation: 1}}/>
              </View>
              <View style={[css.labelInputInline, css.singleFormGroup]}>
                <Text style={[css.b, {flex: 1}]}>Beneficiary Type</Text>
                <Grid style={{flex: 1.8}}>
                  <Col style={{marginRight: 5}}>
                    <TouchableHighlight onPress={() => this.handleRadio1()} 
                    style={[css.radioWrap, {backgroundColor: this.state.radioBackground1 }]}>
                      <View style={css.radioContainer}>
                        <Radio onPress={() => this.handleRadio1()} selected={this.state.itemSelected == 'itemOne'} style={{marginTop: 3}}/>
                        <Text style={[css.radioText, {color: this.state.radioTextColor1}]}>Individual</Text>       
                      </View>
                    </TouchableHighlight>
                  </Col>
                  <Col>
                    <TouchableHighlight onPress={() => this.handleRadio2()} 
                    style={[css.radioWrap, {backgroundColor: this.state.radioBackground2}]}>
                      <View style={css.radioContainer}>
                        <Radio onPress={() => this.handleRadio2()} selected={this.state.itemSelected == 'itemTwo'} style={{marginTop: 3}}/>
                        <Text style={[css.radioText, {color: this.state.radioTextColor2}]}>Corporate</Text>       
                      </View>
                    </TouchableHighlight>
                  </Col>
                </Grid>
              </View>
              <View style={[css.labelInputInline, css.singleFormGroup]}>
                <Text style={[css.b, {flex: 1}]}>Transaction Type</Text>
                <Picker 
                  mode='dropdown'
                  selectedValue={transactionType}
                  onValueChange={(itemValue, itemIndex) => this.onChangeText(itemValue, 'transactionType')}
                  style={[css.textInputView, {flex: 1.4, justifyContent: 'center', height: 45, color: '#a71e23'}]}>
                  <Picker.Item label="    Transfer to Customer" value="Transfer to Customer" />
                  <Picker.Item label="    Transfer to me" value="Transfer to me" />
                </Picker>
                <Icon name='md-arrow-dropdown' style={{color:'#a71e23', fontSize: 20, position: 'absolute', bottom: 11, right: 22, elevation: 1}}/>
              </View>
              <View style={[css.labelInputInline, css.singleFormGroup]}>
                <Text style={[css.b, {flex: 1}]}>Citizenship Type</Text>
                <View style={{flex: 1.8, flexDirection: 'row'}}>
                  <View style={{marginRight: 5, flex: 1}}>
                    <TouchableHighlight onPress={() => this.handleRadio3()}
                    style={[css.radioWrap, {backgroundColor: this.state.radioBackground3 }]}>
                      <View style={css.radioContainer}>
                        <Radio onPress={() => this.handleRadio3()} selected={this.state.itemSelected2 == 'itemOne'} style={{marginTop: 3}}/>
                        <Text style={[css.radioText, {color: this.state.radioTextColor3}]}>Citizen</Text>       
                      </View>
                    </TouchableHighlight>
                  </View>
                  <View style={{flex: 1.3}}>
                    <TouchableHighlight onPress={() => this.handleRadio4()}
                    style={[css.radioWrap, {backgroundColor: this.state.radioBackground4 }]}>
                      <View style={css.radioContainer}>
                        <Radio onPress={() => this.handleRadio4()} selected={this.state.itemSelected2 == 'itemTwo'} style={{marginTop: 3}}/>
                        <Text style={[css.radioText, {color: this.state.radioTextColor4}]}>Non Citizen</Text>       
                      </View>
                    </TouchableHighlight>
                  </View>
                </View>
              </View>
              
              <Grid style={{flex: 0, marginBottom: 20}}>
                <Col><Text style={[css.b, {marginBottom: 15} ]}>Save to beneficiary list</Text></Col>
                <Col style={{alignItems: 'flex-end'}}>
                  <TouchableHighlight onPress={() => this.handleSave()} style={{backgroundColor: this.state.onOffBackground , borderRadius: 3, alignItems: 'center', padding: 10, paddingBottom: 7, elevation: 2}}>
                    <View style={{alignItems: 'center'}}>
                      <Text style={[css.b, css.f18, {marginBottom: 3, color: this.state.onOffColor}]}>{this.state.onOffText}</Text>
                      <View style={{height: 5, width: 70, borderWidth: this.state.onOffWidth, backgroundColor: '#fff', borderRadius: 30}} />
                    </View>
                  </TouchableHighlight>
                </Col>
              </Grid>
              <View style={[css.labelInputInline, css.singleFormGroup, {marginBottom: 0}]}>
                <TouchableOpacity style={[{flex: 1, backgroundColor: '#a71e23', borderRadius: 4, height: 45, marginRight: 15, alignItems: 'center', justifyContent: 'center'}]}>
                  <Text style={[css.b, {color: '#fff'}]}>Send OTP</Text>
                </TouchableOpacity>
                <View style={{flex: 1.4}}>
                  <TextInput underlineColorAndroid='transparent' style={[css.textInput]} 
                  onChangeText={(text) => this.setState({text})} secureTextEntry keyboardType='numeric' maxLength={6} />
                </View>
              </View>
              <View style={[css.labelInputInline, css.singleFormGroup, {justifyContent: 'flex-start'}]}>
                <View style={[{flex: 1, borderRadius: 4, height: 0, marginRight: 15, alignItems: 'center', justifyContent: 'center'}]}>
                </View>
                <View style={{flex: 1.4}}>
                  <Text style={[css.darkRed, {fontSize: 12, textAlign: 'center', justifyContent: 'flex-start', marginTop: 5}]}>Check your message for OTP</Text>
                </View>
              </View>
            </View>

            <View style={{marginBottom: 20}}>
              <TouchableOpacity onPress={() => this.openModal()} style={[css.button, {flex: 0, paddingVertical: 12, marginTop: 25}]}>
                <Text style={css.buttonText}>Submit</Text>
              </TouchableOpacity>
            </View>
          </Content>
          <Modal transparent={true} visible={this.state.modalVisible} onRequestClose={() => this.closeModal()} animationType={'none'}>
            <TouchableHighlight onPress={() => this.handleBenAdded()} style={[styles.modalContainer, {flex: 1, backgroundColor: 'rgba(47,48,48,.3)'}]}>
              <View style={styles.MainContainer}>
               <SvgUri width="50" height="50" source={success01} />
               <Text style={[css.b, {textAlign: 'center', fontSize: 16, maxWidth: 250, marginTop: 15}]}>Beneficiary contact has been submitted</Text>
               <Text style={[{textAlign: 'center', fontSize: 16, maxWidth: 250, color: '#080909'}]}>You will be redirected back to the form</Text>
              </View>
            </TouchableHighlight>         
          </Modal>
        </View>
      </StyleProvider>
    );
  }
}

const styles = StyleSheet.create({
  modalContainer: {
    backgroundColor: 'transparent',
    paddingHorizontal: 20,
    justifyContent: 'center'
  },

  MainContainer :{
    justifyContent: 'center',
    alignItems: 'center',
    padding: 20,
    backgroundColor: '#fff',
    borderRadius: 3,
    elevation: 2,
  },
});

function mapStateToProps(state) {
  return {
    domestic: state.domestic
  }
}

export default connect(mapStateToProps, { newDomesticBeneficiary })(DomNewBen1);