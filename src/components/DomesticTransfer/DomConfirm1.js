import React from 'react';
import { StyleSheet, Text, View, TouchableOpacity, TextInput, Picker, DatePickerAndroid } from 'react-native';
import SvgUri from 'react-native-svg-uri';
import HeaderPage from '../HeaderPage';
import { css } from '../Styles';
import { Grid, Col, Content } from 'native-base';
import { NavigationActions } from "react-navigation";
import { connect } from 'react-redux';

class DomConfirm1 extends React.Component {
  constructor(props) {
    super(props);
    this.state = { 
      text: '',
    };
  }
  render() {
    let { name, id, name2, id2, service, currency, amount, message1, message2, message3, email, date } = this.props.domestic;

    let amountConvert = amount;
    let totalDebit = parseInt(amountConvert);
    totalDebit = totalDebit + 5000;
    totalDebit = totalDebit.toString();
    totalDebit = totalDebit.replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
    amountConvert = amountConvert.replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");

    //Month
    const fields = date.split('/');
    let month = fields[1];

    if (month == '1') {month = 'Januari'}
    else if (month == '2') {month = 'Februari'}
    else if (month == '3') {month = 'Maret'}
    else if (month == '4') {month = 'April'}
    else if (month == '5') {month = 'Mei'}
    else if (month == '6') {month = 'Juni'}
    else if (month == '7') {month = 'Juli'}
    else if (month == '8') {month = 'Agustus'}
    else if (month == '9') {month = 'September'}
    else if (month == '10') {month = 'Oktober'}
    else if (month == '11') {month = 'November'}
    else if (month == '12') {month = 'Desember'}

    date = fields[0] + ' ' + month + ' ' + fields[2];

    return (
      <View style={{flex: 1, backgroundColor: '#eef0f0'}}>
        <Content showsVerticalScrollIndicator={false}>
          <View style={css.container}>
            <Grid style= {{marginHorizontal: 35, marginBottom: 10}}>
              <Col style={{flex: .5,}}><View style={[css.circleViewActive]}><Text style={[css.circleTextActive]}>1</Text></View></Col>
              <Col style={{flex: 2, borderTopWidth: 1, marginTop: 17.5, borderColor: '#cecece'}}></Col>
              <Col style={{flex: .5,}}><View style={css.circleView}><Text style={css.circleText}>2</Text></View></Col>
              <Col style={{flex: 2, borderTopWidth: 1, marginTop: 17.5, borderColor: '#cecece'}}></Col>
              <Col style={{flex: .5, marginRight: 16}}><View style={css.circleView}><Text style={css.circleText}>3</Text></View></Col>
            </Grid>
            <Grid style= {{marginBottom: 25}}>
              <Col style={{alignItems: 'center'}}><Text style={[css.b, css.darkRed]}>Confirmation</Text></Col>
              <Col style={{alignItems: 'center'}}><Text style={{fontWeight: 'bold'}}>Authentication</Text></Col>
              <Col style={{alignItems: 'center'}}><Text style={{fontWeight: 'bold'}}>Submitted</Text></Col>
            </Grid>
            <Grid style={{flex: 0, marginBottom: 20}}>
              <Col><Text style={[css.b, {marginBottom: 15} ]}>Transfer From</Text></Col>
              <Col style={{alignItems: 'flex-end'}}>
                <Text style={[css.b, css.darkRed, {fontSize: 18}]}>{name}</Text>
                <Text style={[css.darkRed, {fontSize: 16}]}>{id}</Text>
              </Col>
            </Grid>
            <Grid style={{flex: 0, marginBottom: 20}}>
              <Col style={{flex: 0.5}}><Text style={[css.b, {marginBottom: 15} ]}>Transfer To</Text></Col>
              <Col style={{alignItems: 'flex-end'}}>
                <Text style={[css.b, css.darkRed, {fontSize: 18}]}>{name2}</Text>
                <Text style={[css.darkRed, {fontSize: 16}]}>{id2}</Text>
              </Col>
            </Grid>
            <Grid style={{flex: 0, marginBottom: 20}}>
              <Col><Text style={[css.b, {marginBottom: 15} ]}>Services</Text></Col>
              <Col style={{alignItems: 'flex-end'}}><Text style={[css.b, css.p, css.darkRed]}>{service}</Text></Col>
            </Grid>
            <Grid style={{flex: 0, marginBottom: 20}}>
              <Col><Text style={[css.b, {marginBottom: 15} ]}>Amount</Text></Col>
              <Col style={{alignItems: 'flex-end'}}><Text style={[css.p, css.darkRed]}>{currency} <Text style={[css.b, css.darkRed, {fontSize: 20}]}> {amountConvert}</Text>.00</Text></Col>
            </Grid>
            <Grid style={{flex: 0, marginBottom: 20}}>
              <Col>
                <Text style={[css.b, {marginBottom: 15} ]}>Message</Text>
                <Text style={[css.p, css.darkRed , {marginBottom: 15, marginLeft: 15} ]}>{message1}</Text>
              </Col>
            </Grid>
            <Grid style={{flex: 0, marginBottom: 20}}>
              <Col style={{flex: 0.5}}><Text style={[css.b, {marginBottom: 15} ]}>Beneficiary Email</Text></Col>
              <Col style={{alignItems: 'flex-end'}}><Text style={[css.p, css.darkRed]}>{email}</Text></Col>
            </Grid>
            <Grid style={{flex: 0, marginBottom: 20}}>
              <Col><Text style={[css.b, {marginBottom: 15} ]}>Total Charges</Text></Col>
              <Col style={{alignItems: 'flex-end'}}><Text style={[css.p, css.darkRed]}>IDR <Text style={[css.b, css.darkRed, {fontSize: 20}]}>5,000</Text>.00</Text></Col>
            </Grid>
            <Grid style={{flex: 0, marginBottom: 20}}>
              <Col><Text style={[css.b, {marginBottom: 15} ]}>Total Debited Amount</Text></Col>
              <Col style={{alignItems: 'flex-end'}}><Text style={[css.p, css.darkRed]}>IDR <Text style={[css.b, css.darkRed, {fontSize: 20}]}>{totalDebit}</Text>.00</Text></Col>
            </Grid>
            <Grid style={{flex: 0, marginBottom: 20}}>
              <Col><Text style={[css.b, {marginBottom: 15} ]}>Beneficiary Email</Text></Col>
              <Col style={{alignItems: 'flex-end'}}><Text style={[css.p, css.darkRed]}>{date}</Text></Col>
            </Grid>

            <TouchableOpacity onPress={() => this.props.navigation.navigate("DomConfirm2")} style={[css.button, {paddingVertical: 12, marginTop: 0}]}>
              <Text style={css.buttonText}>Send OTP</Text>
            </TouchableOpacity>
          </View>
        </Content>
      </View>
    );
  }
}

DomConfirm1.navigationOptions = ({ navigation }) => ({
  header: (
    <HeaderPage goBack={() => navigation.dispatch(NavigationActions.back())} title={'Confirmation'} navigations={() => navigation.navigate("DrawerOpen")} />
  )
});

function mapStateToProps(state) {
  return {
    domestic: state.domestic
  }
}

export default connect(mapStateToProps)(DomConfirm1);