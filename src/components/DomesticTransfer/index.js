import React, { Component } from "react";
import DomesticTransfer from './DomesticTransfer';
import DomNewBen1 from './DomNewBen1';
import DomConfirm1 from './DomConfirm1';
import DomConfirm2 from './DomConfirm2';
import DomSubmitted from './DomSubmitted';
import HomeMenu from '../HomeMenu';
import { StackNavigator } from "react-navigation";
import CardStackStyleInterpolator from 'react-navigation/src/views/CardStack/CardStackStyleInterpolator';

export default (DrawNav = StackNavigator(
	{
	  DomesticTransfer: { screen: DomesticTransfer },
	  DomNewBen1: { screen: DomNewBen1 },
	  DomConfirm1: { screen: DomConfirm1 },
	  DomConfirm2: { screen: DomConfirm2 },
	  DomSubmitted: { screen: DomSubmitted },
	  HomeMenu: { screen: HomeMenu },
	},
	{
		transitionConfig: ()=> {
			return {screenInterpolator: CardStackStyleInterpolator.forHorizontal}
		}
	}
));