import React from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity, Linking } from 'react-native';
import SvgUri from 'react-native-svg-uri';
import HomeMenu from './HomeMenu';
import DeviceRegistration from './DeviceRegistration';
import DeviceRegistrationOTP from './DeviceRegistrationOTP';
import { Container, Header, Title, Left, Icon, Right, Button, Body, Content, Card, CardItem } from "native-base";
import * as Progress from 'react-native-progress';
import { BMLogo } from 'helpers/images';

export default class SplashScreen extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      progress: 0,
      indeterminate: true,
      isMounted: false,
    };
  }

  componentDidMount() {
    this.animate();
  }

  animate() {
    let progress = 0;
    let prog = 0
    this.setState({ progress });
    this.timeoutHandle = setTimeout(() => {
      this.setState({ indeterminate: false });
      setInterval(() => {
        progress += Math.random() / 3;
        prog += prog + 1;
        if (progress > 1) {
          progress = 1;
        }
        if (this.refs.myRef) {
          this.setState({ progress });  
        } 
      }, 500);
    }, 0);
  }

  render() {
    return (
      <View ref='myRef' style={{flex: 1, backgroundColor: '#8a181b', padding: 4, paddingBottom: 6}}>
        <Header androidStatusBarColor={'#8a181b'} style={{backgroundColor: '#a71e23', height: 0}}/>
        <View style={styles.container}>
          <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
            <Image source={BMLogo}></Image>
            <Progress.Bar
              style={styles.progress}
              progress={this.state.progress}
              indeterminate={this.state.indeterminate}
              color={'#a71e23'}
              unfilledColor={'#c1c1c1'}
              borderWidth={0}
              width={250}
              height={1}
            />
          </View>
        </View>
        <Text style={{color: '#a71e23', textAlign: 'center', fontSize: 12, paddingBottom: 10, backgroundColor: '#eef0f0'}}>
        Copyright @ 2017, Bank Mayora{"\n"}
        Designed by PT. Aprisma Indonesia, All right reserved
        </Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#eef0f0',
    // justifyContent: 'center',
    paddingHorizontal: 50,
    borderTopLeftRadius: 30,
  },

  content: {

  },

  optionBox: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#fff',
    maxHeight: 150,
    elevation: 2,
  },

  // Progress Bar
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  circles: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  progress: {
    margin: 10,
  }
});