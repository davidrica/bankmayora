import React from 'react';
import { StyleSheet, Text, View, TouchableOpacity, TextInput, Picker, DatePickerAndroid } from 'react-native';
import SvgUri from 'react-native-svg-uri';
import HeaderPage from '../HeaderPage';
import { css } from '../Styles';
import { Icon } from 'native-base';
import { NavigationActions } from "react-navigation";

export default class NewBeneficiaryContact extends React.Component {
  constructor(props) {
    super(props);
    this.state = { 
      text: '',
    };
  }

  static navigationOptions = ({ navigation }) => ({
    header: (
      <HeaderPage goBack={() => navigation.goBack()} title={'Inhouse Transfer'} navigations={() => navigation.navigate("DrawerOpen")} />
    )
  });

  render() {
    return (
      <View style={{flex: 1, backgroundColor: '#eef0f0'}}>
        <View style={css.container}>
          <View style={{flex: 1}}>
            <Text style={[css.b, {marginBottom: 15} ]}>Transfer To - <Text style={[css.b, css.red, {fontSize: 16}]}>New Beneficiary Contact</Text></Text>
            <View style={[css.labelInputInline, css.singleFormGroup]}>
              <Text style={[css.b, {flex: 1}]}>Account Number</Text>
              <TextInput keyboardType='numeric' maxLength={16} underlineColorAndroid='transparent' style={[css.textInput, {flex: 1.4}]} 
              onChangeText={(text) => this.setState({text})} />
            </View>
          </View>

          <View style={{marginBottom: 20}}>
            <TouchableOpacity onPress={() => this.props.navigation.navigate("NewBeneficiaryContact2")} style={[css.button, {flex: 0, paddingVertical: 12}]}>
              <Text style={css.buttonText}>Submit</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  }
}