import React from 'react';
import { StyleSheet, Text, View, TouchableOpacity, TextInput, TouchableHighlight, Modal } from 'react-native';
import HeaderPage from '../HeaderPage';
import { css } from '../Styles';
import { Grid, Col, Content} from 'native-base';
import { NavigationActions } from "react-navigation";
import SvgUri from 'react-native-svg-uri';
import { success01 } from 'helpers/images';

export default class NewBeneficiaryContact2 extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      onOffBackground: '#fff',
      onOffText: 'OFF',
      onOffColor: '#080909',
      onOffWidth: 1,
      onOffBorderColor: '#080909',
      modalVisible: false,
    };
  }

  handleSave() {
    this.state.onOffBackground == '#fff' ? this.setState({
      onOffBackground: '#a71e23', onOffText: 'ON', onOffColor: '#fff', onOffWidth: 0
    })  : this.setState({
      onOffBackground: '#fff', onOffText: 'OFF', onOffColor: '#080909', onOffWidth: 1
    });
  }

  handleBenAdded(){
    this.setState({
      modalVisible: false,
    })
    this.props.navigation.navigate('InhouseTransfer');
  }

  openModal() {
    this.setState({modalVisible:true});
  }

  closeModal() {
    this.setState({modalVisible:false});
  }

  static navigationOptions = ({ navigation }) => ({
    header: (
      <HeaderPage goBack={() => navigation.goBack()} title={'Inhouse Transfer'} navigations={() => navigation.navigate("DrawerOpen")} />
    )
  });

  render() {
    return (
      <Content showsVerticalScrollIndicator={false} style={{flex: 1, backgroundColor: '#eef0f0'}}>
        <View style={css.container}>
          <View style={{flex: 1}}>
            <Text style={[css.b, {marginBottom: 15} ]}>Transfer To - <Text style={[css.b, css.red, {fontSize: 16}]}>New Beneficiary Contact</Text></Text>
            <Grid style={{flex: 0, marginBottom: 20}}>
              <Col><Text style={[css.b, {marginBottom: 15} ]}>Account Name</Text></Col>
              <Col style={{alignItems: 'flex-end', marginRight: 20,}}><Text style={[css.b, css.red, {fontSize: 20}]}>David Mayora</Text></Col>
            </Grid>
            <Grid style={{flex: 0, marginBottom: 20}}>
              <Col><Text style={[css.b, {marginBottom: 15} ]}>Account Number</Text></Col>
              <Col style={{alignItems: 'flex-end', marginRight: 20,}}><Text style={[css.b, css.red, {fontSize: 20}]}>189362491773</Text></Col>
            </Grid>
            <Grid style={{flex: 0, marginBottom: 20}}>
              <Col><Text style={[css.b, {marginBottom: 15} ]}>Save to beneficiary list</Text></Col>
              <Col style={{alignItems: 'flex-end'}}>
                <TouchableHighlight onPress={this.handleSave.bind(this)} style={{backgroundColor: this.state.onOffBackground , borderRadius: 3, alignItems: 'center', padding: 10, paddingBottom: 7, elevation: 2}}>
                  <View style={{alignItems: 'center'}}>
                    <Text style={[css.b, css.f18, {marginBottom: 3, color: this.state.onOffColor}]}>{this.state.onOffText}</Text>
                    <View style={{height: 5, width: 70, borderWidth: this.state.onOffWidth, backgroundColor: '#fff', borderRadius: 30}} />
                  </View>
                </TouchableHighlight>
              </Col>
            </Grid>
            <View style={[css.labelInputInline, css.singleFormGroup, {marginBottom: 0}]}>
              <TouchableOpacity style={[{flex: 1, backgroundColor: '#a71e23', borderRadius: 4, height: 45, marginRight: 15, alignItems: 'center', justifyContent: 'center'}]}>
                <Text style={[css.b, {color: '#fff'}]}>Send OTP</Text>
              </TouchableOpacity>
              <View style={{flex: 1.4}}>
                <TextInput underlineColorAndroid='transparent' style={[css.textInput]} 
                onChangeText={(text) => this.setState({text})} secureTextEntry keyboardType='numeric' maxLength={6} />
              </View>
            </View>
            <View style={[css.labelInputInline, css.singleFormGroup, {justifyContent: 'flex-start'}]}>
              <View style={[{flex: 1, borderRadius: 4, height: 0, marginRight: 15, alignItems: 'center', justifyContent: 'center'}]}>
              </View>
              <View style={{flex: 1.4}}>
                <Text style={[css.darkRed, {fontSize: 12, textAlign: 'center', justifyContent: 'flex-start', marginTop: 5}]}>Check your message for OTP</Text>
              </View>
            </View>
            <View style={{marginTop: 75}}>
              <TouchableOpacity onPress={() => this.openModal()} style={[css.button, {flex: 0, paddingVertical: 12}]}>
                <Text style={css.buttonText}>Submit</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
        <Modal transparent={true} visible={this.state.modalVisible} onRequestClose={() => this.closeModal()} animationType={'none'}>
          <TouchableHighlight onPress={() => this.handleBenAdded()} style={[styles.modalContainer, {flex: 1, backgroundColor: 'rgba(47,48,48,.3)'}]}>
            <View style={styles.MainContainer}>
             <SvgUri width="50" height="50" source={success01} />
             <Text style={[css.b, {textAlign: 'center', fontSize: 16, maxWidth: 250, marginTop: 15}]}>Beneficiary contact has been submitted</Text>
             <Text style={[{textAlign: 'center', fontSize: 16, maxWidth: 250, color: '#080909'}]}>You will be redirected back to the form</Text>
            </View>
          </TouchableHighlight>         
        </Modal>
      </Content>
    );
  }
}

const styles = StyleSheet.create({
  modalContainer: {
    backgroundColor: 'transparent',
    paddingHorizontal: 20,
    justifyContent: 'center'
  },

  MainContainer :{
    justifyContent: 'center',
    alignItems: 'center',
    padding: 20,
    backgroundColor: '#fff',
    borderRadius: 3,
    elevation: 2,
  },
});