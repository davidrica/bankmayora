import React from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity, Alert, BackHandler } from 'react-native';
import SvgUri from 'react-native-svg-uri';
import HeaderHome from './HeaderHome';
import { Container, Header, Title, Left, Icon, Right, Button, Body, Content, Card, CardItem } from "native-base";
import { balance01, inhouseTransfer01, domesticTransferTriangleWhiteInside, billPayment01, purchaseNew01, fastTransaction,
  transactionStatusChange01, inquiry01 } from 'helpers/images';

export default class HomeMenu extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      homeMenu: true,
    };
    this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
  }

  componentWillMount() {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
}

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
  }

  handleBackButtonClick() {
    console.log('back!');
    return true;
  }
    
  render() {
    const { navigate } = this.props.navigation;
    return (
      <View style={{flex: 1, backgroundColor: '#eef0f0'}}>
        <HeaderHome navigations={() => this.props.navigation.navigate("DrawerOpen")} />

        <View style={{flex: 6, marginHorizontal: 20, marginVertical: 20, flexWrap: 'wrap'}}>
          <View style={styles.rowPannel}>
            <TouchableOpacity onPress={() => navigate("Accounts")} style={styles.pannelLeft}>
              <SvgUri width='60' height='60' source={balance01}/>
              <Text style={[styles.textPannel, {marginTop: 8}]}>Accounts</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => navigate("InhouseTransfer")} style={styles.pannelRight}>
              <SvgUri width='60' height='60' source={inhouseTransfer01}/>
              <Text style={[styles.textPannel, {marginTop: 8}]}>Inhouse Transfer</Text>
            </TouchableOpacity>
          </View>

          <View style={styles.rowPannel}>
            <TouchableOpacity onPress={() => navigate("DomesticTransfer")} style={styles.pannelLeft}>
              <SvgUri width='60' height='60' source={domesticTransferTriangleWhiteInside}/>
              <Text style={[styles.textPannel, {marginTop: 8}]}>Domestic Transfer</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => navigate("BillPayment")}style={styles.pannelRight}>
              <SvgUri width='70' height='70' source={billPayment01}/>
              <Text style={[styles.textPannel, {marginTop: 0}]}>Bill Payment</Text>
            </TouchableOpacity>
          </View>

          <View style={styles.rowPannel}>
            <TouchableOpacity onPress={() => navigate("Purchase")}style={styles.pannelLeft}>
              <SvgUri width='60' height='60' source={purchaseNew01}/>
              <Text style={[styles.textPannel, {marginTop: 8}]}>Purchase</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.pannelRight}>
              <SvgUri width='70' height='70' source={fastTransaction}/>
              <Text style={[styles.textPannel, {marginTop: 0}]}>Fast Transaction</Text>
            </TouchableOpacity>
          </View>

          <View style={[styles.rowPannel, {marginBottom: 0}]}>
            <TouchableOpacity onPress={() => navigate("TransactionStatus")}style={styles.pannelLeft}>
              <SvgUri width='60' height='60' source={transactionStatusChange01}/>
              <Text style={[styles.textPannel, {marginTop: 8}]}>Transaction Status</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => navigate("Inquiry")}style={styles.pannelRight}>
              <SvgUri width='60' height='60' source={inquiry01}/>
              <Text style={[styles.textPannel, {marginTop: 8}]}>Inquiry</Text>
            </TouchableOpacity>
          </View>

        </View>
      </View>
    );
  }
}

HomeMenu.navigationOptions = ({ navigation }) => ({
  header: null
});

const styles = StyleSheet.create({
  rowPannel: {
    flex: 1,
    flexDirection: 'row',
    marginBottom: 20,
  },
  pannelLeft: {
    flex: 1,
    marginRight: 20,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff',
    elevation: 2,
  },
  pannelRight: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff',
    elevation: 2,
  },
  textPannel: {
    color: '#a71e23',
    fontSize: 18,
    fontWeight: 'bold'
  }
});