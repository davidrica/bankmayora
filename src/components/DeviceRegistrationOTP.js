import React from 'react';
import { StyleSheet, Text, View, Image, FormInput, TouchableHighlight, Modal, Button, TouchableOpacity, TouchableWithoutFeedback, 
  TextInput } from 'react-native';
import { Header, Form, Item, Input } from 'native-base';
import { css } from './Styles';
import SvgUri from 'react-native-svg-uri';
import { bm01, success01 } from 'helpers/images';
import { connect } from 'react-redux';
import { deviceRegistrationSubmit } from 'helpers/redux/actions';
 
class DeviceRegistrationOTP extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      modalVisible: false,
      text: '',
    };
  }

  openModal() {
    this.setState({modalVisible:true});
  }

  closeModal() {
    this.setState({modalVisible:false});
  }

  authenticate() {
    this.props.deviceRegistrationSubmit();
    this.openModal();
  }

  render() {
    return (
      <View style={{flex: 1, backgroundColor: '#8a181b', padding: 4, paddingBottom: 6}}>
        <Header androidStatusBarColor={'#8a181b'} style={{backgroundColor: '#a71e23', height: 0}}/>
        <View style={[styles.container]}>
          <Image source={bm01} 
          style={{width:200, resizeMode: 'contain', alignSelf: 'flex-end', marginRight: -10, marginTop: 5}}/>
          <View style={{flex: 2,justifyContent: 'center'}}>
            <View style={[{alignItems: 'center'}]}>
              <Text style={[css.b, css.red, {marginBottom: 10, marginTop: 35, fontSize: 16}]}>Input OTP Code</Text>
              <Form style={{width: 95, paddingLeft: 0, marginLeft: 0}}>
                <Item style={{paddingLeft: 0, marginLeft: 0}}>
                  <Input style={[css.red, {textAlign: 'center', fontSize: 24}]} placeholder="" onChangeText={(text) => this.setState({text})} secureTextEntry keyboardType='numeric' maxLength={6} />
                </Item>
              </Form>
            </View>

            <Text style={[css.darkRed, {marginBottom: 20, marginTop: 50, fontSize: 12, textAlign: 'center'}]}>Check your message for OTP</Text>

            <TouchableOpacity style={[css.buttonGrey, {paddingVertical: 12, marginTop: 0}]}>
              <Text style={css.buttonTextGrey}>Send OTP</Text>
            </TouchableOpacity>

            <TouchableOpacity onPress={() => this.authenticate()} style={[css.button, {paddingVertical: 12, marginTop: 0}]}>
              <Text style={css.buttonText}>Authenticate</Text>
            </TouchableOpacity>
          </View>

          <Modal transparent={true} visible={this.state.modalVisible} onRequestClose={() => this.closeModal()} animationType={'none'}>
            <TouchableHighlight onPress={() => this.closeModal()} style={[styles.modalContainer, {flex: 1, backgroundColor: 'rgba(0,0,0,.6)'}]}>
              <View style={styles.MainContainer}>
               <SvgUri width="50" height="50" source={success01} />
               <Text style={[css.b, {textAlign: 'center', fontSize: 16, maxWidth: 250, marginTop: 15}]}>Your device has been registered</Text>
               <Text style={[{textAlign: 'center', fontSize: 16, maxWidth: 250, color: '#080909'}]}>Device ID : 01364798634929891</Text>
               <TouchableOpacity onPress={this.props.onLogin} style={[css.button, {marginTop: 20}]}>
                 <Text style={[css.buttonText, {fontSize: 16}]}>Log in to mobile banking</Text>
               </TouchableOpacity>
              </View>
            </TouchableHighlight>         
          </Modal>
        </View>
        <Text style={{color: '#a71e23', textAlign: 'center', fontSize: 12, paddingBottom: 10, backgroundColor: '#eef0f0'}}>
        Copyright @ 2017, Bank Mayora{"\n"}
        Designed by PT. Aprisma Indonesia, All right reserved
        </Text>
      </View>
    );
  }
}

// Styles

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#eef0f0',
    // justifyContent: 'center',
    paddingHorizontal: 20,
    borderTopLeftRadius: 30,
  },

  content: {

  },

  optionBox: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#fff',
    maxHeight: 150,
    elevation: 2,
  },
  modalContainer: {
    backgroundColor: 'transparent',
    paddingHorizontal: 20,
    justifyContent: 'center'
  },

  MainContainer :{
    justifyContent: 'center',
    alignItems: 'center',
    padding: 20,
    paddingBottom: 10,
    backgroundColor: '#fff',
    borderRadius: 3,
    elevation: 2,
  },
});

function mapStateToProps(state) {
  return {
    deviceRegistration: state.deviceRegistration
  }
}

export default connect(mapStateToProps, {deviceRegistrationSubmit})(DeviceRegistrationOTP);