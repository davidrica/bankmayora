import React, { Component } from 'react';
import {
  Text,
} from 'react-native';

import {Header,Left,Button,Icon,Right,Body,Title, Content} from 'native-base';
import {Drawer} from 'native-base';

class Sidebar extends Component {
  render() {
    return (
          <Content style={{backgroundColor:'#FFFFFF'}}>
            <Text>Drawer</Text>
          </Content>
    );
  }
}

class AppHeader extends Component {
  render() {
    return (
      <Header>
       <Left>
       <Button transparent
              onPress={()=>this.props.openDrawer()}
       >
         <Icon name='menu' />
       </Button>
       </Left>
       <Body>
         <Title>SDCC Wallet</Title>
       </Body>
       <Right>
         <Button transparent>
           <Icon name='bulb' />
         </Button>
       </Right>
     </Header>
    );
  }
}

export default class NativeBaseCRNA extends Component {
	constructor() {
    super();
    this.state = {
      isReady: false
    };
  }
  componentWillMount() {
    this.loadFonts();
  }
  async loadFonts() {
    await Expo.Font.loadAsync({
      Roboto: require("native-base/Fonts/Roboto.ttf"),
      Roboto_medium: require("native-base/Fonts/Roboto_medium.ttf"),
      Ionicons: require("@expo/vector-icons/fonts/Ionicons.ttf")
    });
    this.setState({ isReady: true });
  }

  closeDrawer = () => {
    this.drawer._root.close()
  };
  openDrawer = () => {
    this.drawer._root.open()
  };
  render() {
  	if (this.state.isReady) {
      return (
      <Drawer
        ref={(ref) => { this.drawer = ref; }}
        content={<Sidebar/>}
        onClose={() => this.closeDrawer()} >

        <AppHeader
            openDrawer={this.openDrawer.bind(this)}
        />
        </Drawer>
    );
    }
    
  }
}