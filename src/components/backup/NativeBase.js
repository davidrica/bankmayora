// import React from 'react';
// import { StyleSheet, Text, View, Alert } from 'react-native';
// import * as Expo from "expo";
// import { StyleProvider, Container, Button } from "native-base";

// export default class NativeBase extends React.Component {

//   state = { fontsAreLoaded: false };

//   async componentWillMount() {
//       await Expo.Font.loadAsync({
//         'Roboto': require('native-base/Fonts/Roboto.ttf'),
//         'Roboto_medium': require('native-base/Fonts/Roboto_medium.ttf'),
//       });
//       this.setState({fontsAreLoaded: true});
//   }


//   render() {
//     if (this.state.fontsAreLoaded) {
//       return (
//       <Container>
//         <Button>
//           <Text>Open up main.js to start working on your app!</Text>     
//         </Button>
//       </Container>
//     );
//     }
//     return (
//       <View>
//         <Text>Open up main.js to start working on your app!</Text>
//       </View>
//     );
//   }
// }





// import React, { Component } from 'react';
// import { Drawer, Text } from 'native-base';
// import BillPayment from './BillPayment';
// export default class NativeBase extends Component {

//   state = { fontsAreLoaded: false };

//   async componentWillMount() {
//       await Expo.Font.loadAsync({
//         'Roboto': require('native-base/Fonts/Roboto.ttf'),
//         'Roboto_medium': require('native-base/Fonts/Roboto_medium.ttf'),
//       });
//       this.setState({fontsAreLoaded: true});
//   }


//   render() {
//     closeDrawer = () => {
//       this.drawer._root.close()
//     };
//     openDrawer = () => {
//       this.drawer._root.open()
//     };
//     return (

//       <Drawer
//         ref={(ref) => { this.drawer = ref; }}
//         content={<BillPayment navigator={this.navigator} />}
//         onClose={() => this.closeDrawer()} >
//         <Text>Main Viewawefweasdfoij</Text>
//         <Text>Main Viewawefweasdfoij</Text>
//         <Text>Main Viewawefweasdfoij</Text>
//         <Text>Main Viewawefweasdfoij</Text>
//       </Drawer>
//     );
//   }
// }





// import React, { Component } from 'react';
// import { Container, Header, Content, H1, H2, H3, Text } from 'native-base';
// export default class TypographyExample extends Component {
//   state = { fontsAreLoaded: false };

//   async componentWillMount() {
//       await Expo.Font.loadAsync({
//         'Roboto': require('native-base/Fonts/Roboto.ttf'),
//         'Roboto_medium': require('native-base/Fonts/Roboto_medium.ttf'),
//       });
//       this.setState({fontsAreLoaded: true});
//   }
//   render() {
//     return (
//       <Container>
//         <Header />
//         <Content>
//           <H1>Header One</H1>
//           <H2>Header Two</H2>
//           <H3>Header Three</H3>
//           <Text>Default</Text>
//         </Content>
//       </Container>
//     );
//   }
// }



// import React, { Component } from 'react';
// import { Container, Header, Content, Toast, Button, Text, Icon } from 'native-base';
// export default class ToastExample extends Component {
//   state = { fontsAreLoaded: false, showToast: false };

//   async componentWillMount() {
//       await Expo.Font.loadAsync({
//         'Roboto': require('native-base/Fonts/Roboto.ttf'),
//         'Roboto_medium': require('native-base/Fonts/Roboto_medium.ttf'),
//       });
//       this.setState({fontsAreLoaded: true});
//   }

//   render() {
//     return (
//       <Container>
//         <Header />
//         <Content padder>
//           <Button onPress={()=> Toast.show({
//               text: 'Wrong password!',
//               position: 'bottom',
//               buttonText: 'Okay'
//             })}>
//             <Text>Toast</Text>
//           </Button>
//         </Content>
//       </Container>
//     );
//   }
// }






import React, { Component } from 'react';
import { View, Text } from 'react-native';
import { Container, Header, Left, Body, Right, Button, Icon, Title } from 'native-base';
import { getStatusBarHeight } from 'react-native-status-bar-height';

export default class HeaderExample extends Component {

  render() {
    return (
      <Container>
        <View style={{height: getStatusBarHeight(), backgroundColor: 'red'}}/>
        <Header>
          <Left>
            <Button transparent>
              <Icon name='arrow-back' />
            </Button>
          </Left>
          <Body>
            <Title>Headers</Title>
          </Body>
          <Right>
            <Button transparent>
              <Icon name='menu' />
            </Button>
          </Right>
        </Header>
      </Container>
    ); 
  }
}