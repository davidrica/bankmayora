import React from 'react';
import { StyleSheet, Text, View, TouchableOpacity, TextInput, Picker, DatePickerAndroid, ScrollView } from 'react-native';
import SvgUri from 'react-native-svg-uri';
import HeaderPage from './HeaderPage';
import { css } from './Styles';
import { Icon } from 'native-base';
// import { Icon } from 'react-native-elements';

export default class DomesticTransfer extends React.Component {
  constructor(props) {
    super(props);
    this.state = { 
      text: '',
      service: 'Online',
      simpleText: 'DD//MM/YYYY',
    };
  }

  static title = 'DatePickerAndroid';
  static description = 'Standard Android date picker dialog';

  state = {
    presetDate: new Date(2020, 4, 5),
    simpleDate: new Date(2020, 4, 5),
    spinnerDate: new Date(2020, 4, 5),
    calendarDate: new Date(2020, 4, 5),
    defaultDate: new Date(2020, 4, 5),
    allDate: new Date(2020, 4, 5),
    simpleText: 'pick a date',
    spinnerText: 'pick a date',
    calendarText: 'pick a date',
    defaultText: 'pick a date',
    minText: 'pick a date, no earlier than today',
    maxText: 'pick a date, no later than today',
    presetText: 'pick a date, preset to 2020/5/5',
    allText: 'pick a date between 2020/5/1 and 2020/5/10',
  };

  showPicker = async (stateKey, options) => {
    try {
      var newState = {};
      const {action, year, month, day} = await DatePickerAndroid.open(options);
      if (action === DatePickerAndroid.dismissedAction) {
        newState[stateKey + 'Text'] = 'dismissed';
      } else {
        var date = new Date(year, month, day);
        newState[stateKey + 'Text'] = date.toLocaleDateString();
        newState[stateKey + 'Date'] = date;
      }
      this.setState(newState);
    } catch ({code, message}) {
      console.warn(`Error in example '${stateKey}': `, message);
    }
  };

  render() {
    const { navigate } = this.props.navigation;
    return (
      <View style={{flex: 1, backgroundColor: '#eef0f0'}}>
        <HeaderPage onBack={this.props.onBack} title={'Domestic Transfer'} navigations={() => this.props.navigation.navigate("DrawerOpen")} />
        <ScrollView showsVerticalScrollIndicator={false}> 
          <View style={[css.container]}>
            <View style={css.singleFormGroup}>
              <Text style={[css.b, {marginBottom: 15}]}>Transfer From</Text>
              <TextInput underlineColorAndroid='transparent' style={css.textInput}
              onChangeText={(text) => this.setState({text})} />
              <Icon name='md-search' style={{color:'#cecece', fontSize: 27, elevation: 99, position: 'absolute', bottom: 9, right: 22}}/>
            </View>

            <View style={css.singleFormGroup}>
              <Text style={[css.b, {marginBottom: 15}]}>Transfer To</Text>
              <TextInput underlineColorAndroid='transparent' style={css.textInput} 
              onChangeText={(text) => this.setState({text})} />
              <Icon name='md-search' style={{color:'#cecece', fontSize: 27, elevation: 99, position: 'absolute', bottom: 9, right: 22}}/>
            </View>

            <View style={[css.labelInputInline, css.singleFormGroup]}>
              <Text style={[css.b, {flex:2}]}>Service</Text>
              <Picker
                selectedValue={this.state.service}
                onValueChange={(itemValue, itemIndex) => this.setState({service: itemValue})}
                style={[css.textInput, {flex: 0.7, justifyContent: 'center', height: 45, color: '#a71e23'}]}>
                <Picker.Item label="  Online" value="online" />
                <Picker.Item label="  Offline" value="offline" />
              </Picker>
              <Icon name='md-arrow-dropdown' style={{color:'#a71e23', fontSize: 20, position: 'absolute', bottom: 11, right: 22, elevation: 1}}/>
            </View>

            <View style={[css.labelInputInline, css.singleFormGroup]}>
              <Text style={[css.b, {flex:2}]}>Currency</Text>
              <Picker
                selectedValue={this.state.language}
                onValueChange={(itemValue, itemIndex) => this.setState({language: itemValue})}
                style={[css.textInput, {flex: 0.7, justifyContent: 'center', height: 45, color: '#a71e23'}]}>
                <Picker.Item label="  IDR" value="idr" />
                <Picker.Item label="  USD" value="usd" />
                <Picker.Item label="  SGD" value="sgd" />
              </Picker>
              <Icon name='md-arrow-dropdown' style={{color:'#a71e23', fontSize: 20, position: 'absolute', bottom: 11, right: 22, elevation: 1}}/>
            </View>

            <View style={[css.labelInputInline, css.singleFormGroup]}>
              <Text style={[css.b, {flex: 1}]}>Amount</Text>
              <TextInput underlineColorAndroid='transparent' style={[css.textInput, {flex: 1.6}]} 
              onChangeText={(text) => this.setState({text})} />
            </View>

            <View style={css.singleFormGroup}>
              <Text style={[css.b, {marginBottom: 10}]}>Message</Text>
              <TextInput underlineColorAndroid='transparent' style={[css.textInput, {marginBottom: 7}]} 
              onChangeText={(text) => this.setState({text})} />
              <TextInput underlineColorAndroid='transparent' style={[css.textInput, {marginBottom: 7}]} 
              onChangeText={(text) => this.setState({text})} />
              <TextInput underlineColorAndroid='transparent' style={[css.textInput, {marginBottom: 7}]} 
              onChangeText={(text) => this.setState({text})} />
            </View>

            <View style={[css.labelInputInline, css.singleFormGroup]}>
              <Text style={[css.b, {flex: 1}]}>Beneficiary Email</Text>
              <TextInput underlineColorAndroid='transparent' style={[css.textInput,{flex: 1.7}]} 
              onChangeText={(text) => this.setState({text})} />
            </View>

            <View style={[css.labelInputInline, css.singleFormGroup]}>
              <Text style={[css.b, {flex: 1}]}>Transfer Date</Text>
              <TouchableOpacity onPress={this.showPicker.bind(this, 'simple', {date: this.state.simpleDate})} style={[css.textInput, {flex: 0.7, justifyContent: 'center'}]}> 
                <Text style={[css.red, {alignItems: 'center', fontSize: 16}]}>{this.state.simpleText}</Text>
                <Icon name='md-arrow-dropdown' style={{color:'#a71e23', fontSize: 20, position: 'absolute', bottom: 11, right: 22, elevation: 1}}/>
              </TouchableOpacity>
            </View>

            <TouchableOpacity style={[css.button, {flex: 0, justifyContent: 'flex-start', marginTop: 10, paddingVertical: 12}]}>
              <Text style={css.buttonText}>Confirm</Text>
            </TouchableOpacity>
          </View>
        </ScrollView>
      </View>
    );
  }
}