import React from 'react';
import { StyleSheet, Text, View, Image } from 'react-native';
import SvgUri from 'react-native-svg-uri';

export default class Test01 extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        <SvgUri width="200" height="200" source={require('../assets/images/icons/Untitled-1.svg')} />
        <SvgUri width="200" height="200" source={require('../assets/images/icons/Untitled-2.svg')} />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#eef0f0',
    alignItems: 'center',
    justifyContent: 'center',
  },
});