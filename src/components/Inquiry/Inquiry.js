import React from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity } from 'react-native';
import SvgUri from 'react-native-svg-uri';
import HeaderPage from '../HeaderPage';
import { css } from '../Styles';
import { Icon } from 'native-base';
import { NavigationActions } from "react-navigation";

export default class Inquiry extends React.Component {
  render() {
    const { navigate } = this.props.navigation;
    return (
      <View style={{flex: 1, backgroundColor: '#eef0f0'}}>
        <View style={[css.container,]}>

          <View style={{flex: 0, marginBottom: 25}}>
            <Text style={[css.h2, css.h2a, {marginBottom: 15}]}>Choose Inquiry</Text>
            <TouchableOpacity onPress={() => this.props.navigation.navigate("ForexRate")} style={[css.panel, {height: 60}]}>
              <View style={css.leftTextPanel}>
                <Text style={[css.h2, css.red]}>Forex Rate Inquiry</Text>
                <Icon name='md-arrow-dropright' style={{color:'#a71e23', fontSize: 24, position: 'absolute', bottom: -3, right: 0}}/>     
              </View>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => this.props.navigation.navigate("InterestRate")} style={[css.panel, {height: 60}]}>
              <View style={css.leftTextPanel}>
                <Text style={[css.h2, css.red]}>Interest Rate Inquiry</Text>
                <Icon name='md-arrow-dropright' style={{color:'#a71e23', fontSize: 24, position: 'absolute', bottom: -3, right: 0}}/>     
              </View>
            </TouchableOpacity>
          </View>

        </View>
      </View>
    );
  }
}

Inquiry.navigationOptions = ({ navigation }) => ({
  header: (
    <HeaderPage goBack={() => navigation.dispatch(NavigationActions.back())} title={'Inquiry'} navigations={() => navigation.navigate("DrawerOpen")} />
  )
});