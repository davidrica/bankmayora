import React from 'react';
import { StyleSheet, Text, View, TouchableOpacity, ScrollView} from 'react-native';
import { Header, Left, Body, Right, Button, Title, Icon, Content, Card, Container, List, ListItem, Grid, Col } from 'native-base';
import HeaderPage from '../HeaderPage';
import { css } from '../Styles';

const datas = [
  {
    currency: 'AUD',
    buy: '9,856',
    sell: '9,886',
  },
  {
    currency: 'EUR',
    buy: '14,504',
    sell: '14,554',
  },
  {
    currency: 'GBP',
    buy: '16,109',
    sell: '16,129',
  },
  {
    currency: 'JPY',
    buy: '156',
    sell: '158',
  },
  {
    currency: 'SGD',
    buy: '8,901',
    sell: '8,991',
  },
  {
    currency: 'THB',
    buy: '370.15',
    sell: '380.15',
  }, 
];

export default class ForexRate extends React.Component {
  static navigationOptions = ({ navigation }) => ({
    header: (
      <HeaderPage goBack={() => navigation.goBack()} title={'Inquiry'} navigations={() => navigation.navigate("DrawerOpen")} />
    )
  });
  render() {
    return (
      <View style={{flex: 1, backgroundColor: '#eef0f0'}}>
        <Content>
          <View style={{flex: 1}}>
            <Text style={[css.b, {fontSize: 18, marginLeft: 20, marginTop: 20}]}>Forex Rate Inquiry</Text>
            <Card style={styles.content}>
              <List>
                <ListItem style={{marginRight: 17, padding: 15}}>
                  <Grid>
                    <Col>
                      <Text style={[css.p, css.b, css.darkRed]}>Currency</Text>
                    </Col>
                    <Col>
                      <Text style={[css.p, css.b, css.darkRed, css.centerText]}>Buy</Text>
                    </Col>
                    <Col>
                      <Text style={[css.p, css.b, css.darkRed, css.rightText]}>Sell</Text>
                    </Col>
                  </Grid>
                </ListItem>
              </List>
              <List dataArray={datas} renderRow={data =>
                <ListItem style={{marginRight: 17, padding: 15}}>
                  <Grid>
                    <Col>
                      <Text style={[css.p, css.b]}>{data.currency}</Text>
                    </Col>
                    <Col>
                      <Text style={[css.p, css.centerText]}>{data.buy}</Text>
                    </Col>
                    <Col>
                      <Text style={[css.p, css.rightText]}>{data.sell}</Text>
                    </Col>
                  </Grid>
                </ListItem>}
              />    
            </Card>
          </View>

          <TouchableOpacity onPress={() => this.props.navigation.navigate("HomeMenu")} style={[css.button, {paddingVertical: 12, marginHorizontal: 20, marginBottom: 20, marginTop: 75,}]}>
            <Text style={css.buttonText}>Go to home</Text>
          </TouchableOpacity>
        </Content>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  content: {
    backgroundColor: '#fff',
    marginHorizontal: 10,
    marginLeft: 10,
    marginRight: 10,
    borderRadius: 10,
    marginTop: 15,
    // marginBottom: 60,
  }
});