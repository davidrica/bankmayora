import React, { Component } from "react";
import Inquiry from './Inquiry';
import ForexRate from './ForexRate';
import InterestRate from './InterestRate';
import HomeMenu from '../HomeMenu';
import { StackNavigator } from "react-navigation";
import CardStackStyleInterpolator from 'react-navigation/src/views/CardStack/CardStackStyleInterpolator';

export default (DrawNav = StackNavigator(
	{
	  Inquiry: { screen: Inquiry },
	  ForexRate: { screen: ForexRate },
	  InterestRate: { screen: InterestRate },
	  HomeMenu: { screen: HomeMenu },
	},
	{
		transitionConfig: ()=> {
			return {screenInterpolator: CardStackStyleInterpolator.forHorizontal}
		}
	}
));