import React from 'react';
import { StyleSheet, Text, View, TouchableOpacity, ScrollView} from 'react-native';
import { Header, Left, Body, Right, Button, Title, Icon, Content, Card, Container, List, ListItem, Grid, Col } from 'native-base';
import HeaderPage from '../HeaderPage';
import { css } from '../Styles';

const datas = [
  {
    segment: 'Corporate Credit',
    rate: '9.39 %',
  },
  {
    segment: 'SME Credit',
    rate: '9.40 %',
  },
  {
    segment: 'Mortgage Consumer Credit',
    rate: '12.34 %',
  },
  {
    segment: 'Non Mortgage Consumer Credit',
    rate: '8.01 %',
  },
  {
    segment: 'Auto Credit',
    rate: '8.12 %',
  },
];

export default class InterestRate extends React.Component {
  static navigationOptions = ({ navigation }) => ({
    header: (
      <HeaderPage goBack={() => navigation.goBack()} title={'Inquiry'} navigations={() => navigation.navigate("DrawerOpen")} />
    )
  });
  render() {
    return (
      <View style={{flex: 1, backgroundColor: '#eef0f0'}}>
        <Content>
          <Text style={[css.b, {fontSize: 18, marginLeft: 20, marginTop: 20}]}>Interest Rate Inquiry</Text>
            <Card style={styles.content}>
            <List>
              <ListItem style={{marginRight: 17, padding: 15}}>
                <Grid>
                  <Col>
                    <Text style={[css.p, css.b, css.darkRed]}>Business Segment</Text>
                  </Col>
                  <Col>
                    <Text style={[css.p, css.b, css.darkRed, css.rightText]}>Rate</Text>
                  </Col>
                </Grid>
              </ListItem>
            </List>
            <List dataArray={datas} renderRow={data =>
              <ListItem style={{marginRight: 17, padding: 15}}>
                <Grid>
                  <Col>
                    <Text style={[css.p, css.b]}>{data.segment}</Text>
                  </Col>
                  <Col>
                    <Text style={[css.p, css.rightText]}>{data.rate}</Text>
                  </Col>
                </Grid>
              </ListItem>}
            />    
          </Card>

          <TouchableOpacity onPress={() => this.props.navigation.navigate("HomeMenu")} style={[css.button, {paddingVertical: 12, marginTop: 0, marginHorizontal: 20, marginTop: 95}]}>
            <Text style={css.buttonText}>Go to home</Text>
          </TouchableOpacity>
        </Content>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  content: {
    backgroundColor: '#fff',
    marginHorizontal: 10,
    marginLeft: 10,
    marginRight: 10,
    borderRadius: 10,
    marginTop: 15,
  }
});