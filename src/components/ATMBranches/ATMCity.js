import React from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity } from 'react-native';
import SvgUri from 'react-native-svg-uri';
import HeaderPage from '../HeaderPage';
import { css } from '../Styles';
import { Icon, List, Content } from 'native-base';
import { NavigationActions } from "react-navigation";

const datas = [
  'Jakarta', 'Bandung', 'Banten', 'Cirebon', 'Semarang', 'Kalimantan', 'Surabaya', 'Medan', 'Padang', 'Jayapura', 'Ambon', 'Sulawesi'
];

export default class ATMCity extends React.Component {
  static navigationOptions = ({ navigation }) => ({
    header: (
      <HeaderPage goBack={() => navigation.goBack()} title={'ATM Directory'} navigations={() => navigation.navigate("DrawerOpen")} />
    )
  });
  render() {
    const { params } = this.props.navigation.state;
    const { navigate } = this.props.navigation;
    return (
      <Content style={{flex: 1, backgroundColor: '#eef0f0'}}>
        <View style={[css.container,]}>
          <View style={{flex: 0, marginBottom: 20}}>
            <Text style={[css.h2, css.h2a, {marginBottom: 15}]}>Select City</Text>
            <List dataArray={datas} renderRow={(data) =>
              <TouchableOpacity onPress={() => navigate("ATMLocation", {city: data})} style={[css.panel, {height: 55}]}>
                <View style={css.leftTextPanel}>
                  <Text style={[css.h2, css.red]}>{data}</Text>
                  <Icon name='md-arrow-dropright' style={{color:'#a71e23', fontSize: 24, position: 'absolute', bottom: -3, right: 0}}/>     
                </View>
              </TouchableOpacity>    
            }/>
          </View>
        </View>
      </Content>
    );
  }
}