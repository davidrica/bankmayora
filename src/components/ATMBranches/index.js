import React, { Component } from "react";
import ATMBranches from './ATMBranches';
import ATMCity from './ATMCity';
import ATMLocation from './ATMLocation';
import ATMMap from './ATMMap';
import BranchesCity from './BranchesCity';
import BranchesLocation from './BranchesLocation';
import BranchesMap from './BranchesMap';
import HomeMenu from '../HomeMenu';
import { StackNavigator } from "react-navigation";
import CardStackStyleInterpolator from 'react-navigation/src/views/CardStack/CardStackStyleInterpolator';
export default (DrawNav = StackNavigator(
	{
	  ATMBranches: { screen: ATMBranches },
	  ATMCity: { screen: ATMCity },
	  ATMLocation: { screen: ATMLocation },
	  ATMMap: { screen: ATMMap },
	  BranchesCity: { screen: BranchesCity },
	  BranchesLocation: { screen: BranchesLocation },
	  BranchesMap: { screen: BranchesMap },
	  HomeMenu: { screen: HomeMenu },
	},
	{
		transitionConfig: ()=> {
			return {screenInterpolator: CardStackStyleInterpolator.forHorizontal}
		}
	}
));