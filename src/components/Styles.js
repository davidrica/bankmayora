import { StyleSheet } from 'react-native';

export const css = StyleSheet.create({
	container: {
		flex: 9.3, marginHorizontal: 20, marginVertical: 15,
		flex: 8.9
	},

	// Fonts
	h2: {
		color: '#080909',
	  fontSize: 18,
	  fontWeight: 'bold'
	},

	f18: {
		fontSize: 18,
	},


	f20: {
		fontSize: 20,
	},

	p: {
		fontSize: 16,
		color: '#080909',
	},
	red: {
		color: '#a71e23'
	},
	darkRed: {
		color: '#8a181b'
	},
	redBackground: {
		backgroundColor: '#a71e23'
	},
	darkRedBackground: {
		backgroundColor: '#8a181b'
	},
	b: {
		fontWeight: 'bold',
		color: '#080909',
	},

	//TextInput
	labelInputInline: {
		flex: 0, flexDirection: 'row', alignItems: 'center', 
	},
	textInput: {
		backgroundColor: '#fff', 
		borderRadius: 4,
		paddingHorizontal: 15,
		height: 45,
		elevation: 1,
		color: '#a71e23',
		fontSize: 18,
	},
	textInputView: {
		backgroundColor: '#fff', 
		borderRadius: 4,
		paddingHorizontal: 15,
		height: 45,
		elevation: 1,
	},
	singleFormGroup: {
		marginBottom: 20,
	},

	//Button
	buttonText: {
    fontSize: 22,
    color: 'white',
    alignSelf: 'center'
  },

  buttonTextGrey: {
    fontSize: 22,
    color: '#a71e23',
    alignSelf: 'center'
  },

  button: {
    backgroundColor: '#a71e23',
    borderColor: '#a71e23',
    borderWidth: 1,
    borderRadius: 4,
    marginBottom: 10,
    alignSelf: 'stretch',
    justifyContent: 'center',
    marginTop: 100,
    paddingVertical: 10
  },

  buttonGrey: {
    backgroundColor: '#cecece',
    borderColor: '#cecece',
    borderWidth: 1,
    borderRadius: 4,
    marginBottom: 10,
    alignSelf: 'stretch',
    justifyContent: 'center',
    marginTop: 100,
    paddingVertical: 10
  },

	panel: {
		flex: 0,
		flexDirection: 'row',
	alignItems:'center',
	backgroundColor: '#fff',
	height: 61,
	paddingHorizontal: 20,
	borderRadius: 3,
	marginBottom: 5,
	elevation: 2
	},

	h2a: {
		marginBottom: 15,
	},
	leftTextPanel: {
		flex: 1,
		justifyContent: 'flex-start',
	},
	rightTextPanel: {
		flex: 1,
		justifyContent: 'flex-end',
	},

	// Circle
	circleView: {
		backgroundColor: '#fff',
		borderRadius: 100,
		justifyContent: 'center',
		alignItems: 'center',
		width: 35,
		height: 35,
		elevation: 2,
	},

	circleViewActive: {
		backgroundColor: '#8a181b',
		borderRadius: 100,
		justifyContent: 'center',
		alignItems: 'center',
		width: 35,
		height: 35,
		elevation: 2,
	},

	circleText: {
		fontSize: 22,
		color: '#cecece',
	},

	circleTextActive: {
		fontSize: 22,
		color: '#fff',
	},

	//
	centerText: {
		textAlign: 'center',
	},
	rightText: {
		textAlign: 'right',
	},
	radioWrap: {
		backgroundColor: '#B1383A',
		borderRadius: 100,
		// padding: 15
		paddingVertical: 5,
		elevation: 1,
	},
	radioContainer: {
		flexDirection: 'row',
		alignItems: 'center',
		justifyContent: 'center',
	},
	radioText: {
		color: '#fff',
		fontSize: 16,
		fontWeight: 'bold',
		paddingLeft: 5
	}
})