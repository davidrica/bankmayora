import React from 'react';
import { StyleSheet, Text, View, Button, Alert } from 'react-native';
import BankMayora from 'components/BankMayora';
import SplashScreen from 'components/SplashScreen';
import FirstPageOption from 'components/FirstPageOption';
import DeviceRegistration from 'components/DeviceRegistration';
import DeviceRegistrationOTP from 'components/DeviceRegistrationOTP';
import Login from 'components/Login';
import HomeMenu from 'components/HomeMenu';
import HomeScreen from 'components/HomeScreen';
import Accounts from 'components/Accounts';
import InhouseTransfer from 'components/InhouseTransfer';
import DomesticTransfer from 'components/DomesticTransfer';
import BillPayment from 'components/BillPayment';
import Purchase from 'components/Purchase';
import TransactionStatus from 'components/TransactionStatus';
import Inquiry from 'components/Inquiry';
import NativeBase from 'components/NativeBase';

// Testing Components
import ATMLocation from 'components/ATMBranches/ATMLocation';
import Inbox from 'components/HelpDesk/Inbox';

// if((process.env.NODE_ENV || '').toLowerCase() === 'production'){
//   // disable console. log in production
//   console.log = function () {};
//   console.info = function () {};
//   console.warn = function () {};
//   console.error = function () {}
//   console.debug = function () {}
// }

// if (!__DEV__) {
//   // eslint-disable-line no-undef
//   [
//     'assert',
//     'clear',
//     'count',
//     'debug',
//     'dir',
//     'dirxml',
//     'error',
//     'exception',
//     'group',
//     'groupCollapsed',
//     'groupEnd',
//     'info',
//     'log',
//     'profile',
//     'profileEnd',
//     'table',
//     'time',
//     'timeEnd',
//     'timeStamp',
//     'trace',
//     'warn',
//   ].forEach(methodName => {
//     console[methodName] = () => {
//       /* noop */
//     };
//   });
// }

export default class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      component: <NativeBase/>,
      // component: <SplashScreen/>,
      // component: <InhouseTransfer/>
    };
  }

  // componentDidMount(){
  //   this.timeoutHandle =  setTimeout(() => {
  //     this.setState({
  //     component: <FirstPageOption
  //     onMobileBanking={() => this.handleMobileBanking()}
  //     onLogin={() => this.handleLogin()}
  //     />,
  //     });
  //   }, 4000);
  // }

  handleMobileBanking() {
    this.setState({
      component: <DeviceRegistration onDeviceRegistrationOTP={() => this.handleDeviceRegistrationOTP()} />,
    });
  }

  handleDeviceRegistrationOTP() {
    this.setState({
      component: <DeviceRegistrationOTP onLogin={() => this.handleLogin()} />
    });
  }

  handleLogin() {
    this.setState({
      component: <Login onHomeMenu={() => this.handleHomeMenu()} />
    });
  }

  handleHomeMenu() {
    this.setState({
      component: <NativeBase />
    });
  }

  render() {
    return this.state.component;      
  }

}
