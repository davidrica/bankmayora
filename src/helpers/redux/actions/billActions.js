import { GO_TO_BILL_CONFIRM_1, NEW_BILL_BENEFICIARY } from './types';

export function goBillConfirm1(name, id, name2, id2, date, amount) {
	return {
		type: GO_TO_BILL_CONFIRM_1,
		name,
		id,
		name2,
		id2,
		date,
		amount
	}
}

export function newBillBeneficiary(category, institution, nickName, phoneNumber, saveBeneficiary) {
	return {
		type: NEW_BILL_BENEFICIARY,
		category, 
		institution, 
		nickName, 
		phoneNumber, 
		saveBeneficiary,
	}
}