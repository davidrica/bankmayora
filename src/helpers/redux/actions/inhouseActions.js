import { GO_TO_INHOUSE_CONFIRM_1 } from './types';

export function goInhouseConfirm1(name, id, name2, id2, currency, amount, message1, message2, message3, email, date) {
	return {
		type: GO_TO_INHOUSE_CONFIRM_1,
		name,
		id,
		name2,
		id2,
		currency,
		amount,
		message1,
		message2,
		message3,
		email,
		date
	}
}