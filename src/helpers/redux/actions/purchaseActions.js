import { GO_TO_PURCHASE_CONFIRM_1, NEW_PURCHASE_BENEFICIARY } from './types';

export function goPurchaseConfirm1(name, id, name2, id2, date, amount) {
	return {
		type: GO_TO_PURCHASE_CONFIRM_1,
		name,
		id,
		name2,
		id2,
		date,
		amount
	}
}

export function newPurchaseBeneficiary(category, institution, nickName, phoneNumber, saveBeneficiary) {
	return {
		type: NEW_PURCHASE_BENEFICIARY,
		category, 
		institution, 
		nickName, 
		phoneNumber, 
		saveBeneficiary,
	}
}