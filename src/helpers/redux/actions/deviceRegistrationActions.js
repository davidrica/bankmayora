import { DEVICE_REGISTRATION_SUBMIT } from './types';

export function deviceRegistrationSubmit() {
	return {
		type: DEVICE_REGISTRATION_SUBMIT,
	}
}