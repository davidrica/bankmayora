import { GO_TO_DOMESTIC_CONFIRM_1, NEW_DOMESTIC_BENEFICIARY } from './types';

export function goDomesticConfirm1(name, id, name2, id2, service, currency, amount, message1, message2, message3, email, date) {
	return {
		type: GO_TO_DOMESTIC_CONFIRM_1,
		name,
		id,
		name2,
		id2,
		service,
		currency,
		amount,
		message1,
		message2,
		message3,
		email,
		date
	}
}

export function newDomesticBeneficiary(nickName, accountNumber, accountName, bank, beneficiaryType, transactionType, citizenshipType, saveBeneficiary) {
	return {
		type: NEW_DOMESTIC_BENEFICIARY,
		nickName, 
		accountNumber, 
		accountName, 
		bank, 
		beneficiaryType, 
		transactionType, 
		citizenshipType, 
		saveBeneficiary
	}
}