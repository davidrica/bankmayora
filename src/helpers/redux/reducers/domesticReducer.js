import { GO_TO_DOMESTIC_CONFIRM_1, NEW_DOMESTIC_BENEFICIARY } from '../actions/types';

const initialState = {
	amount: '',
	date: 'DD/MM/YYYY',
	currency: 'IDR',
	service: 'Online',
	bank: 'Select Bank',
	transactionType: 'Transfer to Customer',
}

export default (state = initialState, action ) => {
	switch(action.type) {
		case GO_TO_DOMESTIC_CONFIRM_1:
			return {...state, name: action.name, id: action.id, name2: action.name2, id2: action.id2, service: action.service, 
				currency: action.currency, amount: action.amount, message1: action.message1, message2: action.message2, 
				message3: action.message3, email: action.email, date: action.date };
		case NEW_DOMESTIC_BENEFICIARY:
			return {...state, nickName: action.nickName , accountNumber: action.accountNumber , accountName: action.accountName , 
				bank: action.bank , beneficiaryType: action.beneficiaryType , transactionType: action.transactionType , 
				citizenshipType: action.citizenshipType , saveBeneficiary: action.saveBeneficiary};
		default:
			return state;
	}
}