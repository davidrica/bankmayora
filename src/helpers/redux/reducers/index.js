import { combineReducers } from 'redux';
import InhouseReducers from './inhouseReducer';
import DomesticReducers from './domesticReducer';
import BillReducers from './billReducer';
import PurchaseReducers from './purchaseReducer';
import DeviceRegistrationReducers from './deviceRegistrationReducer';;

export default combineReducers({
	inhouse: InhouseReducers,
	domestic: DomesticReducers,
	bill: BillReducers,
	purchase: PurchaseReducers,
	deviceRegistration: DeviceRegistrationReducers
});