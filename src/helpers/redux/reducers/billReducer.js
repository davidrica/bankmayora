import { GO_TO_BILL_CONFIRM_1, NEW_BILL_BENEFICIARY } from '../actions/types';

const initialState = {
	date: 'DD/MM/YYYY',
	amount: ''
}

export default (state = initialState, action ) => {
	switch(action.type) {
		case GO_TO_BILL_CONFIRM_1:
			return {...state, name: action.name, id: action.id, name2: action.name2, id2: action.id2, date: action.date, amount: action.amount};
		case NEW_BILL_BENEFICIARY:
			return {...state, category: action.category , institution: action.institution , nickName: action.nickName , 
				phoneNumber: action.phoneNumber , saveBeneficiary: action.saveBeneficiary};
		default:
			return state;
	}
}