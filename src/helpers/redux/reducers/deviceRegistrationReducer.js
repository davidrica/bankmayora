import { DEVICE_REGISTRATION_SUBMIT } from '../actions/types';

const initialState = {
}

export default (state = initialState, action ) => {
	switch(action.type) {
		case DEVICE_REGISTRATION_SUBMIT:
			return {...state, registered: true};
		default:
			return state;
	}
}