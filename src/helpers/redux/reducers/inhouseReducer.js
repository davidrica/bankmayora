import { GO_TO_INHOUSE_CONFIRM_1 } from '../actions/types';

const initialState = {
	amount: '',
	date: 'DD/MM/YYYY',
	currency: 'IDR',
}

export default (state = initialState, action ) => {
	switch(action.type) {
		case GO_TO_INHOUSE_CONFIRM_1:
			return {...state, name: action.name, id: action.id, name2: action.name2, id2: action.id2, currency: action.currency, 
				amount: action.amount, message1: action.message1, message2: action.message2, message3: action.message3, 
				email: action.email, date: action.date };
		default:
			return state;
	}
}