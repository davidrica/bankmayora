import { AppRegistry } from 'react-native';
import App from './src/App';
import React from 'react';
import { Provider } from 'react-redux';
import { createStore } from 'redux';
import reducers from 'helpers/redux/reducers';

// Redux Persist
import { persistStore, persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';
import { PersistGate } from 'redux-persist/integration/react';

const persistConfig = {
  key: 'root',
  storage,
};

const persistedReducer = persistReducer(persistConfig, reducers);
let store = createStore(persistedReducer);
let persistor = persistStore(store);

const AppContainer = () => {
  return (
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        <App />
      </PersistGate>
    </Provider>
  );
};

// const AppContainer = () =>
// 	<Provider store={store}>
// 		P
// 		<App />
// 	</Provider>;

AppRegistry.registerComponent('BankMayora', () => AppContainer);