import { SET_COUNTER, SET_INCREMENT, SET_DECREMENT, SET_CLEAR } from './types';

export const counterIncrement = () => {
	return {
		type: SET_INCREMENT
	};
}

export const counterDecrement = () => {
	return {
		type: SET_DECREMENT
	};
}

export const counterClear = () => {
	return {
		type: SET_CLEAR
	};
}

export const counterSet = (recievednumber) => {
	return {
		type: SET_COUNTER,
		payload: recievednumber
	};
}