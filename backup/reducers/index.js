import { combineReducers } from 'redux';
import CounterReducers from './counterReducer';
import HelloReducers from './helloReducer';

export default combineReducers({
	counter: CounterReducers,
	hello: HelloReducers
});