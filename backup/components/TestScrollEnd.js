import React from 'react';
import {ScrollView, Text} from 'react-native';

const isCloseToBottom = ({layoutMeasurement, contentOffset, contentSize}) => {
  const paddingToBottom = 20;
  return layoutMeasurement.height + contentOffset.y >=
    contentSize.height - paddingToBottom;
};

const TestScrollEnd = ({enableSomeButton}) => (
  <ScrollView
    onScroll={({nativeEvent}) => {
      if (isCloseToBottom(nativeEvent)) {
        enableSomeButton();
      }
    }}
    scrollEventThrottle={400}
  >
    <Text>Here is very long lorem ipsum or something...</Text>
    <Text>Here is very long lorem ipsum or something...</Text>
    <Text>Here is very long lorem ipsum or something...</Text>
    <Text>Here is very long lorem ipsum or something...</Text>
    <Text>Here is very long lorem ipsum or something...</Text>
    <Text>Here is very long lorem ipsum or something...</Text>
    <Text>Here is very long lorem ipsum or something...</Text>
    <Text>Here is very long lorem ipsum or something...</Text>
    <Text>Here is very long lorem ipsum or something...</Text>
    <Text>Here is very long lorem ipsum or something...</Text>
    <Text>Here is very long lorem ipsum or something...</Text>
    <Text>Here is very long lorem ipsum or something...</Text>
    <Text>Here is very long lorem ipsum or something...</Text>
    <Text>Here is very long lorem ipsum or something...</Text>
    <Text>Here is very long lorem ipsum or something...</Text>
    <Text>Here is very long lorem ipsum or something...</Text>
    <Text>Here is very long lorem ipsum or something...</Text>
    <Text>Here is very long lorem ipsum or something...</Text>
    <Text>Here is very long lorem ipsum or something...</Text>
    <Text>Here is very long lorem ipsum or something...</Text>
    <Text>Here is very long lorem ipsum or something...</Text>
    <Text>Here is very long lorem ipsum or something...</Text>
    <Text>Here is very long lorem ipsum or something...</Text>
    <Text>Here is very long lorem ipsum or something...</Text>
    <Text>Here is very long lorem ipsum or something...</Text>
    <Text>Here is very long lorem ipsum or something...</Text>
    <Text>Here is very long lorem ipsum or something...</Text>
    <Text>Here is very long lorem ipsum or something...</Text>
    <Text>Here is very long lorem ipsum or something...</Text>
    <Text>Here is very long lorem ipsum or something...</Text>
    <Text>Here is very long lorem ipsum or something...</Text>
    <Text>Here is very long lorem ipsum or something...</Text>
    <Text>Here is very long lorem ipsum or something...</Text>
    <Text>Here is very long lorem ipsum or something...</Text>
    <Text>Here is very long lorem ipsum or something...</Text>
    <Text>Here is very long lorem ipsum or something...</Text>
    <Text>Here is very long lorem ipsum or something...</Text>
    <Text>Here is very long lorem ipsum or something...</Text>
    <Text>Here is very long lorem ipsum or something...</Text>
    <Text>Here is very long lorem ipsum or something...</Text>
    <Text>Here is very long lorem ipsum or something...</Text>
    <Text>Here is very long lorem ipsum or something...</Text>
    <Text>Here is very long lorem ipsum or something...</Text>
    <Text>Here is very long lorem ipsum or something...</Text>
    <Text>Here is very long lorem ipsum or something...</Text>
    <Text>Here is very long lorem ipsum or something...</Text>
    <Text>Here is very long lorem ipsum or something...</Text>
    <Text>Here is very long lorem ipsum or something...</Text>
    <Text>Here is very long lorem ipsum or something...</Text>
    <Text>Here is very long lorem ipsum or something...</Text>
    <Text>Here is very long lorem ipsum or something...</Text>
    <Text>Here is very long lorem ipsum or something...</Text>
    <Text>Here is very long lorem ipsum or something...</Text>
    <Text>Here is very long lorem ipsum or something...</Text>
    <Text>Here is very long lorem ipsum or something...</Text>
    <Text>Here is very long lorem ipsum or something...</Text>
    <Text>Here is very long lorem ipsum or something...</Text>
    <Text>Here is very long lorem ipsum or something...</Text>
    <Text>Here is very long lorem ipsum or something...</Text>
    <Text>Here is very long lorem ipsum or something...</Text>
    <Text>Here is very long lorem ipsum or something...</Text>
    <Text>Here is very long lorem ipsum or something...</Text>
    <Text>Here is very long lorem ipsum or something...</Text>
    <Text>Here is very long lorem ipsum or something...</Text>
    <Text>Here is very long lorem ipsum or something...</Text>
    <Text>Here is very long lorem ipsum or something...</Text>
    <Text>Here is very long lorem ipsum or something...</Text>
    <Text>Here is very long lorem ipsum or something...</Text>
    <Text>Here is very long lorem ipsum or something...</Text>
    <Text>Here is very long lorem ipsum or something...</Text>
    <Text>Here is very long lorem ipsum or something...</Text>
    <Text>Here is very long lorem ipsum or something...</Text>
    <Text>Here is very long lorem ipsum or something...</Text>
    <Text>Here is very long lorem ipsum or something...</Text>
    <Text>Here is very long lorem ipsum or something...</Text>
    <Text>Here is very long lorem ipsum or something...</Text>
    <Text>Here is very long lorem ipsum or something...</Text>
    <Text>Here is very long lorem ipsum or something...</Text>
    <Text>Here is very long lorem ipsum or something...</Text>
    <Text>Here is very long lorem ipsum or something...</Text>
    <Text>Here is very long lorem ipsum or something...</Text>
    <Text>Here is very long lorem ipsum or something...</Text>
    <Text>Here is very long lorem ipsum or something...</Text>
    <Text>Here is very long lorem ipsum or something...</Text>
    <Text>Here is very long lorem ipsum or something...</Text>
    <Text>Here is very long lorem ipsum or something...</Text>
    <Text>Here is very long lorem ipsum or something...</Text>
    <Text>Here is very long lorem ipsum or something...</Text>
    <Text>Here is very long lorem ipsum or something...</Text>
    <Text>Here is very long lorem ipsum or something...</Text>
    <Text>Here is very long lorem ipsum or something...</Text>
    <Text>Here is very long lorem ipsum or something...</Text>
    <Text>Here is very long lorem ipsum or something...</Text>
    <Text>Here is very long lorem ipsum or something...</Text>
    <Text>Here is very long lorem ipsum or something...</Text>
    <Text>Here is very long lorem ipsum or something...</Text>
    <Text>Here is very long lorem ipsum or something...</Text>
    <Text>Here is very long lorem ipsum or something...</Text>
    <Text>Here is very long lorem ipsum or something...</Text>
    <Text>Here is very long lorem ipsum or something...</Text>
    <Text>Here is very long lorem ipsum or something...</Text>
    <Text>Here is very long lorem ipsum or something...</Text>
    <Text>Here is very long lorem ipsum or something...</Text>
    <Text>Here is very long lorem ipsum or something...</Text>
    <Text>Here is very long lorem ipsum or something...</Text>
    <Text>Here is very long lorem ipsum or something...</Text>
    <Text>Here is very long lorem ipsum or something...</Text>
    <Text>Here is very long lorem ipsum or something...</Text>
  </ScrollView>
);

export default TestScrollEnd;