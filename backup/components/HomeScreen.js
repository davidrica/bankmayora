import React, { Component } from "react";
import { Dimensions } from "react-native";
import HomeScreen2 from "./HomeScreen2.js";
import MainScreenNavigator from "./ChatScreen.js";
import Profile from "./Profile.js";
import SideBar from "./SideBar.js";
import { DrawerNavigator } from "react-navigation";
import FirstPageOption from './FirstPageOption';
import DeviceRegistration from './DeviceRegistration';
import DeviceRegistrationOTP from './DeviceRegistrationOTP';
import Login from './Login';
import HomeMenu from './HomeMenu';
import Accounts from './Accounts/index';
import InhouseTransfer from './InhouseTransfer/index';
import DomesticTransfer from './DomesticTransfer/index';
import BillPayment from './BillPayment/index';
import Purchase from './Purchase/index';
import TransactionStatus from './TransactionStatus/index';
import Inquiry from './Inquiry/index';
import News from './News';
import ATMBranches from './ATMBranches/index';
import HelpDesk from './HelpDesk';

const width = Dimensions.get('window').width;

const HomeScreenRouter = DrawerNavigator(
  {
    // FirstPageOption: { screen: FirstPageOption },
    // DeviceRegistration: { screen: DeviceRegistration },
    // DeviceRegistrationOTP: { screen: DeviceRegistrationOTP },
    // Login: { screen: Login },
  	
    Home: { screen: HomeMenu },
    InhouseTransfer: { screen: InhouseTransfer },
    DomesticTransfer: { screen: DomesticTransfer },
    BillPayment: { screen: BillPayment },
    Purchase: { screen: Purchase },
    TransactionStatus: { screen: TransactionStatus },
    Inquiry: { screen: Inquiry },
    Accounts: { screen: Accounts },
    News: { screen: News },
    ATMBranches: { screen: ATMBranches },
    HelpDesk: { screen: HelpDesk },
  },
  {
    contentComponent: props => <SideBar {...props} />,
    drawerPosition : 'right',
    drawerWidth: width * 21/36,
    drawerBackgroundColor: 'transparent',
  },
);
export default HomeScreenRouter;