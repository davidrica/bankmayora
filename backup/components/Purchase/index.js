import React, { Component } from "react";
import Purchase from './Purchase';
import PurConfirm1 from './PurConfirm1';
import PurConfirm2 from './PurConfirm2';
import PurNewBen1 from './PurNewBen1';
import PurSubmitted from './PurSubmitted';
import HomeMenu from '../HomeMenu';
import { StackNavigator } from "react-navigation";
import CardStackStyleInterpolator from 'react-navigation/src/views/CardStack/CardStackStyleInterpolator';

export default (DrawNav = StackNavigator(
	{
	  Purchase: { screen: Purchase },
	  PurConfirm1: { screen: PurConfirm1 },
	  PurConfirm2: { screen: PurConfirm2 },
	  PurNewBen1: { screen: PurNewBen1 },
	  PurSubmitted: { screen: PurSubmitted },
	  HomeMenu: { screen: HomeMenu },
	},
	{
		transitionConfig: ()=> {
			return {screenInterpolator: CardStackStyleInterpolator.forHorizontal}
		}
	}
));