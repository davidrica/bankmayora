import React from 'react';
import { StyleSheet, Text, View, TouchableOpacity, TextInput, TouchableHighlight, Modal, Picker } from 'react-native';
import HeaderPage from '../HeaderPage';
import { css } from '../Styles';
import { Grid, Col, Content, Icon, Radio} from 'native-base';
import { NavigationActions } from "react-navigation";
import SvgUri from 'react-native-svg-uri';

export default class BillNewBen1 extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      onOffBackground: '#fff',
      onOffText: 'OFF',
      onOffColor: '#080909',
      onOffWidth: 1,
      onOffBorderColor: '#080909',
      modalVisible: false,
      itemSelected: 'itemOne',
      itemSelected2: 'itemOne',
    };
  }

  handleSave() {
    this.state.onOffBackground == '#fff' ? this.setState({
      onOffBackground: '#a71e23', onOffText: 'ON', onOffColor: '#fff', onOffWidth: 0
    })  : this.setState({
      onOffBackground: '#fff', onOffText: 'OFF', onOffColor: '#080909', onOffWidth: 1
    });
  }

  handleBenAdded(){
    this.setState({
      modalVisible: false,
    })
    this.props.navigation.navigate('BillPayment');
  }

  openModal() {
    this.setState({modalVisible:true});
  }

  closeModal() {
    this.setState({modalVisible:false});
  }

  static navigationOptions = ({ navigation }) => ({
    header: (
      <HeaderPage goBack={() => navigation.goBack()} title={'Bill Payment'} navigations={() => navigation.navigate("DrawerOpen")} />
    )
  });

  render() {
    return (
      <View style={{flex: 1, backgroundColor: '#eef0f0'}}>
        <Content showsVerticalScrollIndicator={false} style={css.container}>
          <View style={{flex: 1}}>
            <Text style={[css.b, {marginBottom: 15} ]}>Transfer To - <Text style={[css.b, css.red, {fontSize: 16}]}>New Payee Contact</Text></Text>
            <View style={[css.labelInputInline, css.singleFormGroup]}>
              <Text style={[css.b, {flex: 1}]}>Institution Category</Text>
              <Picker
                mode='dropdown'
                selectedValue={this.state.picker1}
                onValueChange={(itemValue, itemIndex) => this.setState({picker1: itemValue})}
                style={[css.textInputView, {flex: 1.4, justifyContent: 'center', height: 45, color: '#a71e23'}]}>
                <Picker.Item label="    Select category" value="0" />
                <Picker.Item label="    Education" value="1" />
                <Picker.Item label="    Telecommunication" value="2" />
                <Picker.Item label="    PAM" value="3" />
                <Picker.Item label="    PLN" value="4" />
              </Picker>
              <Icon name='md-arrow-dropdown' style={{color:'#a71e23', fontSize: 20, position: 'absolute', bottom: 11, right: 22, elevation: 1}}/>
            </View>
            <View style={[css.labelInputInline, css.singleFormGroup]}>
              <Text style={[css.b, {flex: 1}]}>Institution</Text>
              <Picker
                mode='dropdown'
                selectedValue={this.state.picker2}
                onValueChange={(itemValue, itemIndex) => this.setState({picker2: itemValue})}
                style={[css.textInputView, {flex: 1.4, justifyContent: 'center', height: 45, color: '#a71e23'}]}>
                <Picker.Item label="    Select institution" value="0" />
                <Picker.Item label="    Indosat" value="1" />
                <Picker.Item label="    Telkomsel" value="2" />
                <Picker.Item label="    Three" value="3" />
                <Picker.Item label="    XL" value="4" />
              </Picker>
              <Icon name='md-arrow-dropdown' style={{color:'#a71e23', fontSize: 20, position: 'absolute', bottom: 11, right: 22, elevation: 1}}/>
            </View>
            <View style={[css.labelInputInline, css.singleFormGroup]}>
              <Text style={[css.b, {flex: 1}]}>Nick Name</Text>
              <TextInput  maxLength={16} underlineColorAndroid='transparent' style={[css.textInput, {flex: 1.4}]} 
              onChangeText={(text) => this.setState({text})} />
            </View>
            <View style={[css.labelInputInline, css.singleFormGroup]}>
              <Text style={[css.b, {flex: 1}]}>Phone Number</Text>
              <TextInput keyboardType='numeric' maxLength={16} underlineColorAndroid='transparent' style={[css.textInput, {flex: 1.4}]} 
              onChangeText={(text) => this.setState({text})} />
            </View>
            <Grid style={{flex: 0, marginBottom: 20}}>
              <Col><Text style={[css.b, {marginBottom: 15} ]}>Save to beneficiary list</Text></Col>
              <Col style={{alignItems: 'flex-end'}}>
                <TouchableHighlight onPress={this.handleSave.bind(this)} style={{backgroundColor: this.state.onOffBackground , borderRadius: 3, alignItems: 'center', padding: 10, paddingBottom: 7, elevation: 2}}>
                  <View style={{alignItems: 'center'}}>
                    <Text style={[css.b, css.f18, {marginBottom: 3, color: this.state.onOffColor}]}>{this.state.onOffText}</Text>
                    <View style={{height: 5, width: 70, borderWidth: this.state.onOffWidth, backgroundColor: '#fff', borderRadius: 30}} />
                  </View>
                </TouchableHighlight>
              </Col>
            </Grid>
            <View style={[css.labelInputInline, css.singleFormGroup, {marginBottom: 0}]}>
              <TouchableOpacity style={[{flex: 1, backgroundColor: '#a71e23', borderRadius: 4, height: 45, marginRight: 15, alignItems: 'center', justifyContent: 'center'}]}>
                <Text style={[css.b, {color: '#fff'}]}>Send OTP</Text>
              </TouchableOpacity>
              <View style={{flex: 1.4}}>
                <TextInput underlineColorAndroid='transparent' style={[css.textInput]} 
                onChangeText={(text) => this.setState({text})} secureTextEntry keyboardType='numeric' maxLength={6} />
              </View>
            </View>
            <View style={[css.labelInputInline, css.singleFormGroup, {justifyContent: 'flex-start'}]}>
              <View style={[{flex: 1, borderRadius: 4, height: 0, marginRight: 15, alignItems: 'center', justifyContent: 'center'}]}>
              </View>
              <View style={{flex: 1.4}}>
                <Text style={[css.darkRed, {fontSize: 12, textAlign: 'center', justifyContent: 'flex-start', marginTop: 5}]}>Check your message for OTP</Text>
              </View>
            </View>
          </View>

          <View style={{marginBottom: 0}}>
            <TouchableOpacity onPress={() => this.openModal()} style={[css.button, {flex: 0, paddingVertical: 12, marginTop: 25}]}>
              <Text style={css.buttonText}>Submit</Text>
            </TouchableOpacity>
          </View>
        </Content>
        <Modal transparent={true} visible={this.state.modalVisible} onRequestClose={() => this.closeModal()} animationType={'none'}>
          <TouchableHighlight onPress={() => this.handleBenAdded()} style={[styles.modalContainer, {flex: 1, backgroundColor: 'rgba(47,48,48,.3)'}]}>
            <View style={styles.MainContainer}>
             <SvgUri width="50" height="50" source={require('../../assets/images/icons/success-01.svg')} />
             <Text style={[css.b, {textAlign: 'center', fontSize: 16, maxWidth: 250, marginTop: 15}]}>Beneficiary contact has been submitted</Text>
             <Text style={[{textAlign: 'center', fontSize: 16, maxWidth: 250, color: '#080909'}]}>You will be redirected back to the form</Text>
            </View>
          </TouchableHighlight>         
        </Modal>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  modalContainer: {
    backgroundColor: 'transparent',
    paddingHorizontal: 20,
    justifyContent: 'center'
  },

  MainContainer :{
    justifyContent: 'center',
    alignItems: 'center',
    padding: 20,
    backgroundColor: '#fff',
    borderRadius: 3,
    elevation: 2,
  },
});