import React from 'react';
import { StyleSheet, Text, View, Image, FormInput, TouchableHighlight, Modal, Button, TouchableOpacity, TouchableWithoutFeedback, 
  TextInput } from 'react-native';
import { Header, Form, Item, Input } from 'native-base';
import { css } from './Styles';
import SvgUri from 'react-native-svg-uri';

export default class DeviceRegistrationOTP extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      modalVisible: false,
      text: '',
    };
  }

  openModal() {
    this.setState({modalVisible:true});
  }

  closeModal() {
    this.setState({modalVisible:false});
  }

  render() {

    return (
      // <View style={{flex:1, backgroundColor: '#eef0f0',}}>
      //   <View style={styles.topHead} />
      //   <View style={{flex: 1, marginHorizontal: 25, marginVertical: 25, alignItems: 'flex-end', backgroundColor: '#eef0f0',}}>
      //     <Text>Bank Mayora</Text>
      //   </View>
      //   <View style={styles.content}>
      //     <View style={{flex: 1, alignItems: 'center'}}>
            
      //       <View style={[css.singleFormGroup, {alignItems: 'center'}]}>
      //         <Text style={[css.b, {marginBottom: 10, marginTop: 35, fontSize: 16}]}>Input OTP Code</Text>
      //         <TextInput underlineColorAndroid='transparent' secureTextEntry keyboardType='numeric' maxLength={6} style={[css.textInput, {marginBottom: 7, width: 95, fontSize: 24, textAlign: 'center'}]} 
      //         onChangeText={(text) => this.setState({text})} />
      //       </View>

      //       <Text style={[css.darkRed, {marginBottom: 20, marginTop: 50, fontSize: 12, textAlign: 'center'}]}>Check your message for OTP</Text>

      //       <TouchableOpacity style={[css.buttonGrey, {paddingVertical: 12, marginTop: 0}]}>
      //         <Text style={css.buttonTextGrey}>Resend OTP</Text>
      //       </TouchableOpacity>

      //       <TouchableOpacity onPress={() => this.props.navigation.navigate("Submitted")} style={[css.button, {paddingVertical: 12, marginTop: 0}]}>
      //         <Text style={css.buttonText}>Submit</Text>
      //       </TouchableOpacity>
      //     </View>

      //     <Modal visible={this.state.modalVisible} animationType={'fade'} onRequestClose={() => this.closeModal()} transparent={true}>
      //       <View style={styles.modalContainer}>
      //         <View style={styles.innerContainer}>
      //           <View onPress={() => this.closeModal()}>
      //             <SvgUri width="50" height="50" source={require('../assets/images/icons/success-01.svg')} onPress={() => this.closeModal()} />            
      //           </View>
      //           <Text onPress={() => this.closeModal()} style={{fontWeight: 'bold', fontSize: 18}}>Your device has been registered</Text>
      //           <Text>Device ID : 01364798634929891</Text>
      //           <TouchableOpacity style={[styles.button, styles.buttonModal]} onPress={() => this.closeModal()} title="Log in to mobile banking">
      //             <Text style={styles.buttonText}>Log in to mobile banking</Text>
      //           </TouchableOpacity>
      //         </View>
      //       </View>
      //     </Modal>

      //   </View>
      // </View>




      <View style={{flex: 1, backgroundColor: '#8a181b', padding: 4, paddingBottom: 6}}>
        <Header androidStatusBarColor={'#8a181b'} style={{backgroundColor: '#a71e23', height: 0}}/>
        <View style={[styles.container]}>
          <Image source={require('../assets/images/slices/bm01.png')} 
          style={{width:200, resizeMode: 'contain', alignSelf: 'flex-end', marginRight: -10, marginTop: 5}}/>
          <View style={{flex: 2,justifyContent: 'center'}}>
            <View style={[{alignItems: 'center'}]}>
              <Text style={[css.b, css.red, {marginBottom: 10, marginTop: 35, fontSize: 16}]}>Input OTP Code</Text>
              <Form style={{width: 95, paddingLeft: 0, marginLeft: 0}}>
                <Item style={{paddingLeft: 0, marginLeft: 0}}>
                  <Input style={[css.red, {textAlign: 'center', fontSize: 24}]} placeholder="" onChangeText={(text) => this.setState({text})} secureTextEntry keyboardType='numeric' maxLength={6} />
                </Item>
              </Form>
            </View>

            <Text style={[css.darkRed, {marginBottom: 20, marginTop: 50, fontSize: 12, textAlign: 'center'}]}>Check your message for OTP</Text>

            <TouchableOpacity style={[css.buttonGrey, {paddingVertical: 12, marginTop: 0}]}>
              <Text style={css.buttonTextGrey}>Send OTP</Text>
            </TouchableOpacity>

            <TouchableOpacity onPress={() => this.openModal()} style={[css.button, {paddingVertical: 12, marginTop: 0}]}>
              <Text style={css.buttonText}>Authenticate</Text>
            </TouchableOpacity>
          </View>

          <Modal transparent={true} visible={this.state.modalVisible} onRequestClose={() => this.closeModal()} animationType={'none'}>
            <TouchableHighlight onPress={() => this.closeModal()} style={[styles.modalContainer, {flex: 1, backgroundColor: 'rgba(0,0,0,.6)'}]}>
              <View style={styles.MainContainer}>
               <SvgUri width="50" height="50" source={require('../assets/images/icons/success-01.svg')} />
               <Text style={[css.b, {textAlign: 'center', fontSize: 16, maxWidth: 250, marginTop: 15}]}>Your device has been registered</Text>
               <Text style={[{textAlign: 'center', fontSize: 16, maxWidth: 250, color: '#080909'}]}>Device ID : 01364798634929891</Text>
               <TouchableOpacity onPress={this.props.onLogin} style={[css.button, {marginTop: 20}]}>
                 <Text style={[css.buttonText, {fontSize: 16}]}>Log in to mobile banking</Text>
               </TouchableOpacity>
              </View>
            </TouchableHighlight>         
          </Modal>
        </View>
        <Text style={{color: '#a71e23', textAlign: 'center', fontSize: 12, paddingBottom: 10, backgroundColor: '#eef0f0'}}>
        Copyright @ 2017, Bank Mayora{"\n"}
        Designed by PT. Aprisma Indonesia, All right reserved
        </Text>
      </View>
    );
  }
}

// Styles

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#eef0f0',
    // justifyContent: 'center',
    paddingHorizontal: 20,
    borderTopLeftRadius: 30,
  },

  content: {

  },

  optionBox: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#fff',
    maxHeight: 150,
    elevation: 2,
  },
  modalContainer: {
    backgroundColor: 'transparent',
    paddingHorizontal: 20,
    justifyContent: 'center'
  },

  MainContainer :{
    justifyContent: 'center',
    alignItems: 'center',
    padding: 20,
    paddingBottom: 10,
    backgroundColor: '#fff',
    borderRadius: 3,
    elevation: 2,
  },
});

// // Style form
// const styleForm = _.cloneDeep(t.form.Form.stylesheet);
// styleForm.textbox.normal.color = '#a71e23';
// styleForm.textbox.normal.borderWidth = 0;
// styleForm.textbox.normal.borderBottomWidth = 1;
// styleForm.textbox.normal.borderRadius = 0;
// styleForm.controlLabel.normal.color = '#a71e23';
// const options = {
//   fields: {
//     OTPCode: {
//       stylesheet: styleForm,
//       label: 'Input OTP Code',
//     },

//     password: {
//       stylesheet: styleForm,
//       label: ' ',
//       placeholder: 'Password',
//       secureTextEntry: true
//     }
//   }
// };


// import React from 'react';
// import { StyleSheet, Text, View, Image } from 'react-native';
// import SvgUri from 'react-native-svg-uri';

//         // <SvgUri width="200" height="200" source={require('../assets/images/icons/Untitled-1.svg')} />
//         // <SvgUri width="200" height="200" source={require('../assets/images/icons/Untitled-2.svg')} />
// export default class BankMayora extends React.Component {
//   render() {
//     return (
//       <View style={styles.container}>
//         <Text>Test</Text>
//       </View>
//     );
//   }
// }

// const styles = StyleSheet.create({
//   container: {
//     flex: 1,
//     backgroundColor: '#eef0f0',
//     alignItems: 'center',
//     justifyContent: 'center',
//   },
// });