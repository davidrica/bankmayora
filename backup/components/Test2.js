import React, { Component } from 'react';
import { Text, View, Button, Modal, StyleSheet, TouchableHighlight } from 'react-native';

export default class Test2 extends Component {
  state = {
    modalVisible: false,
  };

  openModal() {
    this.setState({modalVisible:true});
  }

  closeModal() {
    this.setState({modalVisible:false});
  }

  render() {
    return (
        <View style={styles.container}>
          <Modal
              transparent={true}
              visible={this.state.modalVisible}
              onRequestClose={() => this.closeModal()}
          >
              <TouchableHighlight onPress={() => this.closeModal()}style={[styles.modalContainer, {justifyContent: 'center', flex: 1}]}>
                <View style={styles.innerContainer}>
                  <Text>This is content inside of modal component</Text>
                  <Button
                      onPress={() => this.closeModal()}
                      title="Close modal"
                  >
                  </Button>
                </View>
              </TouchableHighlight>
              
          </Modal>
          <Button
              onPress={() => this.openModal()}
              title="Open modal"
          />
        </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
  },
  modalContainer: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: 'transparent',
  },
  innerContainer: {
    alignItems: 'center',
    backgroundColor: 'grey',
    maxHeight: 200,
  },
});