import React, { Component } from 'react';
import { Text, StyleSheet, View, ListView, TextInput, ActivityIndicator, Alert } from 'react-native';

export default class Test3 extends Component {
 
  constructor(props) {
    super(props);

    this.state = {
      isLoading: true,
      text: '', 
    }

    this.arrayholder = [] ;
  }
 
  componentDidMount() {

    const datas = [
      {"id":"1","fruit_name":"Apple"},{"id":"2","fruit_name":"Apricot"}
    ];

    let ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
    this.setState({
      isLoading: false,
      dataSource: ds.cloneWithRows(datas),
    }, 
    function() {
      this.arrayholder = [{"id":"1","fruit_name":"Apple"},{"id":"2","fruit_name":"Apricot"}] ;
    });
      
  }

  GetListViewItem (fruit_name) {   
   Alert.alert(fruit_name); 
  }
  
 SearchFilterFunction(text){
   const newData = this.arrayholder.filter(function(item){
       const itemData = item.fruit_name.toUpperCase()
       const textData = text.toUpperCase()
       return itemData.indexOf(textData) > -1
   })
   this.setState({
       dataSource: this.state.dataSource.cloneWithRows(newData),
       text: text
   })
 }
 
  render() {
    if (this.state.isLoading) {
      return (
        <View style={{flex: 1, paddingTop: 20}}>
          <ActivityIndicator />
        </View>
      );
    }
 
    return (
 
      <View style={styles.MainContainer}>

      <TextInput 
       style={styles.TextInputStyleClass}
       onChangeText={(text) => this.SearchFilterFunction(text)}
       value={this.state.text}
       underlineColorAndroid='transparent'
       placeholder="Search Here"
        />
 
        <ListView
          dataSource={this.state.dataSource}
          renderRow={(rowData) => 
            <View style={{borderWidth: 1, marginBottom: 5, backgroundColor: '#fff', borderRadius: 3, elevation: 1}}>
              <Text style={styles.rowViewContainer} 
              onPress={this.GetListViewItem.bind(this, rowData.fruit_name)} >{rowData.fruit_name}
              </Text>
            </View>
          }
          enableEmptySections={true}
          style={{marginTop: 10}}
        />
 
      </View>
    );
  }
}











 
const styles = StyleSheet.create({
 
 MainContainer :{

  justifyContent: 'center',
  flex:1,
  margin: 7,
 
  },
 
 rowViewContainer: {
   fontSize: 17,
   padding: 10
  },

  TextInputStyleClass:{
        
   textAlign: 'center',
   height: 40,
   borderWidth: 1,
   borderColor: '#009688',
   borderRadius: 7 ,
   backgroundColor : "#FFFFFF"
        
   }
 
});