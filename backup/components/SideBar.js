import React from "react";
import { AppRegistry, Image, StatusBar, View, StyleSheet, TouchableOpacity, Linking, Modal, TouchableHighlight, ActivityIndicator } from "react-native";
import { Container, Content, Text, List, ListItem, CheckBox, Body, Button, Left, Grid, Col } from "native-base";
import SvgUri from 'react-native-svg-uri';
import { css } from './Styles';

const datas = [
  {name: 'Home', icon: require('../assets/images/icons/home-01.svg'), navigate: 'Home'},
  {name: 'News', icon: require('../assets/images/icons/news-01.svg'), navigate: 'News'},
  {name: 'Language', icon: require('../assets/images/icons/home-01.svg'), navigate: 'HomeMenu'},
  {name: 'bankmayora.com', icon: require('../assets/images/icons/website-01.svg'), navigate: 'bankmayora.com'},
  {name: 'ATM & Branches', icon: require('../assets/images/icons/directory-01.svg'), navigate: 'ATMBranches'},
  {name: 'Help Desk', icon: require('../assets/images/icons/help-desk-01.svg'), navigate: 'HelpDesk'},
];

export default class SideBar extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      menuBackground: '#fff',
      modalVisible: false,
      modalVisibleLoading: false,
      language: 'EN',
      logout: 'Log Out',
      iconLanguage: 'EN'
    }
  }

  handlePress(navigate) {
    if (navigate  == 'bankmayora.com') {
      this.openModal();
    }
    else {
      this.props.navigation.navigate(navigate); 
    }
  }

  changeLanguage() {
    this.openModalLoading();
    this.timeoutHandle =  setTimeout(() => {
      this.closeModalLoading();
      this.state.language == 'EN' ? this.setState({language: 'ID'}) :  this.setState({language: 'EN'}) ;
      if (this.state.language == 'EN') {
        console.log('EN');
        datas = [
          {name: 'Home', icon: require('../assets/images/icons/home-01.svg'), navigate: 'Home'},
          {name: 'News', icon: require('../assets/images/icons/news-01.svg'), navigate: 'News'},
          {name: 'Language', icon: require('../assets/images/icons/home-01.svg'), navigate: 'HomeMenu'},
          {name: 'bankmayora.com', icon: require('../assets/images/icons/website-01.svg'), navigate: 'bankmayora.com'},
          {name: 'ATM & Branches', icon: require('../assets/images/icons/directory-01.svg'), navigate: 'ATMBranches'},
          {name: 'Help Desk', icon: require('../assets/images/icons/help-desk-01.svg'), navigate: 'HelpDesk'},
        ];
        this.setState({logout: 'Log Out', iconLanguage: 'EN'});
      } else {
        console.log('ID');
        datas = [
          {name: 'Beranda', icon: require('../assets/images/icons/home-01.svg'), navigate: 'Home'},
          {name: 'Berita', icon: require('../assets/images/icons/news-01.svg'), navigate: 'News'},
          {name: 'Bahasa', icon: require('../assets/images/icons/home-01.svg'), navigate: 'HomeMenu'},
          {name: 'bankmayora.com', icon: require('../assets/images/icons/website-01.svg'), navigate: 'bankmayora.com'},
          {name: 'ATM & Cabang', icon: require('../assets/images/icons/directory-01.svg'), navigate: 'ATMBranches'},
          {name: 'Bantuan', icon: require('../assets/images/icons/help-desk-01.svg'), navigate: 'HelpDesk'},
        ];
        this.setState({logout: 'Keluar', iconLanguage: 'ID'});
      }
    }, 50);
  }

  openModal() {
    this.setState({modalVisible:true});
  }

  closeModal() {
    this.setState({modalVisible:false});
  }

  openModalLoading() {
    this.setState({modalVisibleLoading:true});
  }

  closeModalLoading() {
    this.setState({modalVisibleLoading:false});
  }

  render() {

    return (
      <Container style={{backgroundColor: '#a71e23', borderTopLeftRadius: 20}}>
        <Content style={{paddingTop: 100}}>
          {
            datas.map(function(data, index){
              if(data.name == 'Language' || data.name == 'Bahasa') {
              return (
                <ListItem key={index} onPress={() => this.changeLanguage()} 
                style={[styles.listItem, {backgroundColor: '#fff'}]}>
                  <Text style={[css.b, css.red, {width: 20.5, marginLeft: 2}]}>{this.state.iconLanguage}</Text>
                  <Body>
                    <Text style={styles.listText}>{data.name}</Text>
                  </Body>
                </ListItem>
              )} 
              else {
                return (
                  <ListItem key={index} onPress={() => this.handlePress(data.navigate)}
                  style={[styles.listItem, {backgroundColor: '#fff'}]}>
                  <SvgUri width='23' height='23' source={data.icon}/>
                    <Body>
                      <Text style={styles.listText}>{data.name}</Text>
                    </Body>
                  </ListItem>
                )
              }
            }.bind(this))
          }
          <Text style={{alignSelf: 'flex-end', color: '#fff', fontSize: 11, paddingRight: 7}}>Device ID: 1234567890</Text>
        </Content>
        <Button full light style={{paddingLeft: 35}}>
          <SvgUri width='23' height='23' source={require('../assets/images/icons/logout-01.svg')}/>          
          <Left style={{paddingLeft: 30}}>
            <Text style={styles.listText}>{this.state.logout}</Text>
          </Left>
        </Button>
        <Modal transparent={true} visible={this.state.modalVisible} onRequestClose={() => this.closeModal()} animationType={'none'}>
          <TouchableHighlight onPress={() => this.closeModal()} style={[styles.modalContainer, {flex: 1, backgroundColor: 'rgba(0,0,0,.6)'}]}>
            <View style={styles.MainContainer}>
              <SvgUri width="50" height="50" source={require('../assets/images/icons/website-01.svg')} />
              <Text style={[css.b, {textAlign: 'center', fontSize: 16, marginTop: 15}]}>Continue to website?</Text>
              <Text style={[css.b, {textAlign: 'center', fontSize: 16}]}>You will automatically logout your account</Text>
              <View style={{flexDirection: 'row'}}>
                <TouchableOpacity onPress={() => this.closeModal()} style={[css.buttonGrey, {flex: 1, marginRight: 30, marginTop: 25, elevation: 1, paddingVertical: 5}]}>
                  <Text style={css.buttonTextGrey}>No</Text>
                </TouchableOpacity>
                <TouchableOpacity onPress={()=>{ Linking.openURL('http://bankmayora.com')}} style={[css.button, {flex: 1, marginTop: 25, elevation: 1, paddingVertical: 5}]}>
                  <Text style={css.buttonText}>Yes</Text>
                </TouchableOpacity>            
              </View>
            </View>
          </TouchableHighlight>         
        </Modal>

        <Modal transparent={true} visible={this.state.modalVisibleLoading} onRequestClose={() => this.closeModalLoading()} animationType={'none'}>
          <TouchableHighlight onPress={() => this.closeModalLoading()} style={[styles.modalContainer, {flex: 1, backgroundColor: 'rgba(0,0,0,.6)'}]}>
            <View style={styles.MainContainerLoading}>
              <ActivityIndicator size={50} color="rgba(229,229,229, .8)" /> 
            </View>
          </TouchableHighlight>         
        </Modal>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  listItem: {
    borderTopLeftRadius: 3,
    borderBottomLeftRadius: 3,
    maxHeight: 40,
    paddingLeft: 15,
    paddingRight: 0,
    borderBottomWidth: 0,
    marginBottom: 10
  },
  listText: {
    color: '#a71e23',
    fontWeight: 'bold',
    paddingLeft: 5,
    fontSize: 18
  },
  modalContainer: {
    backgroundColor: 'transparent',
    paddingHorizontal: 20,
    justifyContent: 'center'
  },

  MainContainer :{
    justifyContent: 'center',
    alignItems: 'center',
    padding: 20,
    backgroundColor: '#fff',
    borderRadius: 3,
    elevation: 2,
  },

  MainContainerLoading :{
    justifyContent: 'center',
    alignItems: 'center',
  },
});
