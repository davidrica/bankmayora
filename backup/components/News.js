import React from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity, Linking } from 'react-native';
import SvgUri from 'react-native-svg-uri';
import HeaderPage from './HeaderPage';
import HeaderHome from './HeaderHome';
import { css } from './Styles';
import { Icon } from 'native-base';
import { NavigationActions } from "react-navigation";

export default class News extends React.Component {
  render() {
    // const { navigate } = this.props.navigation;
    return (
      <View style={{flex: 1, backgroundColor: '#eef0f0'}}>
        <HeaderPage goBack={() => this.props.navigation.dispatch(NavigationActions.back())} title={'News'} navigations={() => this.props.navigation.navigate("DrawerOpen")} />
        <View style={[css.container, {marginTop: 20}]}>
          <View style={{marginBottom: 15, flex: 0.8, borderWidth: 2, borderColor: '#a71e23'}}>
            <Image style={{width: '100%', height: '100%', resizeMode: 'stretch'}} source={require('../assets/images/icons/image.png')} />
          </View>
          <View style={{flex: 1}}>
            <TouchableOpacity onPress={()=>{ Linking.openURL('http://bankmayora.com')}}
            style={[css.panel, {height: 70, alignItems: 'center', justifyContent: 'center', backgroundColor: '#a71e23', paddingHorizontal: 15}]}>
              <View style={{alignItems: 'center', justifyContent: 'center'}}>
                <Text style={{color: '#fff', fontSize: 16}}>For more information please visit our website :</Text>
                <Text style={{color: '#fff', fontWeight: 'bold', fontSize: 16}}>www.bankmayora.com</Text>   
              </View>
            </TouchableOpacity>      
          </View>
        </View>
      </View>
    );
  }
}

News.navigationOptions = ({ navigation }) => ({
  header: (
    <HeaderPage goBack={() => navigation.dispatch(NavigationActions.back())} title={'Inquiry'} navigations={() => navigation.navigate("DrawerOpen")} />
  )
});