import React from 'react';
import { StyleSheet, Text, View, TouchableOpacity, ScrollView, ActivityIndicator} from 'react-native';
import { Header, Left, Body, Right, Button, Title, Icon, Content, Card, Container, List, ListItem, Grid, Col } from 'native-base';
import HeaderPage from '../HeaderPage';
import { css } from '../Styles';

const isCloseToBottom = ({layoutMeasurement, contentOffset, contentSize}) => {
  const paddingToBottom = 20;
  return layoutMeasurement.height + contentOffset.y >=
    contentSize.height - paddingToBottom;
};

const datas = [
  {
    date: '06/10/2016',
    name: 'Reload Telkomsel',
    numb: 'Ref No. 133000056893',
    transType: 'Inhouse Transfer',
    currBal: '4,000,000',
    status: 'Executed Successfully',
    time: '12:10:00'
  },
  {
    date: '06/10/2016',
    name: 'Reload Telkomsel',
    numb: 'Ref No. 133000056893',
    transType: 'Inhouse Transfer',
    currBal: '4,000,000',
    status: 'Executed Successfully',
    time: '12:10:00'
  },
  {
    date: '06/10/2016',
    name: 'Reload Telkomsel',
    numb: 'Ref No. 133000056893',
    transType: 'Inhouse Transfer',
    currBal: '4,000,000',
    status: 'Executed Successfully',
    time: '12:10:00'
  },
  {
    date: '06/10/2016',
    name: 'Reload Telkomsel',
    numb: 'Ref No. 133000056893',
    transType: 'Inhouse Transfer',
    currBal: '4,000,000',
    status: 'Executed Successfully',
    time: '12:10:00'
  },
  {
    date: '06/10/2016',
    name: 'Reload Telkomsel',
    numb: 'Ref No. 133000056893',
    transType: 'Inhouse Transfer',
    currBal: '4,000,000',
    status: 'Executed Successfully',
    time: '12:10:00'
  },
  {
    date: '06/10/2016',
    name: 'Reload Telkomsel',
    numb: 'Ref No. 133000056893',
    transType: 'Inhouse Transfer',
    currBal: '4,000,000',
    status: 'Executed Successfully',
    time: '12:10:00'
  },
  {
    date: '06/10/2016',
    name: 'Reload Telkomsel',
    numb: 'Ref No. 133000056893',
    transType: 'Inhouse Transfer',
    currBal: '4,000,000',
    status: 'Executed Successfully',
    time: '12:10:00'
  },
  {
    date: '06/10/2016',
    name: 'Reload Telkomsel',
    numb: 'Ref No. 133000056893',
    transType: 'Inhouse Transfer',
    currBal: '4,000,000',
    status: 'Executed Successfully',
    time: '12:10:00'
  },
];

export default class TransHistory extends React.Component {
  static navigationOptions = ({ navigation }) => ({
    header: (
      <HeaderPage goBack={() => navigation.goBack()} title={'Transaction Status'} navigations={() => navigation.navigate("DrawerOpen")} />
    )
  });

  onScrollToEnd() {

    this.setState({bugState: '?'});

    datas.push(
      {
        date: '06/10/2016',
        name: 'Reload Telkomsel',
        numb: 'Ref No. 133000056893',
        transType: 'Inhouse Transfer',
        currBal: '4,000,000',
        status: 'Executed Successfully',
        time: '12:10:00'
      },
      {
        date: '06/10/2016',
        name: 'Reload Telkomsel',
        numb: 'Ref No. 133000056893',
        transType: 'Inhouse Transfer',
        currBal: '4,000,000',
        status: 'Executed Successfully',
        time: '12:10:00'
      },
      {
        date: '06/10/2016',
        name: 'Reload Telkomsel',
        numb: 'Ref No. 133000056893',
        transType: 'Inhouse Transfer',
        currBal: '4,000,000',
        status: 'Executed Successfully',
        time: '12:10:00'
      },
      {
        date: '06/10/2016',
        name: 'Reload Telkomsel',
        numb: 'Ref No. 133000056893',
        transType: 'Inhouse Transfer',
        currBal: '4,000,000',
        status: 'Executed Successfully',
        time: '12:10:00'
      },
      {
        date: '06/10/2016',
        name: 'Reload Telkomsel',
        numb: 'Ref No. 133000056893',
        transType: 'Inhouse Transfer',
        currBal: '4,000,000',
        status: 'Executed Successfully',
        time: '12:10:00'
      },
    );
    console.log(datas.length);
  }

  render() {
    return (
      <View style={{flex: 1, backgroundColor: '#eef0f0'}}>
        <Content
          onScroll={({nativeEvent}) => {
            if (isCloseToBottom(nativeEvent)) {
              this.onScrollToEnd();
            }
          }}
          scrollEventThrottle={400}
        >
          <View style={css.container}>
            <Text style={[css.b, css.darkRed, {fontSize: 18, marginLeft: 5}]}>All Transaction</Text>
              
            <View style={styles.content}>
              {
                datas.map(function(data, index){
                  return(
                    <TouchableOpacity key={index} onPress={() => this.props.navigation.navigate('TransReference')}>
                      <View style={styles.listItem}>
                        <Grid>
                          <Col>
                            <Text style={[css.p, {color: '#20B420'}]}>{data.date}<Text style={{fontSize: 12}}> {data.time}</Text></Text>
                            <Text style={[css.p, css.b]}>{data.name}</Text>
                            <Text style={[css.p, {fontSize: 14}]}>{data.numb}</Text>
                          </Col>
                          <Col style={{alignItems: 'flex-end'}}>
                            <Text style={[{color: '#20B420', marginBottom: 2}]}><Text style={[css.p, css.b, {color: '#20B420', fontSize: 12}]}>{data.transType}</Text></Text>
                            <Text style={[css.p, {fontSize: 14, marginBottom: 2}]}>IDR <Text style={[css.p, css.b]}>{data.currBal}</Text>.00</Text>
                            <Text style={[css.p, {fontSize: 12}]}>{data.status}</Text>
                          </Col>
                        </Grid>
                      </View>
                    </TouchableOpacity>
                  );
                }.bind(this))
              }

              <View style={{alignItems: 'center', marginTop: 15, marginBottom: 10}}>
                <ActivityIndicator size={30} color="#a71e23" /> 
              </View>

            </View>

          </View>
        </Content>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  content: {
    marginTop: 15,
  },
  listItem: {
    marginLeft: 0,
    marginRight: 0,
    backgroundColor: '#fff',
    borderRadius: 4,
    paddingHorizontal: 15,
    paddingTop: 5,
    paddingBottom: 7,
    marginBottom: 5,
  }
});