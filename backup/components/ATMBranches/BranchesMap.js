import React from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity } from 'react-native';
import SvgUri from 'react-native-svg-uri';
import HeaderPage from '../HeaderPage';
import { css } from '../Styles';
import { Icon } from 'native-base';
import { NavigationActions } from "react-navigation";
import MapView from 'react-native-maps';

export default class BranchesMap extends React.Component {
  static navigationOptions = ({ navigation }) => ({
    header: (
      <HeaderPage goBack={() => navigation.goBack()} title={'Branches Directory'} navigations={() => navigation.navigate("DrawerOpen")} />
    )
  });
  render() {
    // const { navigate } = this.props.navigation;
    // const { params } = this.props.navigation.state;
    // react-native-maps
    const { region } = this.props;
    console.log(region);
    return (
      <View style={{flex: 1, backgroundColor: '#eef0f0'}}>
        <View style={[css.container, {marginTop: 20}]}>
          <Text style={[css.h2, css.h2a, css.red, {marginBottom: 15}]}>{params.location}</Text>
          <View style={{marginBottom: 15, flex: .75}}>
            <MapView
              style={styles.map}
              region={{
                latitude: -6.177325,
                longitude: 106.799071,
                latitudeDelta: 0.015,
                longitudeDelta: 0.0121,
              }}
            >
              <MapView.Marker
                coordinate={{
                  latitude: -6.177325,
                longitude: 106.799071,
              }} />
            </MapView>
          </View>
          <View style={{flex: 1, alignItems: 'flex-end', justifyContent: 'flex-end'}}>
            <TouchableOpacity onPress={() => this.props.navigation.navigate('HomeMenu')} style={[css.button, {flex: 0, paddingVertical: 12, marginBottom: 20}]}>
              <Text style={css.buttonText}>Go to Home</Text>
            </TouchableOpacity>    
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    // ...StyleSheet.absoluteFillObject,
    height: 400,
    width: 400,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  map: {
    ...StyleSheet.absoluteFillObject,
  },
});