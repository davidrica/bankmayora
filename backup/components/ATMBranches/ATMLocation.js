import React from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity } from 'react-native';
import SvgUri from 'react-native-svg-uri';
import HeaderPage from '../HeaderPage';
import { css } from '../Styles';
import { Icon, List, Content } from 'native-base';
import { NavigationActions } from "react-navigation";

const datas = [
  'Jakarta', 'Bandung', 'Banten', 'Cirebon', 'Semarang', 'Kalimantan', 'Surabaya', 'Medan', 'Padang', 'Jayapura', 'Ambon', 'Sulawesi'
];

export default class ATMLocation extends React.Component {
  static navigationOptions = ({ navigation }) => ({
    header: (
      <HeaderPage goBack={() => navigation.goBack()} title={'ATM Directory'} navigations={() => navigation.navigate("DrawerOpen")} />
    )
  });
  render() {
    const { params } = this.props.navigation.state;
    const { navigate } = this.props.navigation;
    // <View style={css.leftTextPanel}>
    //   <Text style={[css.h2, css.red]}>{data}</Text>
    //   <SvgUri style={{color:'#a71e23', fontSize: 24, position: 'absolute', bottom: -3, right: 0}} width="50" height="50" source={require('../../assets/images/icons/success-01.svg')} />     
    // </View>
    return (
      <Content style={{flex: 1, backgroundColor: '#eef0f0'}}>
        <View style={[css.container,]}>
          <View style={{flex: 0, marginBottom: 20}}>
            <Text style={[css.h2, css.h2a, css.red, {marginBottom: 15}]}>{params.city}</Text>
            <List dataArray={datas} renderRow={(data) =>
              <TouchableOpacity onPress={() => this.props.navigation.navigate("ATMMap", {location: 'Lobby Jakarta Kuningan 1'})} style={[css.panel, {height: 90, paddingRight: 15}]}>
                
                <View style={{flex: 2, alignItems: 'flex-start'}}>
                  <Text style={css.h2}>Lobby Jakarta Kuningan 1</Text>
                  <Text style={css.p}>Wisma Budi Lantai 2</Text>
                  <Text style={css.p}>Jl. H.R Rasuna Said Kav 6</Text>
                  <Text style={css.p}>Jakarta Selatan</Text>   
                </View>
                <View style={{flex: 1, alignItems: 'flex-end'}}>
                  <SvgUri width="50" height="50" source={require('../../assets/images/icons/directory-01.svg')} />
                </View>
              </TouchableOpacity>    
            }/>
          </View>
        </View>
      </Content>
    );
  }
}