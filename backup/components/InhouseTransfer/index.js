import React, { Component } from "react";
import InhouseTransfer from './InhouseTransfer';
import NewBeneficiaryContact from './NewBeneficiaryContact';
import NewBeneficiaryContact2 from './NewBeneficiaryContact2';
import Confirmation1 from './Confirmation1';
import Confirmation2 from './Confirmation2';
import HomeMenu from '../HomeMenu';
import Submitted from './Submitted';
import { StackNavigator } from "react-navigation";
import CardStackStyleInterpolator from 'react-navigation/src/views/CardStack/CardStackStyleInterpolator';

export default (DrawNav = StackNavigator(
	{
	  InhouseTransfer: { screen: InhouseTransfer },
	  NewBeneficiaryContact: { screen: NewBeneficiaryContact },
	  NewBeneficiaryContact2: { screen: NewBeneficiaryContact2 },
	  Confirmation1: { screen: Confirmation1 },
	  Confirmation2: { screen: Confirmation2 },
	  Submitted: { screen: Submitted },
	  HomeMenu: { screen: HomeMenu },
	},
	{
		transitionConfig: ()=> {
			return {screenInterpolator: CardStackStyleInterpolator.forHorizontal}
		}
	}
));