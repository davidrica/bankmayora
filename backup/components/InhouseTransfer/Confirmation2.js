import React from 'react';
import { StyleSheet, Text, View, TouchableOpacity, TextInput, Picker, DatePickerAndroid } from 'react-native';
import SvgUri from 'react-native-svg-uri';
import HeaderPage from '../HeaderPage';
import { css } from '../Styles';
import { Grid, Col, Content } from 'native-base';
import { NavigationActions } from "react-navigation";

export default class Confirmation2 extends React.Component {
  constructor(props) {
    super(props);
    this.state = { 
      text: '',
    };
  }
  render() {
    const { params } = this.props.navigation.state;
    return (
      <View style={{flex: 1, backgroundColor: '#eef0f0'}}>
        <Content>
          <View style={css.container}>
            <Grid style= {{marginHorizontal: 35, marginBottom: 10}}>
              <Col style={{flex: .5,}}><View style={[css.circleView]}><Text style={[css.circleText]}>1</Text></View></Col>
              <Col style={{flex: 2, borderTopWidth: 1, marginTop: 17.5, borderColor: '#cecece'}}></Col>
              <Col style={{flex: .5,}}><View style={css.circleViewActive}><Text style={css.circleTextActive}>2</Text></View></Col>
              <Col style={{flex: 2, borderTopWidth: 1, marginTop: 17.5, borderColor: '#cecece'}}></Col>
              <Col style={{flex: .5, marginRight: 16}}><View style={css.circleView}><Text style={css.circleText}>3</Text></View></Col>
            </Grid>
            <Grid style= {{marginBottom: 25}}>
              <Col style={{alignItems: 'center'}}><Text style={{fontWeight: 'bold'}}>Confirmation</Text></Col>
              <Col style={{alignItems: 'center'}}><Text style={[css.b, css.darkRed]}>Authentication</Text></Col>
              <Col style={{alignItems: 'center'}}><Text style={{fontWeight: 'bold'}}>Submitted</Text></Col>
            </Grid>

            <View style={[css.singleFormGroup, {alignItems: 'center'}]}>
              <Text style={[css.b, {marginBottom: 10, marginTop: 35, fontSize: 16}]}>Input OTP Code</Text>
              <TextInput underlineColorAndroid='transparent' secureTextEntry keyboardType='numeric' maxLength={6} style={[css.textInput, {marginBottom: 7, width: 95, fontSize: 24, textAlign: 'center'}]} 
              onChangeText={(text) => this.setState({text})} />
            </View>

            <Text style={[css.darkRed, {marginBottom: 20, marginTop: 50, fontSize: 12, textAlign: 'center'}]}>Check your message for OTP</Text>

            <TouchableOpacity style={[css.buttonGrey, {paddingVertical: 12, marginTop: 0}]}>
              <Text style={css.buttonTextGrey}>Resend OTP</Text>
            </TouchableOpacity>

            <TouchableOpacity onPress={() => this.props.navigation.navigate("Submitted", {
              transferFromName: params.transferFromName, transferToName: params.transferToName, transferFromId: params.transferFromId, 
              transferToId: params.transferToId, message: params.message, email: params.email, amount: params.amount
            })} style={[css.button, {paddingVertical: 12, marginTop: 0}]}>
              <Text style={css.buttonText}>Submit</Text>
            </TouchableOpacity>
          </View>
        </Content>
      </View>
    );
  }
}

Confirmation2.navigationOptions = ({ navigation }) => ({
  header: (
    <HeaderPage goBack={() => navigation.dispatch(NavigationActions.back())} title={'Confirmation'} navigations={() => navigation.navigate("DrawerOpen")} />
  )
});