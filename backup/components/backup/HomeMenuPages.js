import React from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity, Alert } from 'react-native';
import SvgUri from 'react-native-svg-uri';
import HeaderHome from './HeaderHome';
import { Container, Header, Title, Left, Icon, Right, Button, Body, Content, Card, CardItem } from "native-base";

export default class HomeMenu extends React.Component {

  handleAccounts() {
    this.setState({
      component: <Accounts onBack={this.handleBack.bind(this)} />,
    });
  }

  static navigationOptions = ({ navigation }) => ({
    header: (
      <Header androidStatusBarColor={'#8a181b'} style={{backgroundColor: '#a71e23'}}>
        <Left>
          <Button transparent onPress={() => this.props.navigation.navigate("DrawerOpen")}>
            <Icon name="menu" />
          </Button>
        </Left>
        <Body>
          <Title>EditScreenOne</Title>
        </Body>
        <Right />
      </Header>
    )
  });

  render() {
    const { navigate } = this.props.navigation;
    return (
      <View style={{flex: 1, backgroundColor: '#eef0f0'}}>
        <HeaderHome navigations={() => this.props.navigation.navigate("DrawerOpen")} />

        <View style={{flex: 6, marginHorizontal: 20, marginVertical: 20, flexWrap: 'wrap'}}>
          <View style={styles.rowPannel}>
            <TouchableOpacity onPress={() => navigate("Accounts")} style={styles.pannelLeft}>
              <SvgUri width='80' height='80' source={require('../assets/images/icons/balance-01.svg')}/>
              <Text style={styles.textPannel}>Accounts</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={this.props.onInhouseTransfer} style={styles.pannelRight}>
              <SvgUri width='80' height='80' source={require('../assets/images/icons/inhouse-transfer-01.svg')}/>
              <Text style={styles.textPannel}>Inhouse Transfer</Text>
            </TouchableOpacity>
          </View>

          <View style={styles.rowPannel}>
            <TouchableOpacity onPress={this.props.onDomesticTransfer} style={styles.pannelLeft}>
              <SvgUri width='80' height='80' source={require('../assets/images/icons/domestic-transfer-triangle-white-inside.svg')}/>
              <Text style={styles.textPannel}>Domestic Transfer</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={this.props.onBillPayment}style={styles.pannelRight}>
              <SvgUri width='80' height='80' source={require('../assets/images/icons/bill-payment-01.svg')}/>
              <Text style={styles.textPannel}>Bill Payment</Text>
            </TouchableOpacity>
          </View>

          <View style={styles.rowPannel}>
            <TouchableOpacity onPress={this.props.onPurchase}style={styles.pannelLeft}>
              <SvgUri width='80' height='80' source={require('../assets/images/icons/purchase-new-01.svg')}/>
              <Text style={styles.textPannel}>Purchase</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.pannelRight}>
              <SvgUri width='80' height='80' source={require('../assets/images/icons/fast-transaction.svg')}/>
              <Text style={styles.textPannel}>Fast Transaction</Text>
            </TouchableOpacity>
          </View>

          <View style={[styles.rowPannel, {marginBottom: 0}]}>
            <TouchableOpacity onPress={this.props.onTransactionStatus}style={styles.pannelLeft}>
              <SvgUri width='80' height='80' source={require('../assets/images/icons/transaction-status-change-01.svg')}/>
              <Text style={styles.textPannel}>Transaction Status</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={this.props.onInquiry}style={styles.pannelRight}>
              <SvgUri width='80' height='80' source={require('../assets/images/icons/inquiry-01.svg')}/>
              <Text style={styles.textPannel}>Inquiry</Text>
            </TouchableOpacity>
          </View>

        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  rowPannel: {
    flex: 1,
    flexDirection: 'row',
    marginBottom: 20,
  },
  pannelLeft: {
    flex: 1,
    marginRight: 20,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff',
  },
  pannelRight: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff',
  },
  textPannel: {
    color: '#a71e23',
    fontSize: 18,
    fontWeight: 'bold'
  }
});