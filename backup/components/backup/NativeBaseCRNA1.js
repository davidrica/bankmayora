import * as Expo from "expo";
import React, { Component } from "react";
import { StyleProvider } from "native-base";
import { Root } from "native-base";
import { StackNavigator, DrawerNavigator } from "react-navigation";

import Header from "./screens/Header/";
import Header1 from "./screens/Header/1";
import Header2 from "./screens/Header/2";
import Home from "./screens/home/";
import SideBar from "./screens/sidebar";



const Drawer = DrawerNavigator(
  {
    Home: { screen: Home },
    Anatomy: { screen: Anatomy },
    Header: { screen: Header },
    Footer: { screen: Footer },
  },
  {
    initialRouteName: "Home",
    contentOptions: {
      activeTintColor: "#e91e63"
    },
    contentComponent: props => <SideBar {...props} />
  }
);

const AppNavigator = StackNavigator(
  {
    Drawer: { screen: Drawer },

    Header1: { screen: Header1 },
    Header2: { screen: Header2 },


  },
  {
    initialRouteName: "Drawer",
    headerMode: "none"
  }
);



export default class NativeBaseCRNA extends React.Component {
  render() {
    return <Setup />;
  }
}


class Setup extends Component {
  constructor() {
    super();
    this.state = {
      isReady: false
    };
  }
  componentWillMount() {
    this.loadFonts();
  }
  async loadFonts() {
    await Expo.Font.loadAsync({
      Roboto: require("native-base/Fonts/Roboto.ttf"),
      Roboto_medium: require("native-base/Fonts/Roboto_medium.ttf"),
      Ionicons: require("@expo/vector-icons/fonts/Ionicons.ttf")
    });
    this.setState({ isReady: true });
  }
  render() {
    if (!this.state.isReady) {
      return <Expo.AppLoading />;
    }
    return (
      <StyleProvider style={getTheme(variables)}>
        <AppNav />
      </StyleProvider>
    );
  }
}

class AppNav extends Component {
	render() {
		return (
			<Root>
		    <AppNavigator />
		  </Root>
		);
	}
}
