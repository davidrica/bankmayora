import React from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity, ScrollView } from 'react-native';
import SvgUri from 'react-native-svg-uri';
import HeaderPage from './HeaderPage';
import { css } from './Styles';

function CurrentSaving(props){
  const currentSaving = props.currentSaving;
  const listCursave = currentSaving.map((list) => 
    <View style={[css.panel,]} key={list.id}>
      <View style={css.leftTextPanel}>
        <Text style={css.h2}>{list.name}</Text>
        <Text style={css.p}>{list.id}</Text>         
      </View>
      <View style={css.rightTextPanel}>
        <Text style={css.p, {textAlign: 'right'}}>IDR 
          <Text style={css.h2}> {list.balance}</Text>
          .00
        </Text>      
      </View>
    </View>
  );
  return (listCursave);
};

function TimeDeposit(props){
  const timeDeposit = props.timeDeposit;
  const listTimeDep = timeDeposit.map((list) => 
    <View style={css.panel} key={list.index}>
      <View style={[css.leftTextPanel, {flex: 2,}]}>
        <Text style={css.h2}>{list.name}</Text>
        <Text style={css.p}>{list.id}</Text>         
      </View>
      <View style={[css.rightTextPanel, {flex: 1,}]}>
        <Text style={css.p, {textAlign: 'right'}}>IDR 
          <Text style={css.h2}> {list.balance}</Text>
          .00
        </Text>      
      </View>
    </View>
  );
  return (listTimeDep);
};

function Loan(props){
  const loan = props.loan;
  const listLoan = loan.map((list) => 
    <View style={css.panel} key={list.id}>
      <View style={css.leftTextPanel}>
        <Text style={css.h2}>{list.name}</Text>
        <Text style={css.p}>{list.id}</Text>         
      </View>
      <View style={css.rightTextPanel}>
        <Text style={css.p, {textAlign: 'right'}}>IDR 
          <Text style={css.h2}> {list.balance}</Text>
          .00
        </Text>      
      </View>
    </View>
  );
  return (listLoan);
};

const currentSaving = [
  {id: 10150063825, name: 'Fiona Mayora', balance: '4,000,000'},
  {id: 11241279512, name: 'Fiona USD Mayora', balance: '22,134,286'},
  {id: 11241279111, name: 'Fiona New Mayora', balance: '19,997,821'},
];

const timeDeposit = [
  {id: 'A888888', name: 'Time Deposit - 6 Months', balance: '4,000,000', index: '1'},
  {id: 'A888888', name: 'Time Deposit - 3 Months', balance: '4,000,000', index: '2'},
];

const loan = [
  {id: '087763', name: 'KPR', balance: '180,000,000'},
];

export default class Accounts extends React.Component {
  render() {
    const { navigate } = this.props.navigation;
    return (
      <View style={{flex: 1, backgroundColor: '#eef0f0'}}>
        <HeaderPage goBack={() => this.props.navigation.goBack()} title={'Accounts'} navigations={() => this.props.navigation.navigate("DrawerOpen")} />
        <ScrollView showsVerticalScrollIndicator={false}> 
          <View style={[css.container,]}>
            <TouchableOpacity onPress={() => navigate("CurrentBalance")} style={{flex: 0, marginBottom: 25}}>
              <Text style={[css.h2, css.h2a, css.red]}>Current & Saving</Text>
              <CurrentSaving currentSaving={currentSaving}/>
              <Text style={[css.red, {textAlign: 'right', marginTop: 20}]}>IDR 
                <Text style={[css.p, css.b, css.red]}> 46,312,107</Text>
                .62
              </Text> 
            </TouchableOpacity>

            <View style={{flex: 0, marginBottom: 25}}>
              <Text style={[css.h2, css.h2a, css.red]}>Time Deposit</Text>
              <TimeDeposit timeDeposit={timeDeposit}/>
              <Text style={[css.red, {textAlign: 'right', marginTop: 20}]}>IDR 
                <Text style={[css.p, css.b, css.red]}> 8,000,000</Text>
                .00
              </Text> 
            </View>

            <View style={{flex: 0}}>
              <Text style={[css.h2, css.h2a, css.red]}>Loan</Text>
              <Loan loan={loan}/>
              <Text style={[css.p, css.red, {textAlign: 'right', marginTop: 20}]}>IDR 
                <Text style={[css.b, css.red]}> 180,000,000</Text>
                .00
              </Text> 
            </View>
          </View>
        </ScrollView>
      </View>
    );
  }
}