import React, { Component } from 'react';
import {
  Text, View
} from 'react-native';

import {Header,Left,Button,Icon,Right,Body,Title, Content, Container} from 'native-base';
import {Drawer} from 'native-base';
import { getStatusBarHeight } from 'react-native-status-bar-height';

class Sidebar extends Component {
  render() {
    return (
          <Content style={{backgroundColor:'#FFFFFF'}}>
            <Text>Drawer</Text>
          </Content>
    );
  }
}

class AppHeader extends Component {
  render() {
    return (
      <Header>
       <Left>
       <Button transparent
              onPress={()=>this.props.openDrawer()}
       >
         <Icon name='menu' />
       </Button>
       </Left>
       <Body>
         <Title>SDCC Wallet</Title>
       </Body>
       <Right>
         <Button transparent>
           <Icon name='bulb' />
         </Button>
       </Right>
     </Header>
    );
  }
}

export default class NativeBaseCRNA extends Component {
	state = { fontsAreLoaded: false, showToast: false };

  async componentWillMount() {
      await Expo.Font.loadAsync({
        'Roboto': require('native-base/Fonts/Roboto.ttf'),
        'Roboto_medium': require('native-base/Fonts/Roboto_medium.ttf'),
      });
      this.setState({fontsAreLoaded: true});
  }

   closeDrawer = () => {
	    this.drawer._root.close()
	  };
	  openDrawer = () => {
	    this.drawer._root.open()
	  };

  render() {
    if (this.state.fontsAreLoaded) {
      return (
        <Drawer
        ref={(ref) => { this.drawer = ref; }}
        content={<Sidebar/>}
        onClose={() => this.closeDrawer()} >
        <View style={{height: getStatusBarHeight(), backgroundColor: 'red'}}/>
        <AppHeader
            openDrawer={this.openDrawer.bind(this)}
        />
        </Drawer>
      ); 
    }
    else {
      return (<Text>Loading</Text>);
    }
  }
}