import React from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity, TouchableNativeFeedback, Modal, TouchableHighlight } from 'react-native';
import SvgUri from 'react-native-svg-uri';
import HeaderPage from '../HeaderPage';
import { css } from '../Styles';
import { Icon, List, Grid, Col, Radio, StyleProvider } from 'native-base';

//Custom nativeBase Theme
import getTheme from '../../native-base-theme/components';
import customTheme from '../../native-base-theme/variables/customTheme';

const datas = [
  {
    subject: 'RE: Forgot User ID',
    date: '28/02/2018',
    time: '11:15',
    index: 0,
  },
  {
    subject: 'RE: Forgot Password',
    date: '28/02/2018',
    time: '11:15',
    index: 1,
  },
  {
    subject: 'RE: Error',
    date: '28/02/2018',
    time: '11:15',
    index: 2,
  },
  {
    subject: 'RE: Forgot Password',
    date: '28/02/2018',
    time: '11:15',
    index: 3,
  },
];


export default class Sent extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      itemSelected: 'itemOne',
      itemSelected2: 'itemOne',
      indexTemp: {},
      modalVisible: false,
      panelBackground: '#fff',
    };
  }

  radioPress(index) {
    this.setState({indexTemp: index});
    console.log(this.state.indexTemp);
  }

  openModal() {
    this.setState({modalVisible:true});
  }

  closeModal() {
    this.setState({modalVisible:false});
  }

  static navigationOptions = ({ navigation }) => ({
    header: (
      <HeaderPage goBack={() => navigation.goBack()} title={'Help Desk'} navigations={() => navigation.navigate("DrawerOpen")} />
    )
  });

  render() {
    // const { params } = this.props.navigation.state;
    return (
      <StyleProvider style={getTheme(customTheme)}>
        <View style={{flex: 1, backgroundColor: '#eef0f0'}}>
          <View style={[css.container,]}>
            <View style={{flex: 1, marginBottom: 25}}>
              <View style={{flex: 0.1, flexDirection: 'row', paddingRight: 5}}>
                <View style={{flex: 1, alignItems: 'flex-start'}}>
                  <Text style={[css.h2, css.h2a, css.red, {marginBottom: 15}]}>Sent</Text>
                </View>
                <View style={{flex: 1, alignItems: 'flex-end'}}>
                  <TouchableOpacity onPress={() => this.openModal()}>
                    <SvgUri width="22" height="22" source={require('../../assets/images/icons/trash-01.svg')} />
                  </TouchableOpacity>
                </View>
              </View>

              {
                datas.map(function(data, index){
                  return (
                    <TouchableOpacity key={index} 
                    style={[css.panel, {height: 43, flexDirection: 'row', paddingLeft: 0, backgroundColor: index == this.state.indexTemp ? '#a71e23' : '#fff'}]}
                    onPress={() => this.props.navigation.navigate("Message", {subject: data.subject})}>
                      <TouchableOpacity onPress={() => {this.setState({indexTemp: index})}} 
                      style={{flex: 0.5, paddingLeft: 20, height: 43, justifyContent: 'center'}}>
                        <Radio onPress={() => {this.setState({indexTemp: index})}}
                        selected={index == this.state.indexTemp} />  
                      </TouchableOpacity>
                      <View style={{flex: 2}}>
                        <Text style={[css.p,{color: index == this.state.indexTemp ? '#fff' : '#080909'} ]}>{data.subject}</Text>     
                      </View>
                      <View style={{flex: 1, alignItems: 'flex-end'}}>
                        <Text style={{fontSize: 12, textAlign: 'right', color: index == this.state.indexTemp ? '#fff' : '#080909'}}>{data.date}</Text>   
                        <Text style={{fontSize: 12, textAlign: 'right', color: index == this.state.indexTemp ? '#fff' : '#080909'}}>{data.time}</Text>  
                      </View>
                    </TouchableOpacity>
                  );
                }.bind(this))
              }

            </View>
          </View>

          <Modal transparent={true} visible={this.state.modalVisible} onRequestClose={() => this.closeModal()} animationType={'none'}>
            <TouchableHighlight onPress={() => this.closeModal()} style={[styles.modalContainer, {flex: 1, backgroundColor: 'rgba(0,0,0,.6)'}]}>
              <View style={styles.MainContainer}>
                <SvgUri width="70" height="70" source={require('../../assets/images/icons/trash-01.svg')} />
                <Text style={[css.b, {textAlign: 'center', fontSize: 16, marginTop: 15}]}>Do you want to delete this message?</Text>
                <View style={{flexDirection: 'row'}}>
                  <TouchableOpacity onPress={() => this.closeModal()} style={[css.buttonGrey, {flex: 1, marginRight: 30, marginTop: 25, elevation: 1, paddingVertical: 5}]}>
                    <Text style={css.buttonTextGrey}>No</Text>
                  </TouchableOpacity>
                  <TouchableOpacity style={[css.button, {flex: 1, marginTop: 25, elevation: 1, paddingVertical: 5}]}>
                    <Text style={css.buttonText}>Yes</Text>
                  </TouchableOpacity>            
                </View>
              </View>
            </TouchableHighlight>         
          </Modal>
        </View>
      </StyleProvider>
    );
  }
}

const styles = StyleSheet.create({
  modalContainer: {
    backgroundColor: 'transparent',
    paddingHorizontal: 20,
    justifyContent: 'center'
  },

  MainContainer :{
    justifyContent: 'center',
    alignItems: 'center',
    padding: 20,
    paddingBottom: 10,
    backgroundColor: '#fff',
    borderRadius: 3,
    elevation: 2,
  },
});