import React from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity, TouchableNativeFeedback, Modal, TouchableHighlight } from 'react-native';
import SvgUri from 'react-native-svg-uri';
import HeaderPage from '../HeaderPage';
import { css } from '../Styles';
import { Icon } from 'native-base';
import { NavigationActions } from "react-navigation";
import Communications from 'react-native-communications';

export default class HelpDesk extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      menuBackground: '#fff',
      modalVisible: false,
    }
  }

  openModal() {
    this.setState({modalVisible:true});
  }

  closeModal() {
    this.setState({modalVisible:false});
  }

  static navigationOptions = ({ navigation }) => ({
    header: (
      <HeaderPage goBack={() => navigation.goBack()} title={'Help Desk'} navigations={() => navigation.navigate("DrawerOpen")} />
    )
  });

  render() {
    // const { navigate } = this.props.navigation;
    return (
      <View style={{flex: 1, backgroundColor: '#eef0f0'}}>
        <View style={[css.container,]}>
          <View style={{flex: 0, marginBottom: 25}}>
            <Text style={[css.h2, css.h2a, {marginBottom: 15}]}>Please Choose</Text>
            <TouchableOpacity onPress={() => this.openModal()} style={[css.panel, {height: 60}]}>
              <View style={css.leftTextPanel}>
                <Text style={[css.h2, css.red]}>Call</Text>
                <Icon name='md-arrow-dropright' style={{color:'#a71e23', fontSize: 24, position: 'absolute', bottom: -3, right: 0}}/>     
              </View>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => this.props.navigation.navigate("Email")} style={[css.panel, {height: 60}]}>
              <View style={css.leftTextPanel}>
                <Text style={[css.h2, css.red]}>Email</Text>
                <Icon name='md-arrow-dropright' style={{color:'#a71e23', fontSize: 24, position: 'absolute', bottom: -3, right: 0}}/>     
              </View>
            </TouchableOpacity>
          </View>
        </View>
        <Modal transparent={true} visible={this.state.modalVisible} onRequestClose={() => this.closeModal()} animationType={'none'}>
          <TouchableHighlight onPress={() => this.closeModal()} style={[styles.modalContainer, {flex: 1, backgroundColor: 'rgba(0,0,0,.6)'}]}>
            <View style={styles.MainContainer}>
              <SvgUri width="50" height="50" source={require('../../assets/images/icons/help-desk-01.svg')} />
              <Text style={[css.b, {textAlign: 'center', fontSize: 16, marginTop: 15}]}>Continue to call?</Text>
              <Text style={[css.b, {textAlign: 'center', fontSize: 16}]}>You will automatically logout your account</Text>
              <View style={{flexDirection: 'row'}}>
                <TouchableOpacity onPress={() => this.closeModal()} style={[css.buttonGrey, {flex: 1, marginRight: 30, marginTop: 25, elevation: 1, paddingVertical: 5}]}>
                  <Text style={css.buttonTextGrey}>No</Text>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => Communications.phonecall('081289464536', true)} style={[css.button, {flex: 1, marginTop: 25, elevation: 1, paddingVertical: 5}]}>
                  <Text style={css.buttonText}>Yes</Text>
                </TouchableOpacity>            
              </View>
            </View>
          </TouchableHighlight>         
        </Modal>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  modalContainer: {
    backgroundColor: 'transparent',
    paddingHorizontal: 20,
    justifyContent: 'center'
  },

  MainContainer :{
    justifyContent: 'center',
    alignItems: 'center',
    padding: 20,
    backgroundColor: '#fff',
    borderRadius: 3,
    elevation: 2,
  },
});