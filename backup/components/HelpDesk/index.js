import React, { Component } from "react";
import HelpDesk from './HelpDesk';
import Email from './Email';
import Inbox from './Inbox';
import Sent from './Sent';
import Message from './Message';
import NewMessage from './NewMessage';
import HomeMenu from '../HomeMenu';
import { StackNavigator } from "react-navigation";
import CardStackStyleInterpolator from 'react-navigation/src/views/CardStack/CardStackStyleInterpolator';
export default (DrawNav = StackNavigator(
	{
		HelpDesk: { screen: HelpDesk },
		Email: { screen: Email },
		Inbox: { screen: Inbox },
		Sent: { screen: Sent },
		Message: { screen: Message },
		NewMessage: { screen: NewMessage },
		HomeMenu: { screen: HomeMenu },
	},
	{
		transitionConfig: ()=> {
			return {screenInterpolator: CardStackStyleInterpolator.forHorizontal}
		}
	}
));