import React from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity, TouchableNativeFeedback, Modal, TouchableHighlight } from 'react-native';
import SvgUri from 'react-native-svg-uri';
import HeaderPage from '../HeaderPage';
import { css } from '../Styles';
import { Icon } from 'native-base';
import Communications from 'react-native-communications';

export default class Email extends React.Component {
  static navigationOptions = ({ navigation }) => ({
    header: (
      <HeaderPage goBack={() => navigation.goBack()} title={'Help Desk'} navigations={() => navigation.navigate("DrawerOpen")} />
    )
  });

  render() {
    return (
      <View style={{flex: 1, backgroundColor: '#eef0f0'}}>
        <View style={[css.container,]}>
          <View style={{flex: 0, marginBottom: 25}}>
            <Text style={[css.h2, css.h2a, {marginBottom: 15}]}>Please Choose</Text>
            <TouchableOpacity onPress={() => this.props.navigation.navigate("Inbox")} style={[css.panel, {height: 60}]}>
              <View style={css.leftTextPanel}>
                <Text style={[css.h2, css.red]}>Inbox</Text>
                <Icon name='md-arrow-dropright' style={{color:'#a71e23', fontSize: 24, position: 'absolute', bottom: -3, right: 0}}/>     
              </View>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => this.props.navigation.navigate("Sent")} style={[css.panel, {height: 60}]}>
              <View style={css.leftTextPanel}>
                <Text style={[css.h2, css.red]}>Sent</Text>
                <Icon name='md-arrow-dropright' style={{color:'#a71e23', fontSize: 24, position: 'absolute', bottom: -3, right: 0}}/>     
              </View>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => this.props.navigation.navigate("NewMessage", {subject: ''})} style={[css.panel, {height: 60}]}>
              <View style={css.leftTextPanel}>
                <Text style={[css.h2, css.red]}>Compose</Text>
                <Icon name='md-arrow-dropright' style={{color:'#a71e23', fontSize: 24, position: 'absolute', bottom: -3, right: 0}}/>     
              </View>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  }
}