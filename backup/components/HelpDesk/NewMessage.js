import React from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity, TouchableNativeFeedback, Modal, TouchableHighlight, TextInput } from 'react-native';
import SvgUri from 'react-native-svg-uri';
import HeaderPage from '../HeaderPage';
import { css } from '../Styles';
import { Icon, List, Grid, Col, Radio } from 'native-base';

export default class NewMessage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      modalVisible: false,
      text: '',
    };
  }

  openModal() {
    this.setState({modalVisible:true});
  }

  closeModal() {
    this.setState({modalVisible:false});
  }

  static navigationOptions = ({ navigation }) => ({
    header: (
      <HeaderPage goBack={() => navigation.goBack()} title={'Help Desk'} navigations={() => navigation.navigate("DrawerOpen")} />
    )
  });

  render() {
    const { params } = this.props.navigation.state;
    return (
      <View style={{flex: 1, backgroundColor: '#eef0f0'}}>
        <View style={[css.container,]}>
          <View style={{flex: 1, marginBottom: 25}}>
            <View style={{flexDirection: 'row', marginBottom: 35}}>
              <View style={{flex: 1, alignItems: 'flex-start'}}>
              </View>
              <View style={{flex: 0.6, alignItems: 'flex-end'}}>
                <TouchableOpacity onPress={() => this.openModal()}
                style={[css.button, {marginTop: 0, flexDirection: 'row', paddingVertical: 6, justifyContent: 'space-between', paddingHorizontal: 15}]}>
                  <View style={{alignItems: 'flex-start'}}>
                    <Text style={{fontSize:16, color: '#fff', fontWeight: 'bold'}}>Discard</Text>   
                  </View>
                  <View style={{alignItems: 'flex-end', justifyContent: 'center'}}>
                    <SvgUri width="11" height="11" source={require('../../assets/images/icons/cancel-white-01.svg')} />  
                  </View>
                </TouchableOpacity>
              </View>
            </View>

            <View style={{flexDirection: 'row', marginBottom: 20, alignItems: 'center'}}>
              <Text style={[css.b, {marginRight: 55}]}>Subject</Text>
              <Text style={[css.b, {marginRight: 20}]}>:</Text>
              <TextInput underlineColorAndroid='transparent' style={[css.textInput, {flex: 1.6, height: 43}]} 
              onChangeText={(text) => this.setState({text})} value={params.subject == '' ? this.state.text : params.subject} />
            </View>

            <View>
              <Text style={[css.b, {marginBottom: 10}]}>Message</Text>
              <TextInput underlineColorAndroid='transparent' style={[css.textInput, {height: 200, textAlignVertical: 'top'}]} multiline />
              <TouchableOpacity
              style={[css.button, {marginTop: 30, paddingHorizontal: 23, paddingVertical: 8, alignSelf: 'flex-end'}]}>
                <Text style={{fontSize:18, color: '#fff', fontWeight: 'bold'}}>Send</Text>
              </TouchableOpacity>
            </View>

          </View>
        </View>

        <Modal transparent={true} visible={this.state.modalVisible} onRequestClose={() => this.closeModal()} animationType={'none'}>
          <TouchableHighlight onPress={() => this.closeModal()} style={[styles.modalContainer, {flex: 1, backgroundColor: 'rgba(0,0,0,.6)'}]}>
            <View style={styles.MainContainer}>
              <SvgUri width="70" height="70" source={require('../../assets/images/icons/trash-01.svg')} />
              <Text style={[css.b, {textAlign: 'center', fontSize: 16, marginTop: 15}]}>Do you want to delete this message?</Text>
              <View style={{flexDirection: 'row'}}>
                <TouchableOpacity onPress={() => this.closeModal()} style={[css.buttonGrey, {flex: 1, marginRight: 30, marginTop: 25, elevation: 1, paddingVertical: 5}]}>
                  <Text style={css.buttonTextGrey}>No</Text>
                </TouchableOpacity>
                <TouchableOpacity style={[css.button, {flex: 1, marginTop: 25, elevation: 1, paddingVertical: 5}]}>
                  <Text style={css.buttonText}>Yes</Text>
                </TouchableOpacity>            
              </View>
            </View>
          </TouchableHighlight>         
        </Modal>

      </View>
    );
  }
}

const styles = StyleSheet.create({
  modalContainer: {
    backgroundColor: 'transparent',
    paddingHorizontal: 20,
    justifyContent: 'center'
  },

  MainContainer :{
    justifyContent: 'center',
    alignItems: 'center',
    padding: 20,
    paddingBottom: 10,
    backgroundColor: '#fff',
    borderRadius: 3,
    elevation: 2,
  },
});