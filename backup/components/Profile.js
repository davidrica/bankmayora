import React, { Component } from "react";
import Profile2 from "./Profile2.js";
import EditScreenOne from "./EditScreenOne.js";
import { StackNavigator } from "react-navigation";
export default (DrawNav = StackNavigator({
  Profile2: { screen: Profile2 },
  EditScreenOne: { screen: EditScreenOne },
}));