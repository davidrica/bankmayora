import React from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity, ScrollView } from 'react-native';
import { Container, Header, Left, Body, Title, Card, CardItem, Content, Right, Icon, Button, ListItem, List, Badge } from "native-base";
import SvgUri from 'react-native-svg-uri';
import HeaderPage from '../HeaderPage';
import { css } from '../Styles';
import { StackNavigator, NavigationActions } from "react-navigation";

function CurrentSaving(props){
  const currentSaving = props.currentSaving;
  const listCursave = currentSaving.map((list) => 
    <TouchableOpacity style={[css.panel, {paddingHorizontal: 15}]} key={list.id}>
      <View style={css.leftTextPanel}>
        <Text style={css.h2}>{list.name}</Text>
        <Text style={css.p}>{list.id}</Text>         
      </View>
      <View style={css.rightTextPanel}>
        <Text style={[css.p, {textAlign: 'right'}]}>IDR 
          <Text style={css.h2}> {list.balance}</Text>
          .00
        </Text>      
      </View>
    </TouchableOpacity>
  );
  return (listCursave);
};

function TimeDeposit(props){
  const timeDeposit = props.timeDeposit;
  const listTimeDep = timeDeposit.map((list) => 
    <View style={[css.panel, {paddingHorizontal: 15}]} key={list.index}>
      <View style={[css.leftTextPanel, {flex: 1.5,}]}>
        <Text style={css.h2}>{list.name}</Text>
        <Text style={css.p}>{list.id}</Text>         
      </View>
      <View style={[css.rightTextPanel, {flex: 1,}]}>
        <Text style={[css.p, {textAlign: 'right', fontSize: 13}]}>IDR 
          <Text style={css.h2}> {list.balance}</Text>
          .00
        </Text>      
      </View>
    </View>
  );
  return (listTimeDep);
};

function Loan(props){
  const loan = props.loan;
  const listLoan = loan.map((list) => 
    <View style={[css.panel, {paddingHorizontal: 15}]} key={list.id}>
      <View style={css.leftTextPanel}>
        <Text style={css.h2}>{list.name}</Text>
        <Text style={css.p}>{list.id}</Text>         
      </View>
      <View style={css.rightTextPanel}>
        <Text style={[css.p, {textAlign: 'right', fontSize: 13}]}>IDR 
          <Text style={css.h2}> {list.balance}</Text>
          .00
        </Text>      
      </View>
    </View>
  );
  return (listLoan);
};

const currentSaving = [
  {id: 10150063825, name: 'David Mayora', balance: '4,000,000', route: 'CurrentBalance'},
  {id: 11241279512, name: 'David USD Mayora', balance: '22,134,286', route: 'CurrentBalance'},
  {id: 11241279111, name: 'David New Mayora', balance: '19,997,821', route: 'CurrentBalance'},
];

const timeDeposit = [
  {id: 'A888888', name: 'Time Deposit - 6 Months', balance: '4,000,000', index: '1'},
  {id: 'A888888', name: 'Time Deposit - 3 Months', balance: '4,000,000', index: '2'},
];

const loan = [
  {id: '087763', name: 'KPR', balance: '180,000,000'},
];

export default class Accounts extends React.Component {
  render() {
    const { navigate } = this.props.navigation;
    return (
      <View style={{flex: 1, backgroundColor: '#eef0f0'}}>
        <ScrollView showsVerticalScrollIndicator={false}> 

          <View style={[css.container,]}>
            <Text style={[css.h2, css.h2a, css.red]}>Current & Saving</Text>

            <List dataArray={currentSaving} renderRow={data =>
              <ListItem style={[css.panel, {marginLeft: 0, paddingHorizontal: 15}]} button onPress={() => this.props.navigation.navigate(data.route)}>
                <View style={css.leftTextPanel}>
                  <Text style={css.h2}>{data.name}</Text>
                  <Text style={css.p}>{data.id}</Text>         
                </View>
                <View style={css.rightTextPanel}>
                  <Text style={[css.p, {textAlign: 'right', fontSize: 13}]}>IDR 
                    <Text style={css.h2}> {data.balance}</Text>
                    .00
                  </Text>      
                </View>
              </ListItem>}
            />

            <Text style={[css.red, {textAlign: 'right', marginTop: 20, marginBottom: 25}]}>IDR 
              <Text style={[css.p, css.b, css.red]}> 46,312,107</Text>
              .62
            </Text> 

            <View style={{flex: 0, marginBottom: 25}}>
              <Text style={[css.h2, css.h2a, css.red]}>Time Deposit</Text>
              <TimeDeposit timeDeposit={timeDeposit}/>
              <Text style={[css.red, {textAlign: 'right', marginTop: 20}]}>IDR 
                <Text style={[css.p, css.b, css.red]}> 8,000,000</Text>
                .00
              </Text> 
            </View>

            <View style={{flex: 0}}>
              <Text style={[css.h2, css.h2a, css.red]}>Loan</Text>
              <Loan loan={loan}/>
              <Text style={[css.p, css.red, {textAlign: 'right', marginTop: 20, marginBottom: 25}]}>IDR 
                <Text style={[css.b, css.red]}> 180,000,000</Text>
                .00
              </Text> 
            </View>
          </View>
        </ScrollView>
      </View>
    );
  }
}

Accounts.navigationOptions = ({ navigation }) => ({
  header: (
    <HeaderPage goBack={() => navigation.dispatch(NavigationActions.back())} title={'Accounts'} navigations={() => navigation.navigate("DrawerOpen")} />
  )
});