import React from 'react';
import { StyleSheet, Text, View, TouchableOpacity, ScrollView, ActivityIndicator} from 'react-native';
import { Header, Left, Body, Right, Button, Title, Icon, Content, Card, Container, List, ListItem, Grid, Col } from 'native-base';
import HeaderPage from '../HeaderPage';
import { css } from '../Styles';

const isCloseToBottom = ({layoutMeasurement, contentOffset, contentSize}) => {
  const paddingToBottom = 20;
  return layoutMeasurement.height + contentOffset.y >=
    contentSize.height - paddingToBottom;
};

var datas = [
  {
    date: '06/10/2016',
    name: 'Reload Telkomsel',
    numb: 'Ref No. 133000056893',
    pastBal: '22,500',
    currBal: '4,000,000',
  },
  {
    date: '06/10/2016',
    name: 'Reload Telkomsel',
    numb: 'Ref No. 133000056893',
    pastBal: '22,500',
    currBal: '4,000,000',
  },
  {
    date: '06/10/2016',
    name: 'Reload Telkomsel',
    numb: 'Ref No. 133000056893',
    pastBal: '22,500',
    currBal: '4,000,000',
  },
  {
    date: '06/10/2016',
    name: 'Reload Telkomsel',
    numb: 'Ref No. 133000056893',
    pastBal: '22,500',
    currBal: '4,000,000',
  },
  {
    date: '06/10/2016',
    name: 'Reload Telkomsel',
    numb: 'Ref No. 133000056893',
    pastBal: '22,500',
    currBal: '4,000,000',
  },
  {
    date: '06/10/2016',
    name: 'Reload Telkomsel',
    numb: 'Ref No. 133000056893',
    pastBal: '22,500',
    currBal: '4,000,000',
  },
  {
    date: '06/10/2016',
    name: 'Reload Telkomsel',
    numb: 'Ref No. 133000056893',
    pastBal: '22,500',
    currBal: '4,000,000',
  },
  {
    date: '06/10/2016',
    name: 'Reload Telkomsel',
    numb: 'Ref No. 133000056893',
    pastBal: '22,500',
    currBal: '4,000,000',
  },
];

export default class CurrentBalance extends React.Component {
  static navigationOptions = ({ navigation }) => ({
    header: (
      <HeaderPage goBack={() => navigation.goBack()} title={'David Mayora'} navigations={() => navigation.navigate("DrawerOpen")} />
    )
  });

  onScrollToEnd() {

    this.setState({bugState: '?'});

    datas.push(
      {
        date: '06/10/2016',
        name: 'Reload Telkomsel',
        numb: 'Ref No. 133000056893',
        pastBal: '22,500',
        currBal: '4,000,000',
      },
      {
        date: '06/10/2016',
        name: 'Reload Telkomsel',
        numb: 'Ref No. 133000056893',
        pastBal: '22,500',
        currBal: '4,000,000',
      },
      {
        date: '06/10/2016',
        name: 'Reload Telkomsel',
        numb: 'Ref No. 133000056893',
        pastBal: '22,500',
        currBal: '4,000,000',
      },
      {
        date: '06/10/2016',
        name: 'Reload Telkomsel',
        numb: 'Ref No. 133000056893',
        pastBal: '22,500',
        currBal: '4,000,000',
      },
      {
        date: '06/10/2016',
        name: 'Reload Telkomsel',
        numb: 'Ref No. 133000056893',
        pastBal: '22,500',
        currBal: '4,000,000',
      },
    );
    console.log(datas.length);
  }

  render() {
    return (
      <View style={{flex: 1, backgroundColor: '#eef0f0'}}>
        <Content
          onScroll={({nativeEvent}) => {
            if (isCloseToBottom(nativeEvent)) {
              this.onScrollToEnd();
            }
          }}
          scrollEventThrottle={400}
        >
          <View style={styles.title}>
            <Text style={[css.b, css.darkRed, {textAlign: 'center', fontSize: 18}]}>Current Balance</Text>
            <Text style={[css.red, {textAlign: 'center'}]}>IDR <Text style={[css.b, css.red, {fontSize: 22}]}>4,000,000</Text>.00</Text>
          </View>
            
          <Card style={styles.content}>
            {
              datas.map(function(data, index){
                return(
                  <ListItem key={index} style={{marginRight: 17, paddingRight: 0}}>
                    <Grid>
                      <Col>
                        <Text style={[css.p, {color: '#20B420'}]}>{data.date}</Text>
                        <Text style={[css.p, css.b]}>{data.name}</Text>
                        <Text style={[css.p]}>{data.numb}</Text>
                      </Col>
                      <Col style={{alignItems: 'flex-end'}}>
                        <Text style={[{color: '#20B420'}]}>IDR <Text style={[css.p, css.b, {color: '#20B420'}]}>{data.pastBal}</Text>.00</Text>
                        <Text style={[css.p, {fontSize: 14}]}>IDR <Text style={[css.p, css.b]}>{data.currBal}</Text>.00</Text>
                        <Text style={[css.p, {fontSize: 14}]}>(balance)</Text>
                      </Col>
                    </Grid>
                  </ListItem>              
                );
              })
            }
          </Card>
          <View style={{alignItems: 'center', marginTop: 20, marginBottom: 30}}>
            <ActivityIndicator size={30} color="#a71e23" /> 
          </View>
        </Content>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  title: {
    borderRadius: 5,
    alignItems: 'center',
    marginTop: 10
  },
  content: {
    backgroundColor: '#fff',
    marginHorizontal: 10,
    marginLeft: 10,
    marginRight: 10,
    borderRadius: 10,
    marginTop: 15,
    // marginBottom: 60,
  }
});