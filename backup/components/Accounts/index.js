import React, { Component } from "react";
import Accounts from './Accounts';
import CurrentBalance from './CurrentBalance';
import HomeMenu from '../HomeMenu';
import { StackNavigator } from "react-navigation";
import CardStackStyleInterpolator from 'react-navigation/src/views/CardStack/CardStackStyleInterpolator';
export default (DrawNav = StackNavigator(
	{
	  Accounts: { screen: Accounts },
	  CurrentBalance: { screen: CurrentBalance },
	},
	{
		transitionConfig: ()=> {
			return {screenInterpolator: CardStackStyleInterpolator.forHorizontal}
		}
	}
));