import React from 'react';
import { StyleSheet, Text, View, TouchableOpacity, TextInput, Picker, DatePickerAndroid } from 'react-native';
import SvgUri from 'react-native-svg-uri';
import HeaderPage from '../HeaderPage';
import { css } from '../Styles';
import { Grid, Col, Content } from 'native-base';
import { NavigationActions } from "react-navigation";

export default class DomSubmitted extends React.Component {
  constructor(props) {
    super(props);
    this.state = { 
      text: '',
    };
  }
  render() {
    const { params } = this.props.navigation.state;
    return (
      <View style={{flex: 1, backgroundColor: '#eef0f0'}}>
        <Content showsVerticalScrollIndicator={false}>
          <View style={css.container}>
            <View style={{alignItems: 'center', marginTop: 10, marginBottom: 45,}}>
              <SvgUri width="50" height="50" source={require('../../assets/images/icons/success-01.svg')} />
              <Text style={[css.b, {textAlign: 'center', fontSize: 16, maxWidth: 250, marginTop: 10}]}>This transaction has been submitted and successfully executed</Text>
            </View>
    
            <Grid style={{flex: 0, marginBottom: 20}}>
              <Col><Text style={[css.b, {marginBottom: 15} ]}>Transfer From</Text></Col>
              <Col style={{alignItems: 'flex-end'}}>
                <Text style={[css.b, css.darkRed, {fontSize: 18}]}>{ params.transferFromName }</Text>
                <Text style={[css.darkRed, {fontSize: 16}]}>{ params.transferFromId }</Text>
              </Col>
            </Grid>
            <Grid style={{flex: 0, marginBottom: 20}}>
              <Col style={{flex: 0.5}}><Text style={[css.b, {marginBottom: 15} ]}>Transfer To</Text></Col>
              <Col style={{alignItems: 'flex-end'}}>
                <Text style={[css.b, css.darkRed, {fontSize: 18}]}>{ params.transferToName }</Text>
                <Text style={[css.darkRed, {fontSize: 16}]}>{ params.transferToId }</Text>
              </Col>
            </Grid>
            <Grid style={{flex: 0, marginBottom: 20}}>
              <Col><Text style={[css.b, {marginBottom: 15} ]}>Amount</Text></Col>
              <Col style={{alignItems: 'flex-end'}}><Text style={[css.p, css.darkRed]}>IDR <Text style={[css.b, css.darkRed, {fontSize: 20}]}>8,000,000</Text>.00</Text></Col>
            </Grid>
            <Grid style={{flex: 0, marginBottom: 20}}>
              <Col style={{flex: 1.5}}><Text style={[css.b, {marginBottom: 15} ]}>Transaction Reference Number</Text></Col>
              <Col style={{alignItems: 'flex-end'}}><Text style={[css.b, css.p, css.darkRed, {fontSize: 18}]}>1873649134734</Text></Col>
            </Grid>
            <Grid style={{flex: 0, marginBottom: 20}}>
              <Col style={{flex: 1.5}}><Text style={[css.b, {marginBottom: 15} ]}>Reference Number</Text></Col>
              <Col style={{alignItems: 'flex-end'}}><Text style={[css.b, css.p, css.darkRed, {fontSize: 18}]}>1873649134734</Text></Col>
            </Grid>
            <Grid style={{flex: 0, marginBottom: 20}}>
              <Col><Text style={[css.b, {marginBottom: 15} ]}>Submission Date</Text></Col>
              <Col style={{alignItems: 'flex-end'}}><Text style={[css.p, css.darkRed, {fontSize: 18}]}>21 February 2018</Text></Col>
            </Grid>

            <Text style={[css.darkRed, {marginBottom: 20, marginTop: 50, marginHorizontal: -10, fontSize: 11.5, textAlign: 'center'}]}>You can check this reference later on the transaction status home menu</Text>

            <TouchableOpacity onPress={() => this.props.navigation.navigate("HomeMenu")} style={[css.button, {paddingVertical: 12, marginTop: 0}]}>
              <Text style={css.buttonText}>Done</Text>
            </TouchableOpacity>
          </View>
        </Content>
      </View>
    );
  }
}

DomSubmitted.navigationOptions = ({ navigation }) => ({
  header: (
    <HeaderPage goBack={() => navigation.dispatch(NavigationActions.back())} title={'Submitted !'} navigations={() => navigation.navigate("DrawerOpen")} />
  )
});